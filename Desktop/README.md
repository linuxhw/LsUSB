Most popular USB devices in Desktops
====================================

See more info in the [README](https://github.com/linuxhw/LsUSB).

Contents
--------

1. [ USB Devices in Desktops ](#usb-devices)
   * [ Audio ](#audio-usb)
   * [ Bluetooth ](#bluetooth-usb)
   * [ Camera ](#camera-usb)
   * [ Card reader ](#card-reader-usb)
   * [ Cdrom ](#cdrom-usb)
   * [ Chipcard ](#chipcard-usb)
   * [ Converter ](#converter-usb)
   * [ Diagnostic ](#diagnostic-usb)
   * [ Disk ](#disk-usb)
   * [ Dvb card ](#dvb-card-usb)
   * [ Fingerprint reader ](#fingerprint-reader-usb)
   * [ Floppy ](#floppy-usb)
   * [ Gamepad ](#gamepad-usb)
   * [ Hardware key ](#hardware-key-usb)
   * [ Hasp ](#hasp-usb)
   * [ Hub ](#hub-usb)
   * [ Human interface ](#human-interface-usb)
   * [ Infrared ](#infrared-usb)
   * [ Input/keyboard ](#inputkeyboard-usb)
   * [ Input/mouse ](#inputmouse-usb)
   * [ Joystick ](#joystick-usb)
   * [ Mfp ](#mfp-usb)
   * [ Miscellaneous ](#miscellaneous-usb)
   * [ Modem ](#modem-usb)
   * [ Net/wireless ](#netwireless-usb)
   * [ Network ](#network-usb)
   * [ Phone ](#phone-usb)
   * [ Printer ](#printer-usb)
   * [ Scanner ](#scanner-usb)
   * [ Serial controller ](#serial-controller-usb)
   * [ Sound ](#sound-usb)
   * [ Touchpad ](#touchpad-usb)
   * [ Touchscreen ](#touchscreen-usb)
   * [ Tv card ](#tv-card-usb)
   * [ Ups ](#ups-usb)
   * [ Video ](#video-usb)
   * [ Webcam ](#webcam-usb)
   * [ Wireless ](#wireless-usb)
   * [ Xbox ](#xbox-usb)
   * [ Others ](#others-usb)

USB Devices
-----------

Count  — number of computers with this device installed,
Driver — driver in use for this device,
Probe  — latest probe ID of this device.

### Audio (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1043:857c | iCreate... | Xonar U7                     | 7     | snd_usb... | D969A32092 |
| 041e:322c | Creativ... | SB Omni Surround 5.1         | 5     | snd_usb... | D7D3D4F56B |
| 041e:3f19 | Creativ... | E-MU 0204 / USB              | 4     | snd_usb... | 12E892C5EB |
| 0b05:17a8 | ASUSTek... | ASUS Xonar Essence One       | 4     | usbhid     | 14FD5844A7 |
| 0b05:180d | ASUSTek... | STRIX SOUND CARD             | 4     | snd_usb... | 394C81B911 |
| 1235:8202 | Focusri... | Scarlett 2i2 USB             | 4     | snd_usb... | 623E24CBA5 |
| 1235:8205 | Focusri... | Scarlett Solo USB            | 4     | snd_usb... | 347D0DBF57 |
| 0b05:180f | ASUSTek... | XONAR SOUND CARD             | 3     | snd_usb... | D082929A57 |
| 041e:3f02 | Creativ... | E-Mu 0202                    | 2     | snd_usb... | D3E963E3FC |
| 041e:3f04 | Creativ... | E-Mu 0404                    | 2     | snd_usb... | DD8AABC952 |
| 046d:0a0b | Logitech   | ClearChat Pro USB            | 2     | snd_usb... | 4B9D75483A |
| 0499:1506 | Yamaha     | THR5                         | 2     | snd_usb... | CEBC9E7C7E |
| 0d8c:0004 | C-Media... | USB2.0 High-Speed True HD... | 2     | snd_usb... | 2E67236DBB |
| 1235:8200 | Focusri... | Scarlett 2i4 USB             | 2     | snd_usb... | 4E80D3D440 |
| 1397:0509 | BEHRING... | UMC404HD 192k                | 2     | snd_usb... | 5A8BAD0B76 |
| 152a:8750 | Thesyco... | D10                          | 2     | snd_usb... | 6E8096D588 |
| 20b1:3023 | XMOS       | X1S USB DAC                  | 2     | snd_usb... | 0A36000504 |
| b58e:0005 | Blue Mi... |                              | 2     | snd_usb... | CDC565D73D |
| 041e:3f0a | Creativ... | E-MU Tracker Pre / USB       | 1     | snd_usb... | 6996B0918E |
| 0499:1704 | Yamaha     | Steinberg UR44               | 1     | snd_usb... | D61154EABE |
| 0499:170b | Yamaha     | Steinberg UR242              | 1     | snd_usb... | 7EFEC12422 |
| 0582:00e6 | Roland     | EDIROL UA-25EX (Advanced ... | 1     | snd_usb... | FE2E4341C3 |
| 0582:012f | Roland     | QUAD-CAPTURE                 | 1     | snd_usb... | EF22676725 |
| 0644:8030 | TEAC       | US-1800                      | 1     |            | 75C7FE6B69 |
| 0763:4009 | M-Audio    | M-Track Plus                 | 1     | snd_usb... | B5DD161856 |
| 07fd:0005 | Mark of... | 24Ao                         | 1     | snd_usb... | 67D9C7BB6F |
| 095d:9205 | Polycom    | CX600                        | 1     | snd_usb... | 7B334F9FB4 |
| 0b05:17a3 | ASUSTek... | ROG ThunderBolt Audio        | 1     | snd_usb... | C784644541 |
| 0b05:17f3 | ASUSTek... | ASUS EONE MKII USB DAC       | 1     | usbhid     | F599EB6750 |
| 0bda:485a | Realtek... | USB Audio                    | 1     | snd_usb... | CEA175B6F3 |
| 0db0:7926 | Micro S... | USB2.0 High-Speed True HD... | 1     | snd_usb... | 065A4C6977 |
| 1235:8006 | Focusri... | Focusrite Scarlett 2i2       | 1     | snd_usb... | 5DC57BDE0C |
| 1235:8008 | Focusri... | Saffire 6                    | 1     | snd_usb... | 2E214EDAA5 |
| 1235:800a | Focusri... | Scarlett 2i4                 | 1     | snd_usb... | 9BC8084A3E |
| 1235:800e | Focusri... | iTrack Solo                  | 1     | snd_usb... | D1B3D0E283 |
| 1235:8016 | Focusri... | Focusrite Scarlett 2i2       | 1     |            | F16ADBF7F3 |
| 1235:801c | Focusri... | Scarlett Solo USB            | 1     | snd_usb... | 13A468E1E4 |
| 1235:8204 | Focusri... | Scarlett 18i8 2nd Gen        | 1     | snd_usb... | F192890A26 |
| 154e:3005 | D&M Hol... | PM7005                       | 1     | snd_usb... | 1E889CCEFD |
| 17cc:1001 | Native ... | Komplete Audio 6             | 1     | snd_usb... | 7D2057C89D |
| 17cc:1021 | Native ... | Traktor Audio 10             | 1     | snd_usb... | 7D2057C89D |
| 17cc:1330 | Native ... | Traktor Audio 2 MK2          | 1     | usbhid     | DDC37B050D |
| 194f:0103 | PreSonu... | AudioBox 1818 VSL            | 1     | snd_usb... | 5BE101218C |
| 20b1:0002 | XMOS       | USB Audio 2.0                | 1     | snd_usb... | 2EEB0DCBEF |
| 20b1:000a | XMOS       | xCORE USB Audio 2.0          | 1     | snd_usb... | D8358F3D32 |
| 233e:3002 | Aastra     | 6725ip                       | 1     | snd_usb... | B2D3CC175F |
| 2a39:3fb0 | RME        | Babyface Pro (71984822)      | 1     | snd_usb... | 9E0B67B32E |

### Bluetooth (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0a12:0001 | Cambrid... | Bluetooth Dongle (HCI mode)  | 569   | btusb      | 699B1F26E0 |
| 0b05:17cb | ASUSTek... | Broadcom BCM20702A0 Bluet... | 54    | btusb      | 3FB755ED84 |
| 8087:07dc | Intel      | Bluetooth Device             | 54    | btusb      | CACD7C9489 |
| 8087:0aa7 | Intel      | Bluetooth Device             | 54    | btusb      | 240876777B |
| 8087:0a2b | Intel      | Bluetooth Device             | 40    | btusb      | 0F78F0B23E |
| 0a5c:21e8 | Broadcom   | BCM20702A0 Bluetooth 4.0     | 39    | btusb      | A9406FFA03 |
| 0b05:179c | ASUSTek... | Bluetooth Device             | 38    | btusb      | BCF1D99475 |
| 0cf3:3005 | Qualcom... | AR3011 Bluetooth             | 38    | btusb      | AC377EFFFB |
| 8087:0a2a | Intel      | Bluetooth Device             | 36    | btusb      | 44204F310C |
| 1131:1004 | Integra... | Bluetooth Device             | 32    | btusb      | 6A3D0BEA9D |
| 0a5c:2101 | Broadcom   | BCM2045 Bluetooth            | 21    | btusb      | 7A1EBD3E5D |
| 8087:07da | Intel      | Bluetooth Device             | 21    | btusb      | 606AACED27 |
| 1131:1001 | Integra... | KY-BT100 Bluetooth Adapter   | 17    | btusb      | E5577D7A4A |
| 0b05:17b5 | ASUSTek... | BCM20702A0                   | 15    | btusb      | 571C27C038 |
| 0b05:17cf | ASUSTek... | BCM20702A0                   | 15    | btusb      | EEE7D322DF |
| 04ca:3006 | Lite-On... | Lite-On Bluetooth Device     | 14    | ath3k, ... | 7750967159 |
| 0b05:180a | ASUSTek... | Broadcom BCM20702 Single-... | 14    | btusb      | 7F1BE20709 |
| 8087:0aaa | Intel      | Bluetooth Device 9460/9560   | 14    | btusb      | 328F236B37 |
| 0b05:1825 | ASUSTek... | Qualcomm Bluetooth 4.1       | 13    | btusb      | B0D2314507 |
| 0b05:185c | ASUSTek... | Bluetooth Radio              | 12    | btusb      | 8560134FAB |
| 13d3:3414 | IMC Net... | Bluetooth Radio              | 12    | btusb      | 50919B2DDB |
| 0cf3:3002 | Qualcom... | AR3011 Bluetooth             | 11    | ath3k, ... | 5C0FC04230 |
| 0cf3:0036 | Qualcom... | Qualcomm Atheros Bluetoot... | 10    | ath3k, ... | 65A9CF6ADE |
| 8087:0025 | Intel      | Bluetooth Device             | 10    | btusb      | B88A1B7070 |
| 0bda:b008 | Realtek... | Bluetooth Radio              | 9     | btusb      | 200C878097 |
| 19ff:0239 | Dynex      | BCM20702A0                   | 8     | btusb      | EA54CD12AB |
| 0a5c:2100 | Broadcom   | Bluetooth 2.0+eDR dongle     | 7     | btusb      | 594F1F2F61 |
| 0b05:17d0 | ASUSTek... | Bluetooth Device             | 7     | ath3k, ... | BA46A03C82 |
| 0c10:0000 | Bluetoo... | Bluetooth Device             | 7     | btusb      | BDB727808F |
| 0cf3:3004 | Qualcom... | AR3012 Bluetooth 4.0         | 7     | ath3k, ... | 35C9173099 |
| 0cf3:e004 | Qualcom... | Bluetooth USB Host Contro... | 7     | ath3k, ... | 84E10F6B4C |
| 0cf3:e005 | Qualcom... | Qualcomm Atheros Bluetoot... | 7     | ath3k, ... | 3548830389 |
| 13d3:3295 | IMC Net... | Bluetooth Module             | 7     | btusb      | C73B25ED21 |
| 05ac:8215 | Apple      | Built-in Bluetooth 2.0+ED... | 6     | btusb      | 6710749660 |
| 0cf3:e007 | Qualcom... | Qualcomm Atheros Bluetoot... | 6     | btusb      | DB32491DFE |
| 13d3:3404 | IMC Net... | BCM20702A0                   | 6     | btusb      | 7A0FC72547 |
| 0a5c:2121 | Broadcom   | BCM2210 Bluetooth            | 5     | btusb      | 3BC0DC53EF |
| 0a5c:2148 | Broadcom   | BCM92046DG-CL1ROM Bluetoo... | 5     | btusb      | F8301FFE57 |
| 0bda:b00a | Realtek... | Bluetooth Radio              | 5     | btusb      | 33A03DF803 |
| 0db0:a871 | Micro S... | Bluetooth Device             | 5     | btusb      | 0F152D6AD7 |
| 1310:0001 | Roper      | Class 1 Bluetooth Dongle     | 5     | btusb      | 156D974CA7 |
| 046d:c709 | Logitech   | BT Mini-Receiver (HCI mode)  | 4     | btusb      | 9E7F3486F0 |
| 0a5c:200a | Broadcom   | BCM2035 Bluetooth dongle     | 4     | btusb      | 0C90B25A45 |
| 0a5c:2123 | Broadcom   | Bluetooth dongle             | 4     | btusb      | B1A4F2F270 |
| 0a5c:2190 | Broadcom   | Bluetooth 3.0 Device         | 4     | btusb      | AF1613A447 |
| 0a5c:2198 | Broadcom   | Bluetooth 3.0 Device         | 4     | btusb      | 3BCD649400 |
| 0a5c:21f1 | Broadcom   | HP Portable Bumble Bee       | 4     | btusb      | D58DD0D4E0 |
| 0b05:1715 | ASUSTek... | 2045 Bluetooth 2.0 Device... | 4     | btusb      | 1192B16B2A |
| 0bda:0821 | Realtek... | Bluetooth Radio              | 4     | btusb      | DD20758A89 |
| 0cf3:3008 | Qualcom... | Bluetooth (AR3011)           | 4     | ath3k, ... | B8ECD9CD1E |
| 0cf3:e009 | Qualcom... | Qualcomm Atheros Bluetoot... | 4     | btusb      | 2454AEB8AB |
| 03f0:171d | Hewlett... | Bluetooth 2.0 Interface [... | 3     | btusb      | EF835695B4 |
| 050d:016a | Belkin ... | Bluetooth Mini Dongle        | 3     | btusb      | 68BABB69CB |
| 05ac:1000 | Apple      | Bluetooth HCI MacBookPro ... | 3     | usbhid     | A3AC5B74CF |
| 05ac:8206 | Apple      | Bluetooth HCI                | 3     | btusb      | AA53E34C56 |
| 0a5c:218c | Broadcom   | Bluetooth 3.0 Dongle         | 3     | btusb      | F88D5A4221 |
| 0a5c:21ec | Broadcom   | BCM20702A0                   | 3     | btusb      | 46EA2E591E |
| 0b05:17dc | ASUSTek... | Bluetooth Radio              | 3     | btusb      | E7DA324AC8 |
| 0b05:1868 | ASUSTek... | Bluetooth Device             | 3     | btusb      | 5F582A0578 |
| 0e5e:6622 | Conwise... | CW6622                       | 3     | btusb      | B4A9C29659 |
| 0461:4d75 | Primax ... | Rocketfish RF-FLBTAD Blue... | 2     | btusb      | D1B680DC39 |
| 04ca:300b | Lite-On... | Lite-On Bluetooth Device     | 2     | ath3k, ... | 3458AC22F5 |
| 050d:0131 | Belkin ... | Bluetooth Device with tra... | 2     | btusb      | 3B126F53B4 |
| 05ac:828d | Apple      | Bluetooth USB Host Contro... | 2     | btusb      | EEE8F584D6 |
| 0a5c:216c | Broadcom   | BCM43142A0 Bluetooth Device  | 2     | btusb      | 9F366DE52D |
| 0a5c:216d | Broadcom   | BCM43142A0 Bluetooth 4.0     | 2     | btusb      | 4D6F67B025 |
| 0a5c:217d | Broadcom   | HP Bluethunder               | 2     | btusb      | 376BF41CE2 |
| 0bb4:0306 | HTC (Hi... | BCM2045A0                    | 2     | btusb      | C572988845 |
| 0bda:b006 | Realtek... | Bluetooth Radio              | 2     | btusb      | DE8AA8DA97 |
| 0bda:b00b | Realtek... | Bluetooth Radio              | 2     | btusb      | 0FDAC41856 |
| 0bda:b723 | Realtek... | Bluetooth Radio              | 2     | btusb      | E1C8A6CC14 |
| 0bda:b728 | Realtek... | Bluetooth Radio              | 2     | btusb      | BC46ED7DF1 |
| 0bda:b739 | Realtek... | Bluetooth Radio              | 2     | btusb      | 0784C6115A |
| 0bda:c024 | Realtek... | Bluetooth Radio              | 2     | btusb      | 52ED0BAFBD |
| 0cf3:e300 | Qualcom... | Qualcomm Atheros Bluetoot... | 2     | btusb      | DEE8563CD4 |
| 13d3:3304 | IMC Net... | Asus Integrated Bluetooth... | 2     | ath3k, ... | C784644541 |
| 13d3:3410 | IMC Net... | Bluetooth Radio              | 2     | btusb      | DC68A4A236 |
| 13d3:3491 | IMC Net... | Bluetooth Device             | 2     | btusb      | 9CF83B2B61 |
| 1caa:0001 |            | Bluetooth Device             | 2     | btusb      | 57098A751C |
| 2001:f111 | D-Link     | DBT-122 Bluetooth adapter    | 2     | btusb      | A4119B2AD7 |
| 413c:8143 | Dell       | BCM20702A0                   | 2     | btusb      | 2272E1FA1D |
| 03f0:d404 | Hewlett... | Bluetooth Adapter            | 1     | btusb      | 85F1B83B30 |
| 0489:e00f | Foxconn... | Foxconn T77H114 BCM2070 [... | 1     | btusb      | 26B75090A9 |
| 050d:065a | Belkin ... | F8T065BF Mini Bluetooth 4... | 1     | btusb      | CDC565D73D |
| 05ac:8289 | Apple      | Bluetooth USB Host Contro... | 1     | btusb      | 4EFC7FBDC6 |
| 05ac:828b | Apple      | Bluetooth USB Host Contro... | 1     | btusb      | 4EB5F0A330 |
| 05ac:8290 | Apple      | Bluetooth Host Controller    | 1     | usbhid     | E0F7798547 |
| 07d1:f101 | D-Link ... | DBT-122 Bluetooth            | 1     | btusb      | 7A74EB0F6F |
| 0930:0214 | Toshiba    | Askey Bluetooth Module       | 1     | btusb      | 4D5C23AB21 |
| 0a5c:2020 | Broadcom   | Bluetooth dongle             | 1     | btusb      | 28EA4213CB |
| 0a5c:2039 | Broadcom   | BCM2045 Bluetooth            | 1     | btusb      | D5BB47BC4A |
| 0a5c:2046 | Broadcom   | Bluetooth Device             | 1     | btusb      | 156D974CA7 |
| 0a5c:2120 | Broadcom   | 2045 Bluetooth 2.0 USB-UH... | 1     | btusb      | E7164FCDA2 |
| 0a5c:2146 | Broadcom   | BCM92046DG-CL1ROM            | 1     | btusb      | 0A6AE83FB9 |
| 0a5c:2154 | Broadcom   | BCM92046DG-CL1ROM Bluetoo... | 1     | btusb      | AEE2455DD7 |
| 0a5c:21b1 | Broadcom   | HP Bluetooth Module          | 1     | btusb      | DF47C88DB6 |
| 0a5c:21b4 | Broadcom   | BCM2070 Bluetooth 2.1 + EDR  | 1     | btusb      | 01FE7C5779 |
| 0a5c:21e2 | Broadcom   | BCM920702 Bluetooth 4.0 Z... | 1     | btusb      | C3ACCF7164 |
| 0a5c:21e3 | Broadcom   | HP Portable Valentine        | 1     | btusb      | 730A048DD9 |
| 0a5c:21fb | Broadcom   | BCM20702A0                   | 1     | btusb      | 7923D4133C |

### Camera (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:0825 | Logitech   | Webcam C270                  | 295   | uvcvideo   | 8C4047657D |
| 0ac8:3420 | Z-Star ... | Venus USB2.0 Camera          | 236   | uvcvideo   | B0B570F44B |
| 0ac8:c40a | Z-Star ... | A4 TECH USB2.0 PC Camera J   | 144   | uvcvideo   | 78E822AD79 |
| 0c45:6340 | Microdia   | Camera                       | 119   | uvcvideo   | C0B50F9AE1 |
| 0ac8:3450 | Z-Star ... | Vimicro USB Camera (Altair)  | 99    | uvcvideo   | F0CE444FB7 |
| 046d:0819 | Logitech   | Webcam C210                  | 77    | uvcvideo   | B0D2314507 |
| 046d:082b | Logitech   | Webcam C170                  | 74    | uvcvideo   | 48FEA246F9 |
| 046d:081b | Logitech   | Webcam C310                  | 73    | uvcvideo   | 39C0FBE126 |
| 18ec:3399 | Arkmicr... | USB2.0 PC CAMERA             | 73    | uvcvideo   | 969A371A99 |
| 046d:0826 | Logitech   | HD Webcam C525               | 70    | snd_usb... | 699B1F26E0 |
| 093a:2700 | Pixart ... | A4 TECH PC Camera            | 66    | uvcvideo   | C7934C20A0 |
| 04e8:6860 | Samsung... | GT-I9100 Phone [Galaxy S ... | 61    | usbfs      | 2CA17B3AA6 |
| 046d:082d | Logitech   | HD Pro Webcam C920           | 55    | snd_usb... | BDA3B81C14 |
| 1e4e:0102 | Cubeternet | GL-UPC822 UVC WebCam         | 52    | uvcvideo   | E1D8ADF3C9 |
| 046d:0829 | Logitech   | Webcam C110                  | 46    | uvcvideo   | D6764F0C12 |
| 045e:0779 | Microsoft  | LifeCam HD-3000              | 41    | uvcvideo   | 7FDD5F778A |
| 0c45:62c0 | Microdia   | Sonix USB 2.0 Camera         | 36    | uvcvideo   | 7A74EB0F6F |
| 046d:082c | Logitech   | HD Webcam C615               | 31    | uvcvideo   | EA54CD12AB |
| 1871:0141 | Aveo Te... | USB2.0 Camera                | 31    | snd_usb... | 8871866F65 |
| 046d:081d | Logitech   | HD Webcam C510               | 30    | snd_usb... | 23ACB95370 |
| 1908:2310 | GEMBIRD    | USB2.0 PC CAMERA             | 30    | uvcvideo   | 35C9173099 |
| 046d:0821 | Logitech   | HD Webcam C910               | 27    | snd_usb... | B481805845 |
| 05ac:12a8 | Apple      | iPhone5/5C/5S/6              | 27    | usbfs, ... | B3224EB5FC |
| 1908:2311 | GEMBIRD    | USB2.0 PC CAMERA             | 27    | uvcvideo   | E1374515F8 |
| 1871:0101 | Aveo Te... | UVC camera (Bresser micro... | 26    | uvcvideo   | 0FA01E4D66 |
| 045e:0766 | Microsoft  | LifeCam VX-800               | 25    | uvcvideo   | 14FB85B134 |
| 046d:0802 | Logitech   | Webcam C200                  | 25    | uvcvideo   | 873CD3A203 |
| 046d:0817 | Logitech   | Logitech Webcam C100         | 25    | uvcvideo   | E29EC46FD7 |
| 0bda:58b0 | Realtek... | AF FULL HD 1080P Webcam      | 24    | uvcvideo   | 6A9086BF86 |
| 0c45:62f1 | Microdia   | USB 2.0 Camera               | 24    | uvcvideo   | 4BD42BC14C |
| 046d:080f | Logitech   | Webcam C120                  | 23    | uvcvideo   | 55981AF24A |
| 046d:0824 | Logitech   | Logitech Webcam C160         | 23    | uvcvideo   | A4FB239111 |
| 046d:0804 | Logitech   | Webcam C250                  | 20    | uvcvideo   | F978F464EB |
| 046d:0809 | Logitech   | Webcam Pro 9000              | 19    | uvcvideo   | F16EC522A6 |
| 1e4e:0103 | Cubeternet | USB2.0 Camera                | 19    | uvcvideo   | 8208DA7F5B |
| 045e:075d | Microsoft  | LifeCam Cinema               | 18    | snd_usb... | 19AB08EB6F |
| 045e:0761 | Microsoft  | LifeCam VX-2000              | 17    | uvcvideo   | F048270617 |
| 045e:076d | Microsoft  | LifeCam HD-5000              | 17    | uvcvideo   | 09CC6BAB56 |
| 046d:0990 | Logitech   | QuickCam Pro 9000            | 17    | snd_usb... | 7A74EB0F6F |
| 0c45:62e0 | Microdia   | MSI Starcam Racer            | 16    | uvcvideo   | 9204CB3BAC |
| 0ac8:3500 | Z-Star ... | Full HD 1080P PC Camera      | 14    | snd_usb... | 00FC2B5C89 |
| 1e4e:0100 | Cubeternet | WebCam                       | 14    | uvcvideo   | F2E67158F7 |
| 05e3:0510 | Genesys... | USB2.0 Digital Camera        | 13    | uvcvideo   | 6756E83706 |
| 0458:7089 | KYE Sys... | Genius FaceCam 320           | 12    | snd_usb... | FFB74FCE84 |
| 046d:0807 | Logitech   | Webcam B500                  | 12    | uvcvideo   | 87DAB1466B |
| 093a:2628 | Pixart ... | Multimedia audio controller  | 12    | gspca_p... | 59233E4EE7 |
| 0ac8:332d | Z-Star ... | Vega USB 2.0 Camera          | 11    | snd_usb... | 6E181005C4 |
| 1871:0d01 | Aveo Te... | USB2.0 Camera                | 11    | uvcvideo   | 6D29C74B92 |
| 045e:074a | Microsoft  | LifeCam VX-500 [1357]        | 9     | uvcvideo   | 42CC2F59E6 |
| 058f:3841 | Alcor M... | USB 2.0 PC Camera            | 9     | snd_usb... | 17D9D093D4 |
| 0c45:6310 | Microdia   | Sonix USB 2.0 Camera         | 9     | uvcvideo   | 333E36753A |
| 041e:4088 | Creativ... | Live! Cam Chat HD [VF0700]   | 8     | uvcvideo   | 09AC5ADCCD |
| 0458:605e | KYE Sys... | iSlim 320                    | 7     | snd_usb... | D8D0364599 |
| 0458:706d | KYE Sys... | Genius iSlim 2000AF V2       | 7     | uvcvideo   | DA78C40BBA |
| 0458:7076 | KYE Sys... | Genius FaceCam 312           | 7     | snd_usb... | A395E4773B |
| 045e:0810 | Microsoft  | Microsoft LifeCam HD-3000    | 7     | snd_usb... | D8DFF6CEC0 |
| 046d:0805 | Logitech   | Webcam C300                  | 7     | uvcvideo   | AC522E55CE |
| 046d:0843 | Logitech   | Webcam C930e                 | 7     | snd_usb... | 3A8D19C8FC |
| 046d:09a4 | Logitech   | QuickCam E 3500              | 7     | uvcvideo   | 1CEF6B2E8C |
| 04f2:a133 | Chicony... | Gateway Webcam               | 7     | uvcvideo   | D1DED92F20 |
| 04f2:b34a | Chicony... | HP High Definition 1MP We... | 7     | uvcvideo   | E49892B06D |
| 18ec:3299 | Arkmicr... | Webcam Carrefour             | 7     | snd_usb... | 8A024C6E9C |
| 1c4f:3002 | SiGma M... | WebCam SiGma Micro           | 7     | uvcvideo   | 5AEB4ABFE6 |
| 1e4e:0110 | Cubeternet | USB2.0 Camera                | 7     | uvcvideo   | 5E414BC536 |
| 0421:0661 | Nokia M... | Lumia 620/920                | 6     | usbfs      | E3EF0760F5 |
| 0458:7060 | KYE Sys... | iSlim 1320                   | 6     | uvcvideo   | 1D3A3FE6DF |
| 045e:0772 | Microsoft  | LifeCam Studio               | 6     | uvcvideo   | C2D1F5C35A |
| 046d:0992 | Logitech   | QuickCam Communicate Deluxe  | 6     | uvcvideo   | BCF1D99475 |
| 046d:09a5 | Logitech   | Quickcam 3000 For Business   | 6     | uvcvideo   | DE463D111A |
| 090c:71b3 | Silicon... | SM731 Camera                 | 6     | uvcvideo   | 0250180463 |
| 093a:2800 | Pixart ... | USB2.0_Camera                | 6     | uvcvideo   | 944B88BC74 |
| 03f0:e207 | Hewlett... | Webcam HD 2300               | 5     | uvcvideo   | 160C2DE51F |
| 0421:06fc | Nokia M... | Lumia 640 XL Dual SIM (RM... | 5     |            | 28A8C15F9F |
| 0458:505e | KYE Sys... | Genius iSlim 330             | 5     | snd_usb... | 700FC16AC3 |
| 0458:705a | KYE Sys... | Asus USB2.0 Webcam           | 5     | uvcvideo   | 726003B460 |
| 0458:7061 | KYE Sys... | Genius iLook 1321 V2         | 5     | uvcvideo   | 38FD682351 |
| 0458:707c | KYE Sys... | eFace 1300                   | 5     | uvcvideo   | F19AABDAFF |
| 0458:707d | KYE Sys... | FaceCam 311                  | 5     | uvcvideo   | 1192B16B2A |
| 0458:708f | KYE Sys... | FaceCam 1000X                | 5     | snd_usb... | D7DFB1D466 |
| 045e:0770 | Microsoft  | LifeCam-VX700 v2.0           | 5     | snd_usb... | A010B34C1D |
| 046d:085c | Logitech   | C922 Pro Stream Webcam       | 5     | snd_usb... | 87B482DE1D |
| 046d:0994 | Logitech   | QuickCam Orbit/Sphere AF     | 5     | snd_usb... | 93A39661EC |
| 0471:20d0 | Philips... | SPZ2000 Webcam [PixArt PA... | 5     | uvcvideo   | D687B284D2 |
| 1004:633e | LG Elec... | G2/G3 Android Phone [MTP/... | 5     | usbfs      | FD5E41554C |
| 1e4e:0109 | Cubeternet | USB Microscope               | 5     | uvcvideo   | 459DC2D57F |
| 041e:406d | Creativ... | VF0560 Live! Cam Optia AF    | 4     | snd_usb... | 4191570740 |
| 041e:4083 | Creativ... | Live! Cam Socialize [VF0640] | 4     | uvcvideo   | 3ADD10D9B0 |
| 041e:4095 | Creativ... | Live! Cam Sync HD [VF0770]   | 4     | uvcvideo   | 0C62C4C191 |
| 0458:704d | KYE Sys... | Slim 1322AF                  | 4     | uvcvideo   | CB040392E8 |
| 0458:705f | KYE Sys... | iSlim 321R                   | 4     | snd_usb... | 6C7C70F0E2 |
| 0458:7071 | KYE Sys... | iSlim 1300 V2                | 4     | snd_usb... | 5E946FBF6A |
| 045e:0728 | Microsoft  | LifeCam VX-5000              | 4     | uvcvideo   | F87F872590 |
| 046d:0836 | Logitech   | B525 HD Webcam               | 4     | snd_usb... | 4BDD2CB29F |
| 046d:09a1 | Logitech   | QuickCam Communicate MP/S... | 4     | snd_usb... | 4902AA6164 |
| 046d:09c1 | Logitech   | QuickCam Deluxe for Noteb... | 4     | snd_usb... | 950955DCE7 |
| 0471:20b5 | Philips... | SPZ2500                      | 4     | uvcvideo   | CEADCE6F2F |
| 04f2:b187 | Chicony... | HP Webcam                    | 4     | uvcvideo   | 45542209AF |
| 04f2:b28d | Chicony... | HP 0.3MP Webcam              | 4     | uvcvideo   | DE224150CF |
| 058f:3880 | Alcor M... | USB 2.0 PC Camera            | 4     | uvcvideo   | E0DF762471 |
| 093a:2627 | Pixart ... | FaceCam 300                  | 4     | gspca_p... | 70046F217B |

### Card reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:0129 | Realtek... | RTS5129 Card Reader Contr... | 70    | rtsx_usb   | F3279F9D14 |
| 0aec:3260 | Neodio ... | 7-in-1 Card Reader           | 7     | uas, us... | A3B6AB3380 |
| 0bda:0139 | Realtek... | RTS5139 Card Reader Contr... | 6     | rtsx_usb   | EDB984C4E6 |
| 072f:9000 | Advance... | ACR38 AC1038-based Smart ... | 4     | usbfs      | 590B559F72 |
| 0d8c:5200 | C-Media... | Mass Storage Controller(0... | 4     |            | 38C8446DA8 |
| 0c4b:0500 | Reiner ... | cyberJack RFID standard d... | 2     | usbfs      | 2F2703FBEB |
| 047b:020b | Silitek    | SK-3105 SmartCard Reader     | 1     | usbhid     | 986AC800BA |
| 0b0c:003f | Todos AB   | Todos C400 smartcard cont... | 1     |            | 251D958CA9 |
| 0bda:0150 | Realtek... | USB 2.0 Card Reader          | 1     |            | 3C44204AA0 |
| 0c4b:0501 | Reiner ... | cyberJack RFID comfort du... | 1     |            | 37BE65DDB4 |
| 0ca6:0010 | Castles... | EZUSB PC/SC Smart Card Re... | 1     |            | 21FB8F59A4 |

### Cdrom (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 8564:1000 | Transcend  | TS32GJF370 JetFlash 32GB     | 444   | uas, us... | B39AC90CA0 |
| 174c:55aa | ASMedia... | Name: ASM1051E SATA 6Gb/s... | 76    | usb_sto... | 20EE58CBC8 |
| 152d:2329 | JMicron... | JM20329 SATA Bridge          | 71    | usb_sto... | D569843A2D |
| 04c5:2028 | Fujitsu    | External HDD 128GB           | 49    | uas, us... | 240876777B |
| 152d:2338 | JMicron... | JM20337 Hi-Speed USB to S... | 45    | uas, us... | 3B6656BB11 |
| 1bcf:0c31 | Sunplus... | SPIF30x Serial-ATA bridge    | 38    | uas, us... | 92D3DF20F4 |
| 0e8d:1806 | MediaTek   | Samsung SE-208AB Slim Por... | 28    | usb_sto... | 046006226D |
| 0e8d:1887 | MediaTek   | DVDRAM GP60NS50              | 22    | usb_sto... | 1CC81789AC |
| 0e8d:1836 | MediaTek   | Samsung SE-S084 Super Wri... | 18    | usb_sto... | 6A3D0BEA9D |
| 0928:0010 | PLX Tec... | Ext Hard Disk 500GB          | 17    | uas, us... | BE83B1B58F |
| 19d2:1403 | ZTE WCD... | USB SCSI CD-ROM              | 14    | uas, us... | 58D9E7C387 |
| 13fd:0840 | Initio     | INIC-1618L SATA 100GB        | 13    | uas, us... | 1D7422A66F |
| 13fd:1040 | Initio     | INIC-1511L PATA Bridge       | 11    | usb_sto... | 9687F320FD |
| 152d:0578 | JMicron... | JMS567 SATA 6Gb/s bridge     | 11    | uas, us... | 4E526BB85F |
| 152e:1640 | LG (HLDS)  | DVDRAM GE20NU11              | 11    | usb_sto... | E583B3127A |
| 152d:2339 | JMicron... | JM20339 SATA Bridge          | 10    | uas, us... | 46324F79EA |
| 19d2:1410 | ZTE WCD... | LTE Technologies MSM         | 10    | cdc_ether  | C820DDD39E |
| 1c6b:a223 | Philips... | SDRW-08D2S-U                 | 10    | usb_sto... | CDC565D73D |
| 12d1:107e | Huawei ... | File-CD Gadget               | 9     | uas, us... | 341917FD42 |
| 04fc:0c25 | Sunplus... | SATALink SPIF225A 320GB      | 8     | uas, us... | DA55F26B0D |
| 0781:5406 | SanDisk    | Cruzer Micro U3 2GB          | 8     | uas, us... | 6E8256A0D7 |
| 13fd:1640 | Initio     | INIC-1610L SATA Bridge       | 8     | usb_sto... | BDEC0242AA |
| 152e:2507 | LG (HLDS)  | PL-2507 IDE Controller       | 8     | uas, us... | E8794A0676 |
| 0e8d:1956 | MediaTek   | Samsung SE-506 Portable B... | 6     | usb_sto... | 32AFE6A3D4 |
| 13fd:0940 | Initio     | ASUS SBW-06D2X-U 500GB       | 6     | uas, us... | 48DACF6BD2 |
| 13fd:2040 | Initio     | Samsung Writemaster exter... | 5     | usb_sto... | 4E2CEB128E |
| 14cd:6600 | Super Top  | M110E PATA bridge            | 5     | usb_sto... | 6CF60F8AEF |
| 1c6b:a222 | Philips... | DVD Writer Slimtype eTAU108  | 5     | uas, us... | 4768666752 |
| 048d:1176 | Integra... | USB Flash Disk 16GB          | 4     | uas, us... | C7BCF092A2 |
| 04cf:8818 | Myson C... | USB2.0 to ATAPI Bridge Co... | 4     | usb_sto... | F45F113932 |
| 04e8:685b | Samsung... | GT-I9100 Phone [Galaxy S ... | 4     | uas, us... | DC58FEE893 |
| 067b:2571 | Prolifi... | DVDRAM GE24NU30              | 4     | usb_sto... | A5DD292F0E |
| 0bda:1a2b | Realtek... | USB Disk autorun             | 4     | uas, us... | B4020C7021 |
| 13fd:3940 | Initio     | external DVD burner ECD81... | 4     | uas, us... | 277055C3E7 |
| 13fd:3e40 | Initio     | ZALMAN ZM-VE350 256GB        | 4     | uas, us... | 61C75BCC41 |
| 152d:2519 | JMicron... | Transcend                    | 4     | uas, us... | 14FEBB13AA |
| 174c:1351 | ASMedia... | DVDRW DA8AESH                | 4     | uas        | 55981AF24A |
| 1f75:0611 | Innosto... | EZEX-75WN4A0 1TB             | 4     | uas, us... | ACDBCD6A13 |
| 0476:059b | AESP       | DVD RW AD-7580S              | 3     | usb_sto... | B297051783 |
| 05e3:0701 | Genesys... | USB 2.0 IDE Adapter          | 3     | usb_sto... | 15252DCF05 |
| 152e:2571 | LG (HLDS)  | DVDRAM GP08NU6B              | 3     | uas, us... | 862146DFEC |
| 059b:0155 | Iomega     | DVDRAM GSA-4081B             | 2     | uas, us... | 3EE5BD924C |
| 05e3:0719 | Genesys... | SATA adapter                 | 2     | uas, us... | 4A6EB1071A |
| 0b05:7774 | ASUSTek... | Zenfone GO (ZB500KL) (RND... | 2     | rndis_host | 1AA58D5FEA |
| 0bb4:0f25 | HTC (Hi... | One M8                       | 2     | usb_sto... | F88121C780 |
| 0e8d:1807 | MediaTek   | SDRW-08D2S-U                 | 2     | usb_sto... | 0784C6115A |
| 12d1:1052 | Huawei ... | MT7-L09                      | 2     | uas, us... | 8B0A240FA2 |
| 13fd:0841 | Initio     | Samsung SE-T084M DVD-RW      | 2     | usb_sto... | 84E58AC976 |
| 13fd:0842 | Initio     | CDDVDW SE-T084P              | 2     | usb_sto... | F595A31A86 |
| 1782:5d03 | Spreadt... | umopenphone 34GB             | 2     | uas, us... | 1556198BA1 |
| 2e04:c025 | Linux      | File-CD Gadget               | 2     | uas, us... | D5AF14209B |
| 2e04:c026 | Linux      | File-CD Gadget               | 2     | uas, us... | 347D0DBF57 |
| 413c:9016 | Dell       | DVD+/-RW DW316               | 2     | usb_sto... | CC88E139CC |
| 03f0:2027 | Hewlett... | Virtual DVD-ROM              | 1     | usb_sto... | D9B1EC3C37 |
| 0409:0056 | NEC Com... | DW-224E-C                    | 1     | usb_sto... | 53ABDA4642 |
| 0411:01b8 | BUFFALO    | BD-RW BDR-209M               | 1     | usb_sto... | 692C95368A |
| 046e:3005 | Behavio... | Mass Storage Device          | 1     | uas, us... | 6960E1BAAE |
| 0471:0823 | Philips... | DVD RW AD-7200A              | 1     | uas, us... | F3D9E45F07 |
| 0471:082c | Philips... | SPD3200L1                    | 1     | uas, us... | FE5F95F298 |
| 057c:62ff | AVM        | AVM Fritz!WLAN USB (in CD... | 1     | usb_sto... | D772023A07 |
| 058f:6395 | Alcor M... | DV-W28SS-R                   | 1     | uas, us... | 26CFAAC001 |
| 059b:0252 | Iomega     | Optical                      | 1     | usb_sto... | 08A4E0713E |
| 05c6:6000 | Qualcomm   | Siemens SG75                 | 1     | option,... | 90322CEA13 |
| 05c6:92fe | Qualcomm   | Disk                         | 1     | uas, us... | 3F9E332E1D |
| 060f:1007 | Joinsoo... | DVD A DS8A4S                 | 1     | usb_sto... | 853CE95D3B |
| 06e6:c200 | Tiger J... | Internet Phone               | 1     | uas, us... | 004DE30AFC |
| 0718:4006 | Imation    | 8x Slim DVD Multi-Format ... | 1     | usb_sto... | 9CBBBDBFD2 |
| 0789:00cc | Logitec    | VIRTUAL CD-ROM               | 1     | uas, us... | 8F38D85DBF |
| 0840:009d | Argosy ... | CD/DVDW SN-W082B             | 1     | usb_sto... | 38EF812F4E |
| 08e4:0150 | Pioneer    | DVD-RW DVR-XD09              | 1     | usb_sto... | E8C3307D08 |
| 08e4:017a | Pioneer    | BD-RW BDR-XD05               | 1     | usb_sto... | 528F9F1A24 |
| 08e4:0193 | Pioneer    | DVD-RW DVR-XT11              | 1     | uas, us... | 75DE067DF6 |
| 0930:0c06 | Toshiba    | SuperMultiPA3761             | 1     | usb_sto... | D49249D475 |
| 093b:004a | Plextor    | DVDR PX-L611U                | 1     | uas, us... | 14B329796C |
| 093b:004e | Plextor    | DVDR PX-650US                | 1     | usb_sto... | 8991709C89 |
| 0b05:5600 | ASUSTek... | File-CD Gadget               | 1     | uas, us... | 8C2895839E |
| 0e8d:2870 | MediaTek   | Flash autorun                | 1     | uas, us... | 2A5D8331CC |
| 0ea0:2108 | Ours Te... | ShareLink                    | 1     | uas, us... | 6F063AE988 |
| 0ecd:a100 | Lite-On IT | LDW-411SX DVD/CD Rewritab... | 1     | uas, us... | 48DACF6BD2 |
| 1058:070a | Western... | My Passport Essential (WD... | 1     | usb_sto... | C0C0540FA5 |
| 12d1:107d | Huawei ... | File-CD Gadget               | 1     | uas, us... | F22EC924DE |
| 12d1:107f | Huawei ... | File-CD Gadget               | 1     | uas, us... | AD36B40663 |
| 12d1:1082 | Huawei ... | File-CD Gadget               | 1     | uas, us... | 7FC111342C |
| 13fd:1140 | Initio     | DVD/CDRW UJDA770             | 1     | usb_sto... | 9844AD88B9 |
| 152d:0562 | JMicron... | MK2035GSS                    | 1     | uas, us... | B5A237AA3F |
| 152d:2337 | JMicron... | ATA/ATAPI Bridge             | 1     | uas, us... | 726B0FFF82 |
| 16d5:6502 | AnyDATA    | CDMA/UMTS/GPRS modem         | 1     | option,... | 43E409C54B |
| 17ef:74a6 | Lenovo     | File-Stor Gadget             | 1     | uas, us... | 7F9B935EF8 |
| 17ef:7a19 | Lenovo     | File-CD Gadget               | 1     | uas, us... | 73402379FD |
| 19d2:0083 | ZTE WCD... | MF190                        | 1     | uas, us... | 6011EA9E15 |
| 19d2:0501 | ZTE WCD... | File-Stor Gadget             | 1     | usb_sto... | AA10363A06 |
| 19d2:ffaf | ZTE WCD... | Android                      | 1     | uas, us... | 3E52D926EF |
| 19d2:ffcc | ZTE WCD... | Android                      | 1     | uas, us... | 60D3B5BB06 |
| a2a6:7825 | HL-DT-ST   | BD-RE WH14NS40               | 1     | uas, us... | 023D68FC85 |

### Chipcard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0529:0620 | Aladdin... | Token JC                     | 10    | usbfs      | 0CFA900AB7 |
| 058f:9520 | Alcor M... | EMV Certified Smart Card ... | 8     |            | 936BEEBE68 |
| 072f:90cc | Advance... | ACR38 SmartCard Reader       | 6     | usbfs      | 33F663A020 |
| 058f:9540 | Alcor M... | AU9540 Smartcard Reader      | 5     |            | D75C4308F7 |
| 0bda:0165 | Realtek... | Smart Card Reader Interface  | 5     |            | 7B99FD4C95 |
| 076b:1021 | OmniKey    | CardMan 1021                 | 4     | usbfs      | 3D9E77AE8A |
| 04e6:5116 | SCM Mic... | SCR331-LC1 / SCR3310 Smar... | 3     |            | B511052CC4 |
| 1a44:0001 | VASCO D... | Digipass 905 SmartCard Re... | 3     |            | AE29C83D96 |
| 072f:2200 | Advance... | ACR122U                      | 2     | pn533_usb  | C13D443BF5 |
| 072f:b000 | Advance... | ACR3901U                     | 2     |            | A44B651190 |
| 076b:3021 | OmniKey    | CardMan 3121                 | 2     | usbfs      | B45F44A0F7 |
| 08e6:3437 | Gemalto... | GemPC Twin SmartCard Reader  | 2     |            | BB385761F8 |
| 0c4b:9102 | Reiner ... | cyberJack RFID basis cont... | 2     |            | 66EEFE25AF |
| 0ca6:00a0 | Castles... | EZCCID Smart Card Reader     | 2     |            | 3DC7A36CB3 |
| 0dc3:1004 | Athena ... | ASEDrive CCID                | 2     |            | FB13C4ACE3 |
| 0403:e3b4 | Future ... | Parsec Desktop Reader PR-... | 1     |            | 775160993D |
| 04e6:5119 | SCM Mic... | SCR3340 - ExpressCard54 S... | 1     | usbfs      | 96AAA39135 |
| 04e6:e001 | SCM Mic... | SCR331 SmartCard Reader      | 1     |            | D3D8B39015 |
| 072f:90de | Advance... | Token USB 64K                | 1     |            | A8A89AC09A |
| 076b:5421 | OmniKey    | Smart Card Reader USB        | 1     |            | EE4E49C4A1 |
| 08e6:3438 | Gemalto... | GemPC Key SmartCard Reader   | 1     |            | 6BDD8BE020 |
| 096e:0505 | Feitian... | FT SCR310                    | 1     | usbfs      | 4039D51671 |
| 0a89:0025 | Aktiv      | Rutoken lite                 | 1     |            | 8FA80B8A39 |
| 0bf8:1006 | Fujitsu... | SmartCard Reader 2A          | 1     |            | D7D5245A1A |
| 20a0:4108 | Clay Logic | Nitrokey Pro                 | 1     | usbhid     | E7580A2343 |
| 23a0:0004 | BIFIT      | iBank2Key                    | 1     |            | A8A89AC09A |
| 24dc:0101 | ARDS       | JaCarta                      | 1     |            | DA308A78C4 |

### Converter (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03eb:21fe | Atmel      | AVR309:USB to UART protoc... | 1     | igorplu... | 1FFCB35DFD |
| 0403:1237 | Future ... | Z397 GUARD Converter         | 1     |            | 00A5CE299A |
| 110a:1110 | Moxa Te... | UPort 1110 1-port RS-232 ... | 1     |            | 3864E6C70F |

### Diagnostic (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 154f:154f | SNBC       |                              | 1     |            | EA8698BD14 |

### Disk (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 058f:6362 | Alcor M... | Flash Card Reader/Writer     | 837   | uas, us... | 70E729A005 |
| 090c:1000 | Silicon... | Flash Voyager                | 331   | uas, us... | D1EC1DAC8F |
| 058f:6364 | Alcor M... | AU6477 Card Reader Contro... | 297   | uas, us... | 78E822AD79 |
| 058f:6387 | Alcor M... | Transcend JetFlash Flash ... | 284   | uas, us... | D084D64BDF |
| 058f:6366 | Alcor M... | Multi Flash Reader           | 261   | uas, us... | B39AC90CA0 |
| 0bda:0151 | Realtek... | Mass Storage Device (Mult... | 203   | uas, us... | 518A8709C7 |
| 048d:1336 | Integra... | SD/MMC Cardreader            | 158   | uas, us... | E969E52D33 |
| 13fe:4200 | Kingsto... | Silicon-Power32G 32GB        | 143   | uas, us... | A1DD312C27 |
| 0951:1666 | Kingsto... | DataTraveler 100 G3/G4/SE... | 120   | uas, us... | CF410274A0 |
| 0781:5567 | SanDisk    | Cruzer Blade 16GB            | 119   | uas, us... | 8D8BE94484 |
| 0930:6545 | Toshiba    | Kingston DataTraveler 102... | 112   | uas, us... | D569843A2D |
| 0bda:0111 | Realtek... | RTS5111 Card Reader Contr... | 100   | uas, us... | A95491B363 |
| 13fe:4100 | Kingsto... | Silicon-Power16G 16GB        | 95    | uas, us... | 6C061A7A15 |
| 14cd:168a | Super Top  | Storage Device 16GB          | 93    | uas, us... | 99614383EB |
| 1005:b113 | Apacer ... | Handy Steno/AH123 / Handy... | 92    | uas, us... | 88EC731DFB |
| 058f:6377 | Alcor M... | AU6375 4-LUN card reader     | 86    | uas, us... | D084D64BDF |
| 058f:9360 | Alcor M... | 8-in-1 Media Card Reader     | 75    | uas, us... | 9137FB3859 |
| 0951:1665 | Kingsto... | Digital DataTraveler SE9 ... | 67    | uas, us... | 6426CF04BD |
| 0bda:0181 | Realtek... | SD/MMC/MS/MSPRO              | 65    | uas, us... | 6B8676DFEF |
| 05e3:070e | Genesys... | USB 2.0 Card Reader          | 63    | uas, us... | FF78BFBB07 |
| 05e3:0716 | Genesys... | USB 2.0 Multislot Card Re... | 59    | uas, us... | 699B1F26E0 |
| 05e3:0745 | Genesys... | Logilink CR0012 32GB         | 57    | uas, us... | 8E95D2B553 |
| 1307:0330 | Transcend  | 63-in-1 Multi-Card Reader... | 56    | uas, us... | B39AC90CA0 |
| 0781:5581 | SanDisk    | Ultra 128GB                  | 51    | uas, us... | 4A2455EAE6 |
| 14cd:8168 | Super Top  | Storage Device               | 51    | uas, us... | BCD563149D |
| 0930:6544 | Toshiba    | TransMemory-Mini / Kingst... | 50    | uas, us... | 327877AB6E |
| 18a5:0302 | Verbatim   | STORE N GO 16GB              | 47    | uas, us... | 97BEC56798 |
| 05e3:0723 | Genesys... | GL827L SD/MMC/MS Flash Ca... | 43    | usb_sto... | 7A74EB0F6F |
| 0bda:0153 | Realtek... | 3-in-1 (SD/SDHC/SDXC) Car... | 42    | uas, us... | 5AFC22B5E1 |
| 13fe:3e00 | Kingsto... | Silicon-Power16G 16GB        | 42    | uas, us... | C800ABA635 |
| 0cf2:6230 | ENE Tec... | SD Card Reader (UB623X)      | 41    | uas, us... | FEAA959521 |
| 0951:1642 | Kingsto... | DT101 G2 16GB                | 39    | uas, us... | B121962A4C |
| 174c:5106 | ASMedia... | Transcend StoreJet 25M3 3... | 38    | uas, us... | 5D65FDCE91 |
| 0951:1643 | Kingsto... | DataTraveler G3 15GB         | 37    | uas, us... | 56D8AE9D24 |
| 13fe:5500 | Kingsto... | Silicon-Power32G 31GB        | 37    | uas, us... | A1DD312C27 |
| 0424:2228 | Standar... | 9-in-2 Card Reader           | 35    | uas, us... | FF0A7B1FC8 |
| 0781:556b | SanDisk    | Cruzer Edge 16GB             | 34    | uas, us... | D5DC53D7FA |
| 0781:5571 | SanDisk    | Cruzer Fit 16GB              | 34    | uas, us... | 8AC0F3559E |
| 12d1:14dc | Huawei ... | E33372 LTE/UMTS/GSM HiLin... | 33    | uas, us... | 7BE9BEC01E |
| 0781:5575 | SanDisk    | Cruzer Glide 32GB            | 31    | uas, us... | FE7535550F |
| abcd:1234 | Chipsbnk   | Alu Line 16GB                | 30    | uas, us... | 4FD52E4D9A |
| 090c:6300 | Silicon... | Reader SD/MS                 | 29    | uas, us... | 54231260FF |
| 0bda:0184 | Realtek... | RTS5182 Card Reader          | 25    | uas, us... | D7D5245A1A |
| 13fe:3600 | Kingsto... | flash drive (4GB, EMTEC) ... | 24    | usb_sto... | 95D420F37F |
| 14cd:125d | Super Top  | Storage Device 32GB          | 24    | uas, us... | 048F01BE63 |
| 05e3:0722 | Genesys... | SD/MMC card reader           | 23    | uas, us... | C0EABA1044 |
| 0bc2:231a | Seagate    | Expansion Portable           | 23    | uas        | AD98351C8D |
| 0bda:0161 | Realtek... | Mass Storage Device          | 23    | mceusb,... | 087F924E20 |
| 1908:0226 | GEMBIRD    | Mass-Storage 134GB           | 23    | uas, us... | 7F1BE20709 |
| 048d:1345 | Integra... | Multi Cardreader             | 22    | uas, us... | 1E64A21630 |
| 0781:5572 | SanDisk    | Cruzer Switch 16GB           | 22    | uas, us... | 3D5D3F552C |
| 0781:5591 | SanDisk    | Ultra Flair USB 3.0 124GB    | 22    | uas, us... | 862146DFEC |
| 0951:1607 | Kingsto... | DataTraveler 100 32GB        | 22    | uas, us... | B578A84849 |
| 1058:1021 | Western... | Elements Desktop (WDBAAU)    | 22    | uas, us... | 8D8BE94484 |
| 1058:1042 | Western... | Elements SE Portable (WDB... | 22    | uas, us... | 5CCA374BA4 |
| 058f:6361 | Alcor M... | Multimedia Card Reader       | 21    | uas, us... | B2D5544528 |
| 05e3:0732 | Genesys... | All-in-One Cardreader        | 21    | uas, us... | 30502C41DE |
| 0644:0200 | TEAC       | All-In-One Multi-Card Rea... | 21    | uas, us... | C80A50B2B4 |
| 0951:1653 | Kingsto... | Data Traveler 100 G2 8 Gi... | 21    | uas, us... | 9C885EB562 |
| 0bda:0158 | Realtek... | USB 2.0 multicard reader     | 21    | ums_rea... | 96600BDB45 |
| 18e3:9102 | Fitipow... | Multi Card Reader            | 21    | uas, us... | BD8118E7B8 |
| 125f:c08a | A-DATA ... | C008 Flash Drive 32GB        | 19    | uas, us... | C67AE8B7E3 |
| 13fe:3d00 | Kingsto... | Silicon-Power8G 8GB          | 19    | uas, us... | CC9A8D1703 |
| 13fe:4300 | Kingsto... | USB DISK 2.0 16GB            | 19    | uas, us... | 01EB97A943 |
| 0480:a202 | Toshiba... | Canvio Basics HDD 500GB      | 18    | uas, us... | 1395F6EFED |
| 05e3:0751 | Genesys... | microSD Card Reader          | 18    | uas, us... | 37E5DABF3C |
| 1058:10b8 | Western... | Elements Portable (WDBU6Y... | 18    | uas, us... | FA682BCB6A |
| 0781:5530 | SanDisk    | Cruzer                       | 17    | uas, us... | 4C192F8B47 |
| 0781:5583 | SanDisk    | Ultra Fit 123GB              | 17    | uas, us... | B481805845 |
| 0bc2:3300 | Seagate    | Desktop 500GB                | 17    | uas, us... | 8D8BE94484 |
| 125f:c96a | A-DATA ... | C906 Flash Drive 16GB        | 17    | uas, us... | 4529486397 |
| 13fe:3623 | Kingsto... | silicon-power 16GB           | 17    | uas, us... | 0D1F02B4A4 |
| 8644:8003 | Intenso... | Micro Line 16GB              | 17    | uas, us... | 6033E4CDA9 |
| 0bc2:ab24 | Seagate    | Backup Plus Portable Drive   | 16    | uas        | DD4DA32934 |
| 13fe:1d00 | Kingsto... | DataTraveler 2.0 1GB/4GB ... | 16    | uas, us... | 941367D018 |
| 1307:0165 | Transcend  | 2GB/4GB/8GB Flash Drive 16GB | 15    | uas, us... | 0BE7A39100 |
| 13fe:5200 | Kingsto... | DataTraveler R3.0 32GB       | 15    | uas, us... | 5F5E6D3AB1 |
| 1687:3257 | Kingmax... | USB2.0 FlashDisk 33GB        | 15    | uas, us... | E42FD626B2 |
| 18e3:9106 | Fitipow... | MS/MS-Pro                    | 15    | uas, us... | A9406FFA03 |
| 04e8:61b6 | Samsung... | M3 Portable Hard Drive       | 14    | uas, us... | A5BBF4389F |
| 14cd:1212 | Super Top  | microSD card reader (SY-T18) | 14    | uas, us... | 1D3FD80F42 |
| 14cd:125c | Super Top  | SD card reader               | 14    | uas, us... | 313997BAE1 |
| 3538:0901 | pqi        | IntelligentStick 8GB         | 14    | uas, us... | F0FD8645E5 |
| 05e3:0749 | Genesys... | STORAGE DEVICE 8GB           | 13    | uas, us... | E969E52D33 |
| 0951:1625 | Kingsto... | DataTraveler 101 II 4GB      | 13    | uas, us... | 2ACA3CEF22 |
| 0bb4:0001 | HTC (Hi... | Android Phone via mass st... | 13    | uas, us... | 303F6AA456 |
| 14cd:6116 | Super Top  | M6116 SATA Bridge            | 13    | uas, us... | BF318AD0C1 |
| 04cf:8819 | Myson C... | USB 2.0 SD/MMC Reader        | 12    | uas, us... | 7A7933F5D5 |
| 05e3:0710 | Genesys... | USB 2.0 33-in-1 Card Reader  | 12    | uas, us... | 216C8A62A0 |
| 0bc2:2312 | Seagate    | Expansion 500GB              | 12    | uas, us... | 8373600EB6 |
| 0bc2:3320 | Seagate    | SRD00F2 [Expansion Deskto... | 12    | uas, us... | 2DDBCF3D21 |
| 0bda:0301 | Realtek... | multicard reader             | 12    | uas, us... | 0C968AC3AE |
| 1058:074a | Western... | My Passport 074A 500GB       | 12    | uas, us... | 330C957752 |
| 1058:1078 | Western... | Elements Portable (WDBUZG)   | 12    | uas, us... | DA15B2FC1B |
| 1058:1100 | Western... | My Book Essential Edition... | 12    | uas, us... | A02BF3040C |
| 13fe:1f00 | Kingsto... | Kingston DataTraveler / P... | 12    | uas, us... | BCF1D99475 |
| 152d:0567 | JMicron... | JMS567 SATA 6Gb/s bridge     | 12    | uas, us... | 7136FF50B4 |
| 174c:1153 | ASMedia... | ASM2115 SATA 6Gb/s bridge    | 12    | uas, us... | 87D6B1C03E |
| 0480:a006 | Toshiba... | External Disk                | 11    | uas, us... | 77FDDE1684 |
| 0781:5597 | SanDisk    | Cruzer Glide 3.0 124GB       | 11    | uas, us... | CCE7F9292D |

### Dvb card (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:2838 | Realtek... | RTL2838 DVB-T                | 16    | dvb_usb... | 100291AB94 |
| 07ca:0831 | AVerMed... | H831 USB Hybrid DVB-T/T2     | 5     |            | D5A4F195AE |
| 15a4:9016 | Afatech    | AF9015 DVB-T USB2.0 stick    | 5     | dvb_usb... | B5CD6895D0 |
| 07ca:0830 | AVerMed... | H830 USB Hybrid DVB-T        | 4     |            | 79A097BF6F |
| 0bda:2832 | Realtek... | RTL2832U DVB-T               | 3     | dvb_usb... | 9994FB8ABA |
| 04b4:2102 | Cypress... | Cypress DVB Card             | 2     | dvb_usb... | 3F4C49A9AA |
| 14f7:0500 | TechniS... | DVB-PC TV Star HD            | 2     | dvb_usb... | C7D09ABC04 |
| 15f4:0131 | HanfTek    | dvbt2                        | 2     | dvb_usb... | CCB302179F |
| 0413:6a04 | Leadtek... | DVB-T 2                      | 1     | dvb_usb... | C5B8862A26 |
| 048d:9005 | Integra... | DVB-T TV Stick               | 1     | dvb_usb... | 41E56B736D |
| 04b4:861f | Cypress... | Anysee E30 USB 2.0 DVB-T ... | 1     | dvb_usb... | 1C1B87DEA4 |
| 0572:c688 | Conexan... | Geniatech T230 DVB-T2 TV ... | 1     | dvb_usb... | 48397FDB3D |
| 07ca:a815 | AVerMed... | AVerTV DVB-T Volar X (A815)  | 1     | dvb_usb... | 35CFA6018F |
| 0b48:3006 | TechnoT... | TT-connect S-2400 DVB-S r... | 1     | dvb_usb... | 003EB89135 |
| 0bda:2831 | Realtek... | RTL2831U DVB-T               | 1     | dvb_usb... | 2A72B05F9D |
| 13d3:3282 | IMC Net... | DVB-T + GPS Minicard [RTL... | 1     |            | 1FFBE8F223 |
| 15a4:1001 | Afatech    | AF9015/AF9035 DVB-T stick    | 1     | dvb_usb... | 006CC8353F |
| 187f:0202 | Siano M... | Nice                         | 1     | smsusb     | FC4EBA57CC |
| 1b80:d393 | Afatech    | DVB-T receiver [RTL2832U]    | 1     | dvb_usb... | AB722FD0B6 |
| 1d19:1101 | Dexatek... | DK DVB-T Dongle              | 1     | dvb_usb... | BAEBBFC37F |
| 1d19:1102 | Dexatek... | DK mini DVB-T Dongle         | 1     | dvb_usb... | 66EEFE25AF |
| 1f4d:3000 | G-Tek E... | HD Star DVB-S2 USB2.0        | 1     | dvb_usb... | 4D00BA8216 |
| 2040:7070 | Hauppauge  | Nova-T Stick 3               | 1     | dvb_usb... | 3CC746C01B |
| 2040:9580 | Hauppauge  | NovaT 500Stick               | 1     | dvb_usb... | 2457E81077 |
| 2304:022b | Pinnacl... | PCTV 71e [Afatech AF9015]    | 1     | dvb_usb... | B778A6096A |
| 2304:022c | Pinnacl... | PCTV 2000e                   | 1     | dvb_usb... | B2BA637E05 |
| 2304:0237 | Pinnacl... | PCTV 73e [DiBcom DiB7000PC]  | 1     | dvb_usb... | B9F2AE0CCF |

### Fingerprint reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:00bb | Microsoft  | Fingerprint Reader           | 2     | usbhid     | 636774FB05 |
| 0483:2016 | STMicro... | Fingerprint Reader           | 2     |            | F9B5D49091 |
| 05ba:0002 | Digital... | Fingerprint Scanner, U.ar... | 1     |            | AE361E5F23 |
| 06cb:0082 | Synaptics  | My Lockey                    | 1     |            | 3E9DFA313B |
| 08ff:2580 | AuthenTec  | AES2501 Fingerprint Sensor   | 1     |            | 37E5DABF3C |
| 1c7a:0603 | LighTun... | EgisTec ES603                | 1     |            | BBF13C69AE |

### Floppy (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 059b:0034 | Iomega     | Zip 100 Driver               | 1     | usb_sto... | 506F4A7C1D |

### Gamepad (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:028e | Microsoft  | Xbox360 Controller           | 75    | xpad       | A8F2B39356 |
| 046d:c21f | Logitech   | F710 Wireless Gamepad [XI... | 20    | xpad       | 8C8AF8B557 |
| 045e:0291 | Microsoft  | Xbox 360 Wireless Receive... | 15    | xpad       | 6144FC259F |
| 0079:0011 | DragonRise | Gamepad                      | 10    | usbhid     | 847A0934D7 |
| 046d:c21d | Logitech   | F310 Gamepad [XInput Mode]   | 9     | xpad       | 6B4EBF9EE7 |
| 0e8f:0003 | GreenAsia  | MaxFire Blaze2               | 5     | usbhid     | E075ADEAD4 |
| 046d:c21e | Logitech   | F510 Gamepad [XInput Mode]   | 4     | xpad       | 852999BF0D |
| 0810:e501 | Persona... | SNES Gamepad                 | 4     | usbhid     | 6DE273D636 |
| 0e8f:0008 | GreenAsia  | Game Controller for PC       | 4     | usbhid     | B1600EF31C |
| 1345:6006 | Sino Li... | Defender Wireless Controller | 3     |            | E3D0C37620 |
| 0583:a000 | Padix (... | MaxFire G-08XU Gamepad       | 2     | usbhid     | 33EE68D3F5 |
| 2563:0575 | ShanWan    | USB WirelessGamepad          | 2     | usbhid     | 9FFD70ADD1 |
| 0079:181c | DragonRise | Gamepad                      | 1     | usbhid     | 2D24119519 |
| 044f:b326 | ThrustM... | Gamepad GP XID               | 1     | xpad       | 3C19F51786 |
| 046d:c242 | Logitech   | XUSB Gamepad                 | 1     | xpad       | A07F95C027 |
| 050d:0805 | Belkin ... | Nostromo N50 GamePad         | 1     | usbhid     | CE6735B3BF |
| 07b5:0213 | Mega Wo... | Thrustmaster Firestorm Di... | 1     | usbhid     | 70CA2BBB71 |
| 0810:0005 | Persona... | USB Gamepad                  | 1     | usbhid     | 3BDBF957FA |
| 0e6f:011e | Logic3     | Rock Candy Gamepad for PS3   | 1     | usbhid     | 8E73A7CEDB |
| 0e6f:0213 | Logic3     | Afterglow Gamepad for Xbo... | 1     | xpad       | 9DE5FB33B8 |
| 12ab:f701 | Honey B... | Xbox Controller              | 1     | xpad       | B621A131B1 |
| 145f:0231 | Trust      | USB Gamepad                  | 1     | usbhid     | 2DC764FFB7 |
| 1689:fd00 | Razer USA  | Onza Tournament Edition c... | 1     | xpad       | EC671CD080 |

### Hardware key (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0a89:0003 | Aktiv      | Guardant Stealth/Net II      | 7     |            | 5149320E81 |
| 0a89:0020 | Aktiv      | Rutoken S                    | 7     | usbfs      | 07802CEC21 |
| 0471:485d | Philips... | Senselock SenseIV v2.x       | 1     |            | C75142F476 |
| 14a8:0001 | Soft pr... | Soft protection device: U... | 1     |            | 568CDC2D31 |

### Hasp (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0529:0001 | Aladdin... | HASP copy protection dongle  | 28    |            | 920F49F96C |

### Hub (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1d6b:0002 | Linux F... | 2.0 root hub                 | 13498 | hub        | D1EC1DAC8F |
| 1d6b:0001 | Linux F... | 1.1 root hub                 | 8190  | hub        | B481805845 |
| 1d6b:0003 | Linux F... | 3.0 root hub                 | 5882  | hub        | D1EC1DAC8F |
| 8087:0024 | Intel      | Integrated Rate Matching Hub | 2384  | hub        | 8C4047657D |
| 8087:8000 | Intel      | Hub                          | 903   | hub        | DD4DA32934 |
| 8087:8008 | Intel      | Hub                          | 889   | hub        | DD4DA32934 |
| 1a40:0101 | incompl... | 4-Port HUB                   | 543   | hub        | 8EAEF3C906 |
| 05e3:0608 | Genesys... | USB-2.0 4-Port HUB           | 518   | hub        | D1EC1DAC8F |
| 8087:0020 | Intel      | Integrated Rate Matching Hub | 333   | hub        | 16C4C7C1C0 |
| 2109:3431 | VIA Labs   | USB2.0 Hub                   | 237   | hub        | 394C81B911 |
| 05e3:0610 | Genesys... | 4-port hub                   | 222   | hub        | 85F1B83B30 |
| 8087:8001 | Intel      | Hub                          | 206   | hub        | EFA3992D9A |
| 8087:8009 | Intel      | Hub                          | 198   | hub        | EFA3992D9A |
| 0424:2514 | Standar... | USB 2.0 Hub                  | 147   | hub        | 8EAEF3C906 |
| 2109:2812 | VIA Labs   | USB2.0 VL812 Hub             | 110   | hub        | C92DC5D0CF |
| 0409:005a | NEC Com... | HighSpeed Hub                | 106   | hub        | BF6F02A425 |
| 05e3:0606 | Genesys... | USB 2.0 Hub / D-Link DUB-... | 102   | hub        | 47FD7AB796 |
| 174c:2074 | ASMedia... | ASM1074 High-Speed hub       | 95    | hub        | A8F2B39356 |
| 174c:3074 | ASMedia... | ASM1074 SuperSpeed hub       | 95    | hub        | A8F2B39356 |
| 1a40:0201 | Terminu... | FE 2.1 7-port Hub            | 81    | hub        | E969E52D33 |
| 2109:0812 | VLI Labs   | USB 3.0 VL812 HUB            | 76    | hub        | C92DC5D0CF |
| 058f:9254 | Alcor M... | Hub                          | 75    | hub        | FE9E0ACDE5 |
| 058f:6254 | Alcor M... | USB Hub                      | 69    | hub        | 239A2E1678 |
| 8087:8002 | Intel      | Hub                          | 66    | hub        | A8F2B39356 |
| 8087:800a | Intel      | Hub                          | 66    | hub        | A8F2B39356 |
| 14cd:8601 | Super Top  | USB 2.0 Hub                  | 65    | hub        | A1DD312C27 |
| 05e3:0612 | Genesys... | USB3.0 Hub 123               | 56    | hub        | 0C7ACF2C99 |
| 2109:0811 | VIA Labs   | 4-Port USB 3.0 Hub           | 50    | hub        | FB18F180A5 |
| 0424:2504 | Standar... | USB 2.0 Hub                  | 47    | hub        | C9E6C4517D |
| 2109:2813 | VIA Labs   | USB2.0 Hub                   | 45    | hub        | 537D11B224 |
| 2109:2811 | VIA Labs   | USB2.0 Hub                   | 40    | hub        | A8F2B39356 |
| 05ac:1006 | Apple      | Hub in Aluminum Keyboard     | 37    | hub        | 7D2057C89D |
| 0a5c:4500 | Broadcom   | BCM2046B1 USB 2.0 Hub (pa... | 37    | hub        | 68BABB69CB |
| 05e3:0616 | Genesys... | hub                          | 35    | hub        | 7F1F084A4F |
| 0bda:5411 | Realtek... | 4-Port USB 2.0 Hub           | 34    | hub        | 8C6609B568 |
| 214b:7000 |            | USB2.0 HUB                   | 34    | hub        | 95611F735C |
| 045b:0209 | Hitachi    | Hub                          | 33    | hub        | 7875ECD5A5 |
| 2109:0813 | VIA Labs   | USB3.0 Hub                   | 33    | hub        | 537D11B224 |
| 045b:0210 | Hitachi    | Hub                          | 32    | hub        | 7875ECD5A5 |
| 2109:8110 | VIA Labs   | USB 3.0 Hub                  | 32    | hub        | A8F2B39356 |
| 0bda:0411 | Realtek... | 4-Port USB 3.0 Hub           | 26    | hub        | 8C6609B568 |
| 0451:8142 | Texas I... | TUSB8041 4-Port Hub          | 25    | hub        | 3B3DC1A093 |
| 0557:8021 | ATEN In... | CS1764A [CubiQ DVI KVMP S... | 24    | hub        | 0A0473CD45 |
| 2109:0810 | VIA Labs   | 4-Port USB 3.0 VL81x Hub     | 24    | hub        | 07DD5B8424 |
| 05e3:0607 | Genesys... | Logitech G110 Hub            | 23    | hub        | 842E440F34 |
| 0a05:7211 | Unknown... | hub                          | 22    | hub        | A63447351B |
| 0438:7900 | AMD        | Hub                          | 20    | hub        | 37D7E42722 |
| 04b4:6560 | Cypress... | CY7C65640 USB-2.0 "TetraHub" | 20    | hub        | ABA6CA8F34 |
| 413c:1003 | Dell       | Keyboard Hub                 | 19    | hub        | C5B86FB4CB |
| 046d:c223 | Logitech   | G11/G15 Keyboard / USB Hub   | 18    | hub        | B77180A9F5 |
| 8564:4000 | Transcend  | RDF8 16GB                    | 18    | uas, us... | 725B24FE69 |
| 05ac:1003 | Apple      | Hub in Pro Keyboard [Mits... | 16    | hub        | 44204F310C |
| 04b4:2050 | Cypress... | hub                          | 15    | hub        | 544F0D2A23 |
| 05e3:0605 | Genesys... | USB 2.0 Hub                  | 15    | hub        | 21FB8F59A4 |
| 03eb:0902 | Atmel      | 4-Port Hub                   | 14    | hub        | E4A10B1178 |
| 0451:8140 | Texas I... | TUSB8041 4-Port Hub          | 14    | hub        | 3B3DC1A093 |
| 2001:f103 | D-Link     | DUB-H7 7-port USB 2.0 hub    | 14    | hub        | 5B8AE7E31F |
| 0409:0059 | NEC Com... | HighSpeed Hub                | 13    | hub        | 06776696F3 |
| 0424:2640 | Standar... | USB 2.0 Hub                  | 13    | hub        | D5D160AFDF |
| 03eb:3301 | Atmel      | at43301 4-Port Hub           | 11    | hub        | 31EC690BE3 |
| 0424:2512 | Standar... | USB 2.0 Hub                  | 11    | hub        | DDDDDCBF45 |
| 0424:2660 | Standar... | Hub                          | 11    | hub        | FB66D16E30 |
| 0424:2744 | Standar... | USB2744                      | 11    | hub        | 3FF206C6B8 |
| 0451:8044 | Texas I... | Hub                          | 11    | hub        | 734EB3795E |
| 214b:7250 |            | USB2.0 HUB                   | 11    | hub        | 37E5DABF3C |
| 0458:0736 | KYE Sys... | Multimedia Keyboard HUB      | 10    | hub        | 292785F9EC |
| 413c:1002 | Dell       | Keyboard Hub                 | 10    | hub        | BD8118E7B8 |
| 0424:2134 | Standar... | USB2134B                     | 9     | hub        | 8B7204FCBA |
| 0424:2602 | Standar... | USB 2.0 Hub                  | 9     | hub        | 4E3DF2DAE2 |
| 0451:8046 | Texas I... | Hub                          | 8     | hub        | 734EB3795E |
| 046d:0b06 | Logitech   | BT Mini-Receiver             | 8     | hub        | 833D6EE41C |
| 0557:7000 | ATEN In... | Hub                          | 8     | hub        | EBB5316694 |
| 413c:1005 | Dell       | Multimedia Pro Keyboard Hub  | 8     | hub        | 0BEB4B51BA |
| 0424:5744 | Standar... | USB5744                      | 7     | hub        | A9B1FD6467 |
| 0451:2046 | Texas I... | TUSB2046 Hub                 | 7     | hub        | 2A53FCCBF5 |
| 05ac:1002 | Apple      | Extended Keyboard Hub [Mi... | 7     | hub        | 48B2ED99AE |
| 0bc2:ab44 | Seagate    | Backup+ Hub                  | 7     | hub        | F09C81116E |
| 2109:2817 | VIA Labs   | USB2.0 Hub                   | 7     | hub        | 62B0B05A66 |
| 8087:07e6 | Intel      | Hub                          | 7     | hub        | 0C90B25A45 |
| 0409:0058 | NEC Com... | HighSpeed Hub                | 6     | hub        | 54BAEE136E |
| 0424:2137 | Standar... | USB2137B                     | 6     | hub        | C87E189FA6 |
| 0424:2412 | Standar... | Hub                          | 6     | hub        | 92BF714334 |
| 0451:1446 | Texas I... | TUSB2040/2070 Hub            | 6     | hub        | 409FAE415E |
| 0451:2036 | Texas I... | TUSB2036 Hub                 | 6     | hub        | 374ABA6BB7 |
| 0451:8043 | Texas I... | Hub                          | 6     | hub        | 47FD7AB796 |
| 05e3:0604 | Genesys... | USB 1.1 Hub                  | 6     | hub        | 0D95B6B366 |
| 05e3:0617 | Genesys... | USB3.0 Hub                   | 6     | hub        | 85F1B83B30 |
| 2109:0817 | VIA Labs   | USB3.0 Hub                   | 6     | hub        | 62B0B05A66 |
| 413c:1010 | Dell       | USB 2.0 Hub [MTT]            | 6     | hub        | DA78DC8E90 |
| 0424:2502 | Standar... | Hub                          | 5     | hub        | 4E3DF2DAE2 |
| 050d:0234 | Belkin ... | F5U234 USB 2.0 4-Port Hub    | 5     | hub        | 7136FF50B4 |
| 050d:0237 | Belkin ... | F5U237 USB 2.0 7-Port Hub    | 5     | hub        | 48E75D806A |
| 050d:0304 | Belkin ... | FSU304 USB 2.0 - 4 Ports Hub | 5     | hub        | 44AA7CC920 |
| 0566:3020 | Montere... | USB 2.0 HUB                  | 5     | usbcore    | 98FF7EAA07 |
| 0835:8500 | Action ... | USB2.0 Hub                   | 5     | hub        | 18F3EC18B4 |
| 0bc2:ab45 | Seagate    | Backup+ Hub                  | 5     | hub        | C3052C67D6 |
| 0bda:5401 | Realtek... | RTL 8153 USB 3.0 hub with... | 5     | hub        | FEAA959521 |
|           | ServerE... | SE USB Device                | 4     | usbhid     | E3CF1A821F |
| 0451:8042 | Texas I... | Hub                          | 4     | hub        | 90CA95CC4C |
| 046b:ff01 | America... | Virtual Hub                  | 4     | hub        | 9E52EE363E |

### Human interface (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0079:0006 | DragonRise | PC TWIN SHOCK Gamepad        | 46    | usbhid     | 9CEDE9D704 |
| 0665:5161 | Cypress... | USB to Serial                | 35    | usbhid     | D7D3D4F56B |
| 10d5:55a2 | Uni Cla... | 2Port KVMSwitcher            | 22    | usbhid     | A27D10FE73 |
| 1b1c:0c04 | Corsair    | Integrated USB Bridge        | 22    | usbhid     | 725B24FE69 |
| 0b05:1872 | ASUSTek... | AURA LED Controller          | 20    | usbhid     | E57993E26A |
| 0b05:1867 | ASUSTek... | AURA Custom Human interface  | 14    | usbhid     | 5F582A0578 |
| 2101:1407 | ActionStar | USB KVM                      | 14    | usbhid     | 144815A4E9 |
| 046d:c215 | Logitech   | Extreme 3D Pro               | 13    | usbhid     | 29A14836FD |
| 054c:0268 | Sony       | Batoh Device / PlayStatio... | 13    | usbhid     | BC9F90FD94 |
| 046d:c216 | Logitech   | F310 Gamepad [DirectInput... | 11    | usbhid     | 6D9668A464 |
| 147a:e00d | Formosa... | IR Receiver                  | 9     | usbhid     | 331F603CE2 |
| 1e71:170e | NZXT       | USB Device                   | 8     | usbhid     | C9E6C4517D |
| 046d:c225 | Logitech   | G11/G15 Keyboard / G keys    | 7     | usbhid     | 5A16BDDE76 |
| 046d:c227 | Logitech   | G15 Refresh Keyboard         | 7     | usbhid     | B77180A9F5 |
| 0810:0001 | Persona... | Dual PSX Adaptor             | 7     | usbhid     | 0BEB4B51BA |
| 0810:0003 | Persona... | PlayStation Gamepad          | 7     | usbhid     | D7CC0778FC |
| 1770:ef35 | ASUS OSD   | ASUS OSD                     | 7     | usbhid     | 7F1BE20709 |
| 0001:0000 | Fry's E... | STD UPS MON V1.0             | 6     | usbhid     | 6564000AC6 |
| 0419:8002 | Samsung... | SyncMaster HID Monitor Co... | 6     | usbhid     | 399FE679CE |
| 054c:05c4 | Sony       | DualShock 4 [CUH-ZCT1E]      | 6     | usbhid     | FAAD08E39A |
| 10d5:000d | Uni Cla... | SP04-A1                      | 6     | usbhid     | 2F8F1A592B |
| 11ff:3331 |            | PC Game Controller           | 6     | usbhid     | 4787FF9CAC |
| 2563:0523 | ShanWan    | PS3/PC Wired GamePad         | 6     | usbhid     | 32AFE6A3D4 |
| 0408:3001 | Quanta ... | Optical Touch Screen         | 5     | usbhid     | E969E52D33 |
| 044f:b108 | ThrustM... | T-Flight Hotas X Flight S... | 5     | usbhid     | 744236B602 |
| 046d:c218 | Logitech   | Logitech RumblePad 2 USB     | 5     | usbhid     | EEBB334395 |
| 046d:c219 | Logitech   | F710 Gamepad [DirectInput... | 5     | usbhid     | 92459D790C |
| 046d:c214 | Logitech   | ATK3 (Attack III Joystick)   | 4     | usbhid     | 33687A527A |
| 046d:c21c | Logitech   | G13 Advanced Gameboard       | 4     | usbhid     | 1C3B65242D |
| 046d:c222 | Logitech   | G15 Keyboard / LCD           | 4     | usbhid     | 4118086F4C |
| 04d8:0b29 | Microch... | U2417H_0B29_15083001         | 4     | usbhid     | C87E189FA6 |
| 06a3:0460 | Saitek     | ST290 Pro Flight Stick       | 4     | usbhid     | 95BA8F99A0 |
| 11ff:3341 |            | USB Joystick                 | 4     | usbhid     | C183D48CD3 |
| 16c0:05df | Van Ooi... | HID device except mice, k... | 4     | radio_m... | A71AC3A709 |
| 0409:02b8 | NEC Com... | PA241W                       | 3     | usbhid     | A9E77A23FE |
| 0416:5020 | Winbond... | HID Transfer                 | 3     | usbhid     | 8F87769D12 |
| 0424:274c | Standar... | Hub Controller               | 3     | usbhid     | B31F25BBB7 |
| 044f:b106 | ThrustM... | T.Flight Stick X             | 3     | usbhid     | 218F087B6C |
| 044f:b10a | ThrustM... | T.16000M Joystick            | 3     | usbhid     | 14A8808D2B |
| 046d:0a5d | Logitech   | Gaming Headset Battery Ch... | 3     | usbhid     | E361B0B9F1 |
| 046d:c29b | Logitech   | G27 Racing Wheel             | 3     | usbhid     | 2C043EBD94 |
| 046d:c626 | Logitech   | 3Dconnexion Space Navigat... | 3     | usbhid     | 07E431CDAB |
| 04eb:e030 | Northst... | Media Center Interface       | 3     | usbhid     | 3736215480 |
| 0955:000a | Nvidia     | ESA FW Update                | 3     | usbhid     | 92BF714334 |
| 1044:7a02 | Chu Yue... |                              | 3     | usbhid     | 5F6E1A3B61 |
| 10d5:55a4 | Uni Cla... | 4 Port KVMSwicther           | 3     | usbhid     | 68158424AF |
| 187c:0526 | Alienware  | A-51                         | 3     | usbhid     | E1BABF19AD |
| 264a:2329 | Thermal... | Thermaltake DPS PSU          | 3     | usbhid     | 87B482DE1D |
| 2687:fb01 | Fitbit     | Base Station                 | 3     | usbhid     | B2D5544528 |
| 2833:0031 | Oculus VR  | Rift                         | 3     | usbhid     | B3E32D1026 |
| ffff:0000 |            | 068A                         | 3     | usbhid     | 68ECBC0D7A |
| 0451:ca01 | Texas I... | USBtoI2C Solution            | 2     | usbhid     | 4CC72409B5 |
| 045e:001a | Microsoft  | SideWinder Precision Raci... | 2     | usbhid     | D6C60523A0 |
| 046d:c21a | Logitech   | Precision Gamepad            | 2     | usbhid     | B0D2314507 |
| 046d:c29a | Logitech   | Driving Force GT             | 2     | usbhid     | D123408FCA |
| 0483:0035 | STMicro... | ST7 RS232 USB BRIDGE         | 2     | usbhid     | 0F99E7899D |
| 0596:0524 | MicroTo... | 3M MicroTouch USB controller | 2     | usbhid     | 920E3BC20D |
| 05ac:921b | Apple      | Cinema Display               | 2     | usbhid     | 89119460C8 |
| 05ac:921c | Apple      | A1082 [Cinema HD Display ... | 2     | appledi... | 47C35A4877 |
| 05ac:921d | Apple      | Cinema Display               | 2     | appledi... | F6D05FF577 |
| 05ac:9221 | Apple      | 30" Cinema Display           | 2     | usbhid     | 0438613602 |
| 05ac:9222 | Apple      | Cinema Display               | 2     | appledi... | 63D8A60BA7 |
| 05ac:9223 | Apple      | Cinema HD Display            | 2     | usbhid     | DA940E77B0 |
| 068e:00ff | CH Prod... | Flight Sim Yoke              | 2     | usbhid     | 803350A260 |
| 07b5:0316 | Mega Wo... | USB Game Controllers         | 2     | usbhid     | E3C3739395 |
| 0971:2005 | Gretag-... | Huey                         | 2     | usbhid     | 304F67F539 |
| 0b05:18a3 | ASUSTek... | AURA MOTHERBOARD             | 2     | usbhid     | F733910E90 |
| 0bb4:2c87 | HTC (Hi... | Vive                         | 2     | snd_usb... | C572988845 |
| 1044:7a03 | Chu Yue... | Osmium Interface             | 2     | usbhid     | ACCFB42482 |
| 1345:3008 | Sino Li... | USB Controller               | 2     | usbhid     | BE67B91F72 |
| 172f:0500 | Waltop ... | Media Tablet 14.1"           | 2     | usbhid     | 38C8446DA8 |
| 19fa:0703 | Gampaq     | Steering Wheel               | 2     | usbhid     | 33A0CD03EF |
| 1a34:0805 | ACRUX      | SL-6632 Dark Tornado Joys... | 2     | usbhid     | 36B161957A |
| 1b1c:1c05 | Corsair    | Corsair HX-Series C-Link ... | 2     | usbhid     | CC9738C393 |
| 1b1c:1c06 | Corsair    | Corsair HX-Series C-Link ... | 2     | usbhid     | C4788779C1 |
| 1b1c:1c07 | Corsair    | Corsair HX-Series C-Link ... | 2     | usbhid     | 0AF0FA2869 |
| 1b1c:1c08 | Corsair    | Corsair HX1200i C-Link Ad... | 2     | usbhid     | F09C81116E |
| 1b1c:1c0c | Corsair    | RM850i Power Supply          | 2     | usbhid     | 608FAFB692 |
| 1bfd:1688 | TouchPack  | Resistive Touch Screen       | 2     | usbhid     | 1AD1411940 |
| 2149:2303 | Advance... | CoolTouch System             | 2     | usbhid     | 2F18EFFE9B |
| 2516:0003 | Cooler ... | Xornet gaming mouse          | 2     | usbhid     | 09BCC236C3 |
| 264a:1fa5 | Thermal... |                              | 2     | usbfs      | 87B482DE1D |
| 28de:2000 | Valve S... | Lighthouse FPGA RX           | 2     | usbhid     | C572988845 |
| 28de:2101 | Valve S... | Watchman Dongle              | 2     | usbhid     | C572988845 |
| 0078:0006 | Microntek  | USB Joystick                 | 1     | usbhid     | 055F0F8CEC |
| 0079:0009 | DragonRise | Generic USB Wheel            | 1     | usbhid     | C1742116E2 |
| 03eb:2013 | Atmel      | AVR USB HID DEMO             | 1     | usbhid     | B73AB7E35A |
| 03eb:c8b4 | Atmel      | USBHID                       | 1     | usbhid     | 27AC18D677 |
| 03f0:0367 | Hewlett... | QHMD                         | 1     | usbhid     | CEA175B6F3 |
| 0409:02b7 | NEC Com... | P241W                        | 1     | usbhid     | 11C6A5856B |
| 0409:044f | NEC Com... | EA244UHD                     | 1     | usbhid     | E21B8FB5B6 |
| 0416:e007 | Winbond... | OLED Display Power Button    | 1     | usbhid     | 20A5F5B027 |
| 0424:2021 | Standar... | USB-I2C Bridge               | 1     | usbhid     | 27E7BDA60A |
| 0425:0301 | Motorol... | RAGTECH Sistemas de Energia  | 1     | usbhid     | 7FBD02378A |
| 044f:b315 | ThrustM... | Firestorm Dual Analog 3      | 1     | usbhid     | 9AED431851 |
| 044f:b320 | ThrustM... | 2 in 1 DT                    | 1     | usbhid     | 8276D533D6 |
| 044f:b67c | ThrustM... | T.Flight Hotas 4             | 1     | usbhid     | 5F140FC4E2 |
| 0451:82ff | Texas I... |                              | 1     | usbhid     | 13FB3CAF43 |
| 0457:1030 | Silicon... | SiS HID Touch Controller     | 1     | usbhid     | 3F1B997D89 |
| 0457:1080 | Silicon... | SiS HID Touch Controller     | 1     | usbhid     | 7792EF7BC8 |

### Infrared (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0609:031d | SMK Man... | eHome Infrared Receiver      | 4     | mceusb     | B5CD6895D0 |
| 066f:4200 | SigmaTel   | STIr4200 IrDA Bridge         | 3     | stir4200   | F8AA7F430A |
| 1509:9242 | First I... | eHome Infrared Transceiver   | 2     | mceusb     | 3ADB7D01A9 |

### Input/keyboard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:c52b | Logitech   | Unifying Receiver            | 812   | usbhid     | 62E3C9247C |
| 1c4f:0002 | SiGma M... | Keyboard TRACER Gamma Ivory  | 508   | usbhid     | D0D6CD20B1 |
| 09da:9090 | A4Tech     | XL-730K / XL-750BK / XL-7... | 440   | usbhid     | 666B4349AD |
| 046d:c31c | Logitech   | Keyboard K120 for Business   | 426   | usbhid     | 808448B5B6 |
| 046d:c534 | Logitech   | Unifying Receiver            | 345   | usbhid     | F655F180AC |
| 04d9:1702 | Holtek ... | Keyboard LKS02               | 345   | usbhid     | 02CBA6FDA0 |
| 062a:4101 | MosArt ... | Wireless Keyboard/Mouse      | 343   | usbhid     | 29B7777E42 |
| 09da:054f | A4Tech     | USB Device                   | 266   | usbhid     | BD3CFBCE71 |
| 1c4f:0026 | SiGma M... | Keyboard                     | 244   | usbhid     | 56D8AE9D24 |
| 04f3:0103 | Elan Mi... | ActiveJet K-2024 Multimed... | 233   | usbhid     | 8B7204FCBA |
| 045e:0745 | Microsoft  | Nano Transceiver v1.0 for... | 217   | usbhid     | 26243D93F4 |
| 1a2c:2124 | China R... | USB Keyboard                 | 190   | usbhid     | B2869F2D2D |
| 0518:0001 | EzKEY      | USB to PS2 Adaptor v1.09     | 179   | usbhid     | 8226B90793 |
| 04d9:1603 | Holtek ... | Keyboard                     | 147   | usbhid     | 240876777B |
| 09da:0260 | A4Tech     | KV-300H Isolation Keyboard   | 142   | usbhid     | 8EAEF3C906 |
| 1a2c:2d23 | China R... | USB Keyboard                 | 133   | usbhid     | 8C4047657D |
| 046d:c52e | Logitech   | MK260 Wireless Combo Rece... | 132   | usbhid     | 2E391F0E83 |
| 1a2c:0e24 | China R... | USB Keyboard                 | 117   | usbhid     | DD4DA32934 |
| 04d9:1503 | Holtek ... | Shortboard Lefty             | 103   | usbhid     | 7F1F084A4F |
| 1a2c:0c21 | China R... | USB Keyboard                 | 96    | usbhid     | 699B1F26E0 |
| 1a2c:0c23 | China R... | USB Keyboard                 | 95    | usbhid     | A1DD312C27 |
| 046d:c517 | Logitech   | LX710 Cordless Desktop Laser | 93    | usbhid     | F3E244219B |
| 0461:0010 | Primax ... | HP PR1101U / Primax PMX-K... | 90    | usbhid     | 0B02A9E571 |
| 413c:2107 | Dell       | USB Entry Keyboard           | 86    | usbhid     | 65A9CF6ADE |
| 1a2c:2c27 | China R... | USB Keyboard                 | 80    | usbhid     | A0518E949A |
| 24ae:2000 | RAPOO      | 2.4G Wireless Device         | 76    | usbhid     | ABA07B8B6E |
| 03f0:0024 | Hewlett... | KU-0316 Keyboard             | 75    | usbhid     | 2D53282148 |
| 046d:c31d | Logitech   | Media Keyboard K200          | 74    | usbhid     | C9CCBD7C3F |
| 062a:0201 | MosArt ... | Defender Office Keyboard ... | 74    | usbhid     | B26ED41AB8 |
| 413c:2003 | Dell       | Keyboard                     | 73    | usbhid     | C92DC5D0CF |
| 258a:0001 | SINO WE... | USB KEYBOARD                 | 70    | usbhid     | 70E729A005 |
| 0e8f:0022 | GreenAsia  | multimedia keyboard contr... | 64    | usbhid     | 3602A90378 |
| 046d:c312 | Logitech   | DeLuxe 250 Keyboard          | 60    | usbhid     | 4E440FBE69 |
| 09da:9066 | A4Tech     | F3 V-Track Gaming Mouse      | 60    | usbhid     | B724162662 |
| 040b:2000 | Weltren... | USB Keyboard                 | 58    | usbhid     | 54231260FF |
| 04f2:0833 | Chicony... | KU-0833 Keyboard             | 58    | usbhid     | 89E5AA85A6 |
| 045e:00db | Microsoft  | Natural Ergonomic Keyboar... | 56    | usbhid     | FEAA959521 |
| 062a:3286 | MosArt ... | Nano Receiver [Sandstrom ... | 56    | usbhid     | 1E64A21630 |
| 0566:3002 | Montere... | Keyboard                     | 51    | usbhid     | D77F80F180 |
| 0458:0708 | KYE Sys... | Multimedia Keyboard          | 49    | usbhid     | 5C380FEC25 |
| 045e:0750 | Microsoft  | Wired Keyboard 600           | 49    | usbhid     | A1E970227D |
| 04b3:3025 | IBM        | NetVista Full Width Keyboard | 49    | usbhid     | F664FB0C42 |
| 413c:2113 | Dell       | KB216 Wired Keyboard         | 49    | usbhid     | 21F5EEF1AB |
| 045e:07f8 | Microsoft  | Wired Keyboard 600 (model... | 47    | usbhid     | 42C519E784 |
| 0566:3107 | Montere... | Keyboard                     | 47    | usbhid     | E2E650868B |
| 0a81:0101 | Chesen ... | Keyboard                     | 45    | usbhid     | F0B3D3251C |
| 1c4f:0016 | SiGma M... | USB Keyboard                 | 45    | usbhid     | DDAB5FE8B4 |
| 0c45:7603 | Microdia   | USB Keyboard                 | 44    | usbhid     | AAD0551E27 |
| 1d57:fa20 | Xenta      | 2.4G Receiver                | 43    | usbhid     | 047DAC469E |
| 046d:c328 | Logitech   | Corded Keyboard K280e        | 42    | usbhid     | 1E9BB53944 |
| 0b38:0010 | Gear Head  | 107-Key Keyboard             | 40    | usbhid     | 42CC2F59E6 |
| 1a81:1004 | Holtek ... | Wireless Dongle              | 39    | usbhid     | 6567AEB3C4 |
| 045e:00dd | Microsoft  | Comfort Curve Keyboard 20... | 37    | usbhid     | 5AFC22B5E1 |
| 046d:c318 | Logitech   | Illuminated Keyboard         | 36    | usbhid     | D1EC1DAC8F |
| 2a7a:9a18 | CASUE      | USB Keyboard                 | 36    | usbhid     | 01EB97A943 |
| 04f2:0116 | Chicony... | KU-2971/KU-0325 Keyboard     | 34    | usbhid     | 0F3DCCFE4E |
| 04f2:0402 | Chicony... | Genius LuxeMate i200 Keyb... | 34    | usbhid     | 77D7A6574E |
| 413c:2106 | Dell       | Dell QuietKey Keyboard       | 34    | usbhid     | 0BE7A39100 |
| 046d:c315 | Logitech   | Classic Keyboard 200         | 33    | usbhid     | 9F366DE52D |
| 045e:0800 | Microsoft  | Microsoft Nano Transceive... | 31    | usbhid     | 9A41C725F3 |
| 09da:9033 | A4Tech     | X-718BK Optical Mouse        | 31    | usbhid     | E4A1AC8DFE |
| 045e:07b2 | Microsoft  | 2.4GHz Transceiver v8.0 u... | 30    | usbhid     | EB9D0FE3F5 |
| 046d:c316 | Logitech   | HID-Compliant Keyboard       | 30    | usbhid     | 8D8BE94484 |
| 09da:90c0 | A4Tech     | USB Device                   | 30    | usbhid     | 6A60A724F9 |
| 09da:f613 | A4Tech     | USB Device                   | 30    | usbhid     | 873CD3A203 |
| 248a:8566 | Maxxter    | SVEN RX 360 Art Wireless ... | 30    | usbhid     | 8AB719EA6C |
| 03f0:034a | Hewlett... | Elite Keyboard               | 29    | usbhid     | 341917FD42 |
| 0e8f:00a7 | GreenAsia  | 2.4G RX                      | 29    | usbhid     | 82E5D349D1 |
| 1a2c:0021 | China R... | Keyboard                     | 29    | usbhid     | 664ABD17F4 |
| 046a:0023 | Cherry     | CyMotion Master Linux Key... | 28    | usbhid     | D084D64BDF |
| 04d9:1605 | Holtek ... | USB Keyboard                 | 27    | usbhid     | 5D6E2E4B43 |
| 04fc:05d8 | Sunplus... | Wireless keyboard/mouse      | 27    | usbhid     | 99B2ABEC05 |
| 1241:1503 | Belkin     | Keyboard                     | 27    | usbhid     | AA051D69D4 |
| 1a2c:0002 | China R... | USB Keykoard                 | 27    | usbhid     | BE6DED977D |
| 1d57:fa60 | Xenta      | 2.4G Receiver                | 27    | usbhid     | A20DD4DABA |
| 03ee:8801 | Mitsumi    | Keyboard                     | 26    | usbhid     | 0D2A7EFA14 |
| 046d:c313 | Logitech   | Internet 350 Keyboard        | 26    | usbhid     | 6C061A7A15 |
| 1a2c:0b23 | China R... | USB Keyboard                 | 26    | usbhid     | 80AC248C74 |
| 045e:0780 | Microsoft  | Comfort Curve Keyboard 3000  | 25    | usbhid     | ED68722348 |
| 17ef:602d | Lenovo     | Black Silk USB Keyboard      | 25    | usbhid     | C80A50B2B4 |
| 1a2c:4c5e | China R... | USB Keyboard                 | 25    | usbhid     | F8AE92B32F |
| 040b:2013 | Weltren... | 2.4G Wireless Keyboard&Mouse | 24    | usbhid     | F8FD891B32 |
| 045e:07b9 | Microsoft  | Wired Keyboard 200           | 24    | usbhid     | FFB658BAF4 |
| 04d9:1203 | Holtek ... | Keyboard                     | 24    | usbhid     | 08DCF9AA49 |
| 045e:0752 | Microsoft  | Wired Keyboard 400           | 23    | usbhid     | 88DB43EE6F |
| 0603:00f2 | Novatek... | Keyboard (Labtec Ultra Fl... | 23    | usbhid     | E3B0CDFB7D |
| 04f2:0408 | Chicony... | USB-PS/2 Combo Keyboard      | 22    | usbhid     | 9D629FBC20 |
| 05d5:8001 | Super G... | Ergomedia                    | 22    | usbhid     | B4A9C29659 |
| 0e6a:02c0 | Megawin... | PS/2+USB Keyboard            | 22    | usbhid     | E3FD7D5295 |
| 28de:1142 | Valve S... | Wireless Steam Controller    | 22    | usbhid     | 6A9086BF86 |
| 1ea7:0066 | SHARKOO... | [Mediatrack Edge Mini Key... | 21    | usbhid     | C76B2FB327 |
| 413c:2105 | Dell       | Model L100 Keyboard          | 21    | usbhid     | A36E17BC4E |
| 03f0:a407 | Hewlett... | Wireless Optical Comfort ... | 20    | usbhid     | 02CA1CBBEC |
| 04f2:0111 | Chicony... | KU-9908 Keyboard             | 20    | usbhid     | 048C1A4F90 |
| 09da:90a0 | A4Tech     | USB Device                   | 20    | usbhid     | 4DC8527429 |
| 0c45:760b | Microdia   | USB Keyboard                 | 20    | usbhid     | BE6D815002 |
| 04d9:a06b | Holtek ... | Wireless USB Device          | 19    | usbhid     | 620F2A2FAB |
| 04f2:0841 | Chicony... | HP Multimedia Keyboard       | 19    | usbhid     | FA9955F862 |
| 09da:f624 | A4Tech     | USB Device                   | 19    | usbhid     | 873CD3A203 |
| 0a5c:4502 | Broadcom   | Keyboard (Boot Interface ... | 19    | usbhid     | 68BABB69CB |

### Input/mouse (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 093a:2510 | Pixart ... | Optical Mouse                | 727   | usbhid     | 8C4047657D |
| 0458:003a | KYE Sys... | NetScroll+ Mini Traveler ... | 605   | usbhid     | EDC325267B |
| 046d:c05a | Logitech   | M90/M100 Optical Mouse       | 367   | usbhid     | 56D8AE9D24 |
| 046d:c077 | Logitech   | M105 Optical Mouse           | 362   | usbhid     | 7C7D8F8CB6 |
| 09da:000a | A4Tech     | Optical Mouse Opto 510D /... | 355   | usbhid     | F8AE92B32F |
| 046d:c52f | Logitech   | Unifying Receiver            | 267   | usbhid     | DF15AC1D7E |
| 0000:0538 |            | USB OPTICAL MOUSE            | 224   | usbhid     | A6050DD558 |
| 093a:2521 | Pixart ... | Optical Mouse                | 201   | usbhid     | C44B707FC4 |
| 1bcf:0005 | Sunplus... | USB Optical Mouse            | 195   | usbhid     | 0438613602 |
| 275d:0ba6 |            | USB OPTICAL MOUSE            | 192   | usbhid     | 78E822AD79 |
| 1c4f:0034 | SiGma M... | Usb Mouse                    | 162   | usbhid     | 8226B90793 |
| 046d:c05b | Logitech   | M-U0004 810-001317 [B110 ... | 147   | usbhid     | 20EE58CBC8 |
| 09da:c10a | A4Tech     | USB Mouse                    | 140   | usbhid     | EE184BFE51 |
| 18f8:0f97 | [Maxxter]  | USB OPTICAL MOUSE            | 103   | usbhid     | 763D365B92 |
| 046d:c050 | Logitech   | RX 250 Optical Mouse         | 101   | usbhid     | D77F80F180 |
| 045e:00cb | Microsoft  | Basic Optical Mouse v2.0     | 85    | usbhid     | B39AC90CA0 |
| 046d:c018 | Logitech   | Optical Wheel Mouse          | 82    | usbhid     | 0C7ACF2C99 |
| 10c4:8105 | Cygnal ... | USB OPTICAL MOUSE            | 81    | usbhid     | 080AE7C292 |
| 1a2c:0042 | China R... | Usb Mouse                    | 79    | usbhid     | 22D8D62C57 |
| 248a:8367 | Maxxter    | SVEN Comfort 3400 Wireless   | 76    | usbhid     | 6053E60588 |
| 15d9:0a4f | Trust I... | Optical Mouse                | 75    | usbhid     | 8C6609B568 |
| 046d:c03e | Logitech   | Premium Optical Wheel Mou... | 74    | usbhid     | CCE7F9292D |
| 1bcf:0007 | Sunplus... | Optical Mouse                | 73    | usbhid     | 6790768959 |
| 0101:0007 |            | USB OPTICAL MOUSE            | 65    | usbhid     | E3D9D5BAE8 |
| 1ea7:0064 | SHARKOO... | 2.4G Wireless Mouse          | 65    | usbhid     | 84D3F5ED57 |
| 2188:0ae1 | CalDigit   | USB OPTICAL MOUSE            | 63    | usbhid     | 0BF4CAF942 |
| 046d:c016 | Logitech   | Optical Wheel Mouse          | 62    | usbhid     | D569843A2D |
| 046d:c00e | Logitech   | M-BJ58/M-BJ69 Optical Whe... | 61    | usbhid     | 72549E030D |
| 062a:4102 | MosArt ... | 2.4G Wireless Mouse          | 61    | usbhid     | B88A1B7070 |
| 045e:0040 | Microsoft  | Wheel Mouse Optical          | 60    | usbhid     | 46324F79EA |
| 13ee:0001 | MosArt     | Optical Mouse                | 60    | usbhid     | 160C2DE51F |
| 09da:8090 | A4Tech     | X-718BK Oscar Optical Gam... | 58    | usbhid     | 18FC3D2E64 |
| 046d:c062 | Logitech   | M-UAS144 [LS1 Laser Mouse]   | 53    | usbhid     | CBA535C695 |
| 04f3:0235 | Elan Mi... | Optical Mouse                | 50    | usbhid     | A4DA822D7B |
| 15d9:0a4d | Trust I... | Optical Mouse                | 49    | usbhid     | FF78BFBB07 |
| 18f8:0f99 | [Maxxter]  | Optical gaming mouse         | 45    | usbhid     | 02CBA6FDA0 |
| 17ef:6019 | Lenovo     | Lenovo USB Optical Mouse     | 44    | usbhid     | F664FB0C42 |
| 248a:8366 | Maxxter    | Wireless Optical Mouse AC... | 42    | usbhid     | 9CD2D7F72B |
| 04b4:0033 | Cypress... | Mouse                        | 41    | usbhid     | FFA80B4E3F |
| 192f:0916 | Avago T... | USB Optical Mouse            | 41    | usbhid     | DD20758A89 |
| 1c4f:0003 | SiGma M... | HID controller               | 41    | usbhid     | 65A9CF6ADE |
| 413c:301a | Dell       | MS116 USB Optical Mouse      | 41    | usbhid     | 191B15812A |
| 0461:4d0f | Primax ... | HP Optical Mouse             | 40    | usbhid     | EFA3992D9A |
| 0458:0186 | KYE Sys... | Wired Mouse                  | 39    | usbhid     | 32E1C72BAA |
| 046d:c069 | Logitech   | M-U0007 [Corded Mouse M500]  | 39    | usbhid     | 7B26B20B57 |
| 09da:000e | A4Tech     | X-F710F Optical Mouse 3xF... | 39    | usbhid     | 049A1FF815 |
| 09da:0080 | A4Tech     | USB Mouse                    | 36    | usbhid     | B478F266C2 |
| 046d:c408 | Logitech   | Marble Mouse (4-button)      | 34    | usbhid     | 70E729A005 |
| 093a:2516 | Pixart ... | USB OPTICAL MOUSE            | 34    | usbhid     | 99614383EB |
| 0e8f:00fb | GreenAsia  | USB Mouse                    | 34    | usbhid     | 763D365B92 |
| 25a7:fa23 | Compx      | 2.4G Receiver                | 32    | usbhid     | 3D9E77AE8A |
| 1bcf:0002 | Sunplus... | USB Optical Wheel Mouse      | 31    | usbhid     | 6B863A586E |
| 04d9:a09f | Holtek ... | E-Signal LUOM G10 Mechani... | 30    | usbhid     | F6CCF2BBA6 |
| 045e:0039 | Microsoft  | IntelliMouse Optical         | 29    | usbhid     | 1395F6EFED |
| 045e:0084 | Microsoft  | Basic Optical Mouse          | 29    | usbhid     | 9F4F497293 |
| 275d:0a29 |            | USB OPTICAL MOUSE            | 29    | usbhid     | 9CEDE9D704 |
| 0458:002e | KYE Sys... | NetScroll + Traveler / Ne... | 28    | usbhid     | EB29C895FA |
| 046d:c045 | Logitech   | Optical Mouse                | 28    | usbhid     | 88DB43EE6F |
| 046d:c246 | Logitech   | Gaming Mouse G300            | 27    | usbhid     | 1984E65634 |
| 15d9:0a4c | Trust I... | USB+PS/2 Optical Mouse       | 27    | usbhid     | 42CC2F59E6 |
| 192f:0416 | Avago T... | ADNS-5700 Optical Mouse C... | 27    | usbhid     | 5D6E2E4B43 |
| 045e:0083 | Microsoft  | Basic Optical Mouse          | 26    | usbhid     | 4EA0694850 |
| 04b4:0060 | Cypress... | Wireless optical mouse       | 26    | usbhid     | 88B7D96AEE |
| 09da:0006 | A4Tech     | Optical Mouse WOP-35 / Tr... | 26    | usbhid     | 0533339E4E |
| 04d9:0499 | Holtek ... | Optical Mouse                | 25    | usbhid     | 77F3E43AB2 |
| 276d:1160 | YSTEK      | G Mouse                      | 24    | usbhid     | 0BEB4B51BA |
| 0000:3825 |            | USB OPTICAL MOUSE            | 23    | usbhid     | 888F2F55D8 |
| 0461:4d64 | Primax ... | USB Optical Mouse            | 23    | usbhid     | 09CC6BAB56 |
| 279e:024e | 2.4G wi... | 2.4G wireless USB Device     | 23    | usbhid     | 8AB719EA6C |
| 03f0:094a | Hewlett... | Optical Mouse [672662-001]   | 22    | usbhid     | C9FA6E4826 |
| 03f0:134a | Hewlett... | Optical Mouse                | 22    | usbhid     | 046006226D |
| 046d:c040 | Logitech   | Corded Tilt-Wheel Mouse      | 22    | usbhid     | 63547A209E |
| 1d57:0005 | Xenta      | Wireless Receiver (Keyboa... | 22    | usbhid     | F2560038AF |
| 045e:0797 | Microsoft  | Optical Mouse 200            | 21    | usbhid     | F1E5880584 |
| 046d:c03d | Logitech   | M-BT96a Pilot Optical Mouse  | 21    | usbhid     | 28F7EE26B5 |
| 046d:c07e | Logitech   | Gaming Mouse G402            | 21    | usbhid     | 3FB755ED84 |
| 04b3:310c | IBM        | Wheel Mouse                  | 21    | usbhid     | E2D3942D30 |
| 1a2c:0044 | China R... | Usb Mouse                    | 21    | usbhid     | 5B60FEFCEC |
| 1c4f:0032 | SiGma M... | Usb Mouse                    | 21    | usbhid     | 4C163D8A71 |
| 04f3:0234 | Elan Mi... | Optical Mouse                | 20    | usbhid     | 936B9A47EC |
| 10c4:8103 | Cygnal ... | USB OPTICAL MOUSE            | 20    | usbhid     | 573813F74B |
| 413c:3012 | Dell       | Optical Wheel Mouse          | 20    | usbhid     | 475863E34D |
| 045e:076c | Microsoft  | Comfort Mouse 4500           | 19    | usbhid     | 13B9B4E6E9 |
| 0a5c:4503 | Broadcom   | Mouse (Boot Interface Sub... | 19    | usbhid     | 68BABB69CB |
| 3938:1031 | MOSART ... | 2.4G Wireless Mouse          | 19    | usbhid     | FE652A02C9 |
| 046d:c046 | Logitech   | RX1000 Laser Mouse           | 18    | usbhid     | 5E7019C1F4 |
| 046d:c084 | Logitech   | G102 Prodigy Gaming Mouse    | 18    | usbhid     | 16C4C7C1C0 |
| 046d:c332 | Logitech   | G502 Proteus Spectrum Opt... | 18    | usbhid     | BF6F02A425 |
| 046d:c51b | Logitech   | V220 Cordless Optical Mou... | 18    | usbhid     | FE7535550F |
| 258a:1007 | SINOWEALTH | Game Mouse                   | 18    | usbhid     | 451A733603 |
| 046d:c531 | Logitech   | C-U0007 [Unifying Receiver]  | 17    | usbhid     | 518A8709C7 |
| 04fc:0538 | Sunplus... | Wireless Optical Mouse 2.... | 17    | usbhid     | 3845E956B9 |
| 1c4f:0048 | SiGma M... | Usb Mouse                    | 17    | usbhid     | BD61806194 |
| 1d57:0001 | Xenta      | wireless device              | 17    | usbhid     | EF835695B4 |
| 046d:c049 | Logitech   | G5 Laser Mouse               | 16    | usbhid     | A3AC5B74CF |
| 046d:c247 | Logitech   | G100s Optical Gaming Mouse   | 16    | usbhid     | 699B1F26E0 |
| 04f2:0939 | Chicony... | USB Optical Mouse            | 16    | usbhid     | 89E5AA85A6 |
| 1d57:0008 | Xenta      | 2.4G Wireless Optical Mouse  | 16    | usbhid     | 29C755D826 |
| 1d57:ad03 | Xenta      | Feeling-tech-USB product     | 16    | usbhid     | 291EF2F99E |
| 1d57:ad17 | Xenta      | ZELOTES GAME MOUSE           | 16    | usbhid     | 328F236B37 |

### Joystick (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 068e:00f2 | CH Prod... | Flight Sim Pedals            | 3     | usbhid     | 803350A260 |
| 24c6:581a | Unknown... | XB1 Classic controller       | 3     | xpad       | 87B482DE1D |
| 046d:c262 | Logitech   | G920 Driving Force Racing... | 2     | usbhid     | 13D4BA319E |
| 20bc:5500 | SHANWAN    | Android Gamepad              | 2     | usbhid     | 491DB7C6BD |
| 231d:1105 | www.vkb... | GTX Throttle                 | 2     | usbhid     | CB2AAA3EDB |
| 0079:189c | DragonRise | PXN-V3II                     | 1     | xpad       | D90F3585DA |
| 045e:000e | Microsoft  | SideWinder Freestyle Pro     | 1     | usbhid     | 631092B6D1 |
| 068e:00f3 | CH Prod... | Fighterstick                 | 1     | usbhid     | CB2AAA3EDB |
| 07b5:0317 | Mega Wo... | USB Game Controllers         | 1     | usbhid     | 0BA22D90BD |
| 11ff:001b |            | PXN-9613                     | 1     | usbhid     | C23CA49E1E |
| 11ff:001c |            | PXN-V3II                     | 1     | usbhid     | D1DED92F20 |
| 20d6:ca6d | Unknown... | Pro Ex                       | 1     | usbhid     | 0E26792709 |

### Mfp (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:042a | Hewlett... | LaserJet M1132 MFP           | 21    | usblp      | 2489824225 |
| 03f0:3b17 | Hewlett... | LaserJet M1005 MFP           | 19    | usblp      | 3A74ED7842 |
| 03f0:222a | Hewlett... | LaserJet Pro MFP M125rnw     | 11    | usblp      | EE184BFE51 |
| 03f0:5617 | Hewlett... | LaserJet M1120 MFP           | 11    | usblp      | 097B4FFD71 |
| 0924:3cef | Xerox      | Phaser 3100MFP               | 10    | usblp      | A2D36255B7 |
| 04b8:083f | Seiko E... | Stylus CX4300/CX4400/CX55... | 7     | usblp      | EDDD834850 |
| 03f0:5717 | Hewlett... | LaserJet M1120n MFP          | 6     | usblp      | 6697D98216 |
| 03f0:622a | Hewlett... | LaserJet MFP M129-M134       | 5     | usblp      | 1B55239D68 |
| 04b8:084d | Seiko E... | PX-402A [Stylus SX115/Sty... | 5     | usblp      | 105F0DC109 |
| 03f0:052a | Hewlett... | LaserJet Professional M12... | 4     | uas, us... | 5CE0BBFFF6 |
| 04b8:082f | Seiko E... | PX-A620 [Stylus CX3900/DX... | 4     | usblp      | 8B7624FF98 |
| 0924:42af | Xerox      | WorkCentre 3045B             | 4     | usblp      | 4E86FFF7EA |
| 03f0:062a | Hewlett... | LaserJet 100 colorMFP M175a  | 3     | usblp      | A3BBB7F8A9 |
| 03f0:3b2a | Hewlett... | Color LaserJet MFP M277dw    | 3     | usblp      | 240876777B |
| 04b8:080e | Seiko E... | PX-A550 [CX-3500/3600/365... | 3     | usblp      | 1AB6F30041 |
| 03f0:142a | Hewlett... | LaserJet 400 MFP M425dn      | 2     | usblp      | C496949B2E |
| 0482:0497 | Kyocera    | FS-1025MFP                   | 2     | usblp      | FD225327D7 |
| 04b8:0841 | Seiko E... | PX-401A [ME 300/Stylus NX... | 2     | usblp      | 75F89195A5 |
| 0924:3d6f | Xerox      | WorkCentre 6605DN            | 2     | usblp      | 79A097BF6F |
| 03f0:5a2a | Hewlett... | LaserJet MFP M426fdw         | 1     | usblp      | 2FB769F3C1 |
| 03f0:932a | Hewlett... | LaserJet Pro MFP M26nw       | 1     | usblp      | 30B096BF35 |
| 04b8:0818 | Seiko E... | Stylus CX3700/CX3800/DX3800  | 1     | usblp      | 25B3B9DEBA |
| 0924:42da | Xerox      | WorkCentre 3025              | 1     | usblp      | 70F28E74B9 |
| 0924:42dc | Xerox      | WorkCentre 3225              | 1     | usblp      | 8ABC3D5F35 |
| 413c:590b | Dell       | MFP                          | 1     | usblp      | 0F4BBAE4FA |

### Miscellaneous (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1e10:4000 | Point G... | Blackfly S BFS-U3-13Y3C      | 1     |            | 6F498D9D7D |

### Modem (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 12d1:1506 | Huawei ... | E398 LTE/UMTS/GSM Modem/N... | 161   | uas, us... | CD0F6202CB |
| 12d1:1436 | Huawei ... | E173 3G Modem (modem-mode)   | 18    | usb_sto... | E327FDD558 |
| 12d1:140c | Huawei ... | E180v                        | 16    | usb_sto... | 9A0C8FB6A2 |
| 12d1:1001 | Huawei ... | E161/E169/E620/E800 HSDPA... | 13    | option     | 4C163D8A71 |
| 12d1:1003 | Huawei ... | E220 HSDPA Modem / E230/E... | 10    | usb_sto... | D0B36608B4 |
| 1546:01a7 | U-Blox     | u-blox 7 - GPS/GNSS Receiver | 5     | cdc_acm    | 8C6609B568 |
| 04d8:00df | Microch... | MCP2200 USB Serial Port E... | 4     | cdc_acm    | C09E006619 |
| 0658:0200 | Sigma D... | Aeotec Z-Stick Gen5 (ZW09... | 4     | cdc_acm    | 3D0DACB49E |
| 2341:0043 | Arduino SA | Uno R3 (CDC ACM)             | 4     | cdc_acm    | E267629A35 |
| 04e2:1410 | Exar       | XR21V1410 USB-UART IC        | 2     | cdc_acm... | 44204F310C |
| 0baf:00ec | U.S. Ro... | 56K Faxmodem                 | 2     |            | EF974030B3 |
| 106c:3714 | Curitel... | PANTECH USB MODEM [UM175]    | 2     | cdc_acm    | 93E38006D9 |
| 106c:3716 | Curitel... | UMW190 Modem                 | 2     | cdc_acm    | 06FBB87A4E |
| 12d1:14ac | Huawei ... | HUAWEI Mobile                | 2     | uas, us... | 87AE047C32 |
| 12d1:1c1e | Huawei ... | Mass Storage                 | 2     | uas, us... | DDD57927D5 |
| 19d2:1515 | ZTE WCD... | MF192                        | 2     | cdc_acm    | FCB799A800 |
| 0421:026c | Nokia M... | N97 (PC Suite)               | 1     | cdc_acm    | 11D0F0433C |
| 0421:0302 | Nokia M... | N8-00                        | 1     | cdc_acm    | 495FCF02B1 |
| 0421:0355 | Nokia M... | Nokia X2-00                  | 1     | cdc_acm    | 1F5E6B026B |
| 0421:0664 | Nokia M... | Nokia 301 Dual SIM           | 1     | cdc_acm    | 17752ECF0C |
| 0451:16a8 | Texas I... | TI CC2531 USB CDC            | 1     | cdc_acm    | D4224E0573 |
| 0483:5740 | STMicro... | STM32F407                    | 1     | cdc_acm    | 919E37B101 |
| 04d8:fd5e | Microch... | VeRSis                       | 1     | cdc_acm    | AF6FC8545B |
| 04e8:6845 | Samsung... | Mobile USB Modem             | 1     | cdc_acm    | 01619C75A2 |
| 067b:2323 | Prolifi... | 123_AaP                      | 1     | cdc_acm    | D9A29354A7 |
| 0ace:1608 | ZyDAS      | ZyXEL Omni FaxModem 56K UNO  | 1     | cdc_acm    | 3489DBD51A |
| 0bf1:0002 | Intracom   | netMod Driver Ver 2.4 (CAPI) | 1     | cdc_acm    | 9DDAB18C28 |
| 0e8d:2011 | MediaTek   | PassPlus                     | 1     | cdc_acm    | 83BFA1527C |
| 1004:61f2 | LG Elec... | LGE Android phone            | 1     | cdc_acm    | 4273247882 |
| 1209:1776 | InterBi... | Io                           | 1     | system7... | ECC26A9233 |
| 12d1:151d | Huawei ... | Mass Storage                 | 1     | uas, us... | 304AA59840 |
| 1546:01a5 | U-Blox     | [u-blox 5]                   | 1     | cdc_acm    | FC4EBA57CC |
| 16c0:05e1 | Van Ooi... | Free shared USB VID/PID p... | 1     | cdc_acm    | C7A9B4C273 |
| 18d1:d012 | Google     | Android                      | 1     | rndis_host | 14C479036B |
| 1c11:b04d | Input Club | Keyboard - WhiteFox Parti... | 1     | cdc_acm    | 7A92C75597 |
| 1d50:6086 | OpenMoko   | OneRNG entropy device        | 1     | cdc_acm    | D42C7918FD |
| 1fc9:0083 | NXP Sem... | VCOM Port                    | 1     | cdc_acm    | AAA8608D22 |
| 1fc9:2002 | NXP Sem... | NXP LPC17xx VCOM             | 1     | cdc_acm    | E5901957AD |
| 20d3:0007 | Linux 2... | Gadget Serial v2.4           | 1     | cdc_acm    | 16ECB5A13F |
| 216f:0051 | Das U-Boot | U-Boot 2010.12               | 1     | cdc_acm    | BB85981F98 |
| 2184:0021 | GW Instek  | GDS-71062A                   | 1     | cdc_acm    | 26D6AB71AA |
| 2341:0001 | Arduino SA | Uno (CDC ACM)                | 1     | cdc_acm    | 515907EC21 |
| 2548:1002 | Pulse-E... | USB-CEC Adapter              | 1     | cdc_acm    | B4173B7EB7 |
| 27b1:0001 | UltiMac... | RAMBo                        | 1     | cdc_acm    | FFDEE4764D |

### Net/wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 148f:7601 | Ralink ... | MT7601U Wireless Adapter     | 189   | mt7601u    | DD4DA32934 |
| 0cf3:9271 | Qualcom... | AR9271 802.11n               | 171   | ath9k_htc  | 82E5D349D1 |
| 0bda:8179 | Realtek... | RTL8188EUS 802.11n Wirele... | 169   | r8188eu    | FE7535550F |
| 148f:5370 | Ralink ... | RT5370 Wireless Adapter      | 117   | rt2800usb  | C6FDAC0AEE |
| 0bda:8178 | Realtek... | RTL8192CU 802.11n WLAN Ad... | 116   | rtl8192cu  | 5F6E1A3B61 |
| 148f:3070 | Ralink ... | RT2870/RT3070 Wireless Ad... | 80    | rt2800usb  | F664FB0C42 |
| 0bda:8176 | Realtek... | RTL8188CUS 802.11n WLAN A... | 69    | rtl8192cu  | EF1765E325 |
| 2001:3c20 | D-Link     | 802.11 n WLAN                | 33    | rt2800usb  | 3F04B83DFD |
| 0bda:8172 | Realtek... | RTL8191SU 802.11n WLAN Ad... | 27    | r8712u     | 65A9CF6ADE |
| 2357:0109 | Realtek    | RTL8192EU TL-WN823N 802.1... | 27    | rtl8xxxu   | 81D8954A1C |
| 0bda:818b | Realtek... | RTL8192EU ACT-WNP-UA-005 ... | 25    | rtl8xxxu   | 9CD2D7F72B |
| 045e:0719 | Microsoft  | Xbox 360 Wireless Adapter    | 24    | xpad       | ED7B2348A1 |
| 07d1:3c16 | D-Link ... | DWA-125 Wireless N 150 Ad... | 23    | rt2800usb  | 22D8D62C57 |
| 0846:9030 | NetGear    | WNA1100 Wireless-N 150 [A... | 21    | ath9k_htc  | B86DF74030 |
| 0b05:1786 | ASUSTek... | USB-N10 802.11n Network A... | 21    | r8712u     | B2D5544528 |
| 0cf3:7015 | Qualcom... | TP-Link TL-WN821N v3 / TL... | 21    | ath9k_htc  | DBCE9BE550 |
| 148f:5372 | Ralink ... | RT5372 Wireless Adapter      | 21    | rt2800usb  | FF78BFBB07 |
| 2357:010c | Realtek    | RTL8188EUS TL-WN722N v2      | 21    | r8188eu    | 5B8AE7E31F |
| 0b05:17ab | ASUSTek... | USB-N13 802.11n Network A... | 20    | rtl8192cu  | A20DD4DABA |
| 0bda:b812 | Realtek... | RTL8812BU USB3.0 802.11ac... | 20    | 8822bu     | 2703EE0766 |
| 0cf3:1006 | Qualcom... | TP-Link TL-WN322G v3 / TL... | 20    | ath9k_htc  | 53C6C139DA |
| 148f:5572 | Ralink ... | RT5572 Wireless Adapter      | 20    | rt2800usb  | 0C9D9C254F |
| 148f:761a | Ralink ... | MT7610U ("Archer T2U" 2.4... | 20    | mt76x0u    | AAA77437D5 |
| 0b05:17ba | ASUSTek... | N10 Nano 802.11n Network ... | 19    | rtl8192cu  | 4C45EB1803 |
| 0bda:8171 | Realtek... | RTL8188SU 802.11n WLAN Ad... | 19    | r8712u     | 9CD7C321BD |
| 148f:2573 | Ralink ... | RT2501/RT2573 Wireless Ad... | 19    | rt73usb    | 08FB21590A |
| 2001:3319 | D-Link     | RTL8192EU DWA-131 Wireles... | 19    | rtl8xxxu   | A95491B363 |
| 2001:3c19 | D-Link     | DWA-125 Wireless N 150 Ad... | 18    | rt2800usb  | 4C91A0DF19 |
| 0bda:8812 | Realtek... | RTL8812AU 802.11a/b/g/n/a... | 16    | 8812au     | 8C6609B568 |
| 0b05:179d | ASUSTek... | USB-N53 802.11abgn Networ... | 14    | rt2800usb  | 0C9CE69911 |
| 13d3:3323 | IMC Net... | RTL8191S WLAN Adapter        | 14    | r8712u     | 191844565C |
| 7392:7811 | Edimax ... | EW-7811Un 802.11n Wireles... | 14    | rtl8192... | BC9F90FD94 |
| 07d1:3303 | D-Link ... | DWA-131 802.11n Wireless ... | 13    | r8712u     | C5EE060717 |
| 07d1:3c03 | D-Link ... | AirPlus G DWL-G122 Wirele... | 13    | rt73usb    | 8AE6B6F651 |
| 0b05:1791 | ASUSTek... | WL-167G v3 802.11n Adapte... | 13    | r8712u     | BA1AF12DA4 |
| 07d1:3a10 | D-Link ... | DWA-126 802.11n Wireless ... | 12    | ath9k_htc  | 7B96F419AD |
| 07d1:3c07 | D-Link ... | DWA-110 Wireless G Adapte... | 11    | rt73usb    | 2F49EDDD49 |
| 13d3:3306 | IMC Net... | Mediao 802.11n WLAN [Real... | 11    | r8712u     | 0BE5F7A9E8 |
| 0bda:0811 | Realtek... | RTL8811AU 802.11ac WLAN A... | 10    | rtl8812au  | 5D65FDCE91 |
| 0bda:a811 | Realtek... | RTL8811AU 802.11a/b/g/n/a... | 10    | rtl8812au  | 96B4E832EE |
| 148f:3072 | Ralink ... | RT3072 Wireless Adapter      | 10    | rt2800usb  | A3BF15A0BB |
| 050d:845a | Belkin ... | F7D2101 802.11n Surf & Sh... | 9     | r8712u     | EC17A422A8 |
| 07d1:3c0d | D-Link ... | DWA-125 Wireless N 150 Ad... | 9     | rt2800usb  | CC88E139CC |
| 0bda:c811 | Realtek... | RTL8811CU 802.11ac NIC       | 9     | 8821cu     | 327877AB6E |
| 0bda:8189 | Realtek... | RTL8187B Wireless 802.11g... | 8     | rtl8187    | B26ED41AB8 |
| 0bda:f179 | Realtek... | 802.11n                      | 8     | rtl8188fu  | CCE7F9292D |
| 2357:0107 | Realtek    | RTL8192EU TL-WN821N Versi... | 8     | 8192eu     | 3A8D19C8FC |
| 050d:2103 | Belkin ... | F7D2102 802.11n N300 Micr... | 7     | rtl8192... | C9CCBD7C3F |
| 0bda:0179 | Realtek... | RTL8188ETV Wireless LAN 8... | 7     | r8188eu    | CDCFCEEBFD |
| 0e8d:7610 | MediaTek   | MT7610U WiFi                 | 7     | mt7650u... | F16EC522A6 |
| 148f:2870 | Ralink ... | RT2870 Wireless Adapter      | 7     | rt2800usb  | 6F259EC9AA |
| 15a9:0004 | Gemtek     | WUBR-177G [Ralink RT2571W]   | 7     | rt73usb    | 3A9AB88271 |
| 2357:0101 | Realtek    | RTL8812AU Archer T4U 802.... | 7     | rtl8812au  | BF63570D7D |
| 0586:341f | ZyXEL C... | NWD2205 802.11n Wireless ... | 6     | rtl8192cu  | B79EE752B4 |
| 0846:9052 | NetGear    | RTL8811AU A6100 AC600 DB ... | 6     | rtl8812au  | AA73E69EE4 |
| 2001:3317 | D-Link     | 802.11 n WLAN                | 6     | rt2800usb  | BE6D815002 |
| 2001:3c1b | D-Link     | DWA-127 Wireless N 150 Hi... | 6     | rt2800usb  | 22FADF1971 |
| 7392:7711 | Edimax ... | EW-7711UTn nLite Wireless... | 6     | rt2800usb  | CB4983BAF6 |
| 0586:3410 | ZyXEL C... | ZyAIR G-202 802.11bg         | 5     | zd1211rw   | BBCFFE1ECE |
| 0586:341e | ZyXEL C... | NWD2105 802.11bgn Wireles... | 5     | rt2800usb  | B8931B91E5 |
| 0846:4260 | NetGear    | WG111v3 54 Mbps Wireless ... | 5     | rtl8187    | 6BDDC45E74 |
| 0846:9020 | NetGear    | BCM43231 WNA3100(v1) Wire... | 5     |            | 68CF43B792 |
| 0ace:1215 | ZyDAS      | ZD1211B 802.11g              | 5     | zd1211rw   | 9CB261390B |
| 0b05:1723 | ASUSTek... | WL-167G v2 802.11g Adapte... | 5     | rt73usb    | 24F00A45BF |
| 0cf3:1002 | Qualcom... | TP-Link TL-WN821N v2 / TL... | 5     | carl9170   | A8D0FA2257 |
| 0db0:3871 | Micro S... | MS-3871 802.11bgn Wireles... | 5     | rt2800usb  | 0F152D6AD7 |
| 148f:2070 | Ralink ... | RT2070 Wireless Adapter      | 5     | rt2800usb  | 9AD73F77F9 |
| 148f:3572 | Ralink ... | RT3572 Wireless Adapter      | 5     | rt2800usb  | 21E173D41F |
| 148f:760b | Ralink ... | MT7601U Wireless Adapter     | 5     | mt7601u    | 10121DE278 |
| 2001:3314 | D-Link     | RTL8821AU DWA-171 802.11n... | 5     | rtl8812au  | 10BA468B45 |
| 079b:0062 | Sagem      | XG-76NA 802.11bg             | 4     | zd1211rw   | CD0F6202CB |
| 07d1:3c09 | D-Link ... | DWA-140 RangeBooster N Ad... | 4     | rt2800usb  | F01EC9BA23 |
| 0b05:17e8 | ASUSTek... | USB-N14 802.11b/g/n (2x2)... | 4     | rt2800usb  | CE7F216DFB |
| 0b05:184c | ASUSTek... | RTL8812BU USB-AC53 Nano 8... | 4     |            | B27681EA0E |
| 13b1:003f | Linksys    | RTL8812AU WUSB6300 802.11... | 4     | 8812au     | FDC986FDE7 |
| 13d3:3247 | IMC Net... | AW-NU222 802.11bgn Wirele... | 4     | rt2800usb  | 31559386B2 |
| 1737:0078 | Linksys    | WUSB100 v2 RangePlus Wire... | 4     | rt2800usb  | 4B7960D4AB |
| 2001:3315 | D-Link     | RTL8812AU DWA-182 Wireles... | 4     | rtl8812au  | CA162546EE |
| 2357:0108 | TP-Link    | RTL8192EU TL-WN822N Versi... | 4     | rtl8xxxu   | 6AD994E2BF |
| 2717:4106 | MediaTek   | MI WLAN Adapter              | 4     | mt7601u    | F9B65F1415 |
| 050d:705a | Belkin ... | F5D7050 Wireless G Adapte... | 3     | rt73usb    | 2BF7B65239 |
| 050d:815c | Belkin ... | F5D8053 N Wireless USB Ad... | 3     | rt2800usb  | B7DB1F6D08 |
| 07d1:3a09 | D-Link ... | DWA-160 802.11abgn Xtreme... | 3     | carl9170   | 4E72D045DC |
| 0846:9011 | NetGear    | BCM4323 WNDA3100v2 802.11... | 3     |            | E1F01EA189 |
| 0b05:17d1 | ASUSTek... | MT7610U AC51 802.11a/b/g/... | 3     |            | 48E75D806A |
| 13b1:003a | Linksys    | BCM43236 AE2500 802.11abg... | 3     |            | B703E66C25 |
| 2001:3c1a | D-Link     | DWA-160 802.11abgn Xtreme... | 3     | rt2800usb  | 7BA347026E |
| 2001:3c22 | D-Link     | 802.11 n WLAN                | 3     | rt2800usb  | 07183070ED |
| 20f4:648b | TRENDnet   | TEW-648UBM 802.11n 150Mbp... | 3     | rtl8192... | 0DF514358A |
| 2357:0100 | Realtek    | RTL8192CU TL-WN8200ND        | 3     | rtl8192cu  | 6FB7A2E4D4 |
| 2357:010d | Realtek    | RTL8812AU TP-Link Archer ... | 3     | 8812au     | 130C16E46D |
| 2357:0115 | TP-Link    | 802.11ac NIC                 | 3     | 8822bu     | 54A49FCBF7 |
| 2604:0012 | Realtek    | RTL8812AU Tenda U12 802.1... | 3     |            | C1F0DB0D58 |
| 0411:0242 | BUFFALO    | RTL8811AU WI-U2-433DM 802... | 2     |            | D92D4F0666 |
| 050d:1102 | Belkin ... | F7D1102 N150/Surf Micro W... | 2     | rtl8192cu  | 40F56D3108 |
| 050d:935b | Belkin ... | F6D4050 N150 Enhanced Wir... | 2     | rt2800usb  | A2DBF52E17 |
| 0846:9012 | NetGear    | WNDA4100 802.11abgn 3x3:3... | 2     | rt2800usb  | 63DE98D3F1 |
| 0846:9021 | NetGear    | WNA3100M(v1) Wireless-N 3... | 2     | rtl8192... | 33687A527A |
| 0b05:1784 | ASUSTek... | USB-N13 802.11n Network A... | 2     | rt2800usb  | 6B863A586E |
| 0b05:17ad | ASUSTek... | 802.11 n WLAN                | 2     | rt2800usb  | 66E92FA977 |

### Network (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 19d2:1405 | ZTE WCD... | ZTE Mobile Broadband Station | 50    | cdc_ether  | C0EABA1044 |
| 0bda:8187 | Realtek... | RTL8187 Wireless LAN Adapter | 41    | rtl8187    | 29B7777E42 |
| 12d1:14db | Huawei ... | E353/E3131 34GB              | 35    | cdc_ether  | 4C56265E1C |
| 04e8:6863 | Samsung... | GT-I9500 [Galaxy S4] / GT... | 28    | rndis_host | 7267B22360 |
| 2001:3c15 | D-Link     | DWA-140 RangeBooster N Ad... | 24    | rt2800usb  | CA130B13F3 |
| 07d1:3c0a | D-Link ... | DWA-140 RangeBooster N Ad... | 22    | rt2800usb  | 3549E0C30D |
| 0bb4:0003 | HTC (Hi... | Android Incorporated GSM ... | 20    | rndis_host | 00484769BC |
| 2717:ff80 | Android    | SDM636-MTP _SN:5F93851E      | 18    | rndis_host | DE79E2EEA8 |
| 2001:330f | D-Link     | RTL8188ETV DWA-125 11n Ad... | 17    | r8188eu    | 969A371A99 |
| 12d1:108a | Huawei ... | DLI-TL20                     | 13    | rndis_host | F6B9026258 |
| 0bb4:0004 | HTC (Hi... | MT65xx Android Phone         | 10    | rndis_host | 39B777FE86 |
| 0e8d:2004 | MediaTek   | Power Ice Evo                | 10    | rndis_host | 822571FC94 |
| 1076:8002 | GCT Sem... | LU150 LTE Modem [Yota LU150] | 9     | rndis_host | 6999B8686F |
| 15a9:002d | Gemtek     | WLTUBA-107 [Yota 4G LTE]     | 9     | cdc_ether  | 9057FD4986 |
| 1bbb:0174 | T & A M... | ALCATEL ONETOUCH PIXI 3 (... | 9     | rndis_host | 69ED9F1FFC |
| 2001:3c1e | D-Link     | 11n Adapter                  | 9     | rt2800usb  | F1FF7D2409 |
| 1782:5d20 | Spreadt... | Fly Era Nano 3               | 8     | rndis_host | 753ED60051 |
| 04e8:6864 | Samsung... | GT-I9070 (network tetheri... | 7     | rndis_host | C58FEB4F91 |
| 0bda:8153 | Realtek... | RTL8153 Gigabit Ethernet ... | 7     | r8152      | 62B0B05A66 |
| 0b95:1790 | ASIX El... | AX88179 Gigabit Ethernet     | 6     | ax88179... | BC13E44A9B |
| 0e8d:2005 | MediaTek   | X5max_PRO                    | 6     | rndis_host | 74171D7162 |
| 0846:9053 | NetGear    | A6210                        | 5     | mt7662u... | FB6393C3D5 |
| 0bda:8152 | Realtek... | RTL8152 Fast Ethernet Ada... | 5     | r8152      | 7A88DA2451 |
| 7392:a812 | Edimax ... | RTL8811AU AC600 USB          | 5     | rtl8812au  | 409FAE415E |
| 0bda:0823 | Realtek... | 802.11ac WLAN Adapter        | 4     | btusb      | 7F43E2651E |
| 0bda:b720 | Realtek... | RTL8723BU 802.11n WLAN Ad... | 4     | btusb      | 2FB24608FB |
| 2001:3c21 | D-Link     | DWA-160 Xtreme N Dual Ban... | 4     | rt2800usb  | 8418BEF88A |
| 0586:3309 | ZyXEL C... | ADSL Modem Prestige 600 s... | 3     | rndis_w... | 316CFC71D0 |
| 05c6:f00e | Qualcomm   | DEXP Ixion X LTE 4.5"        | 3     | rndis_host | 5DC57BDE0C |
| 0846:6a00 | NetGear    | WG111v2 54 Mbps Wireless ... | 3     | rtl8187    | 501A01E376 |
| 0b05:5602 | ASUSTek... | Android                      | 3     | rndis_host | 4DD77DD778 |
| 0b95:772b | ASIX El... | AX88772B                     | 3     | asix       | 82BBDAF0A1 |
| 0fce:7161 | Sony Er... | ST18i                        | 3     | rndis_host | 9DF4057415 |
| 1004:6344 | LG Elec... | G2 Android Phone [tetheri... | 3     | rndis_host | EFDAEDB573 |
| 1271:052a | Android    | Android                      | 3     | rndis_host | 407C4DAF14 |
| 15a9:003e | Gemtek     | Yota Many                    | 3     | rndis_host | F48E23AB6A |
| 17ef:7436 | Lenovo     | MT65xx Android Phone         | 3     | rndis_host | 2FB1ED8374 |
| 19d2:1365 | ZTE WCD... | MT65xx Android Phone         | 3     | rndis_host | AD77614F78 |
| 2001:3310 | D-Link     | DWA-123 11n Adapter          | 3     | r8188eu    | 180D33B399 |
| 2001:3318 | D-Link     | 11ac adapter                 | 3     | rtl8812au  | ADF45BCCC3 |
| 2001:4a00 | D-Link     | DUB-1312                     | 3     | ax88179... | 0818F19E7A |
| 216f:0043 | Altair ... | Modem YOTA 4G LTE            | 3     | cdc_ether  | 03602DBEA6 |
| 22b8:2e24 | Motorol... | Moto G (4)                   | 3     | rndis_host | A4C8471B20 |
| 2357:0601 | TP-Link    | RTL8153 TP-Link UE300 USB... | 3     | r8152      | B1EB5D5F20 |
| 2717:1280 | JSR Tech   | HM1 Android Phone            | 3     | rndis_host | B81345909F |
| 2e04:c022 | MediaTek   | Android                      | 3     | rndis_host | 5CE732DF30 |
| 04e8:6881 | Samsung... | Android USB Device           | 2     | rndis_host | 6B2E5020C2 |
| 056a:0084 | Wacom      | Wireless adapter for Bamb... | 2     | usbhid     | CC894A09CE |
| 0a46:1269 | Davicom... | DM9621 USB To Fast Ether     | 2     | dm9601,... | 019B115510 |
| 0b95:1780 | ASIX El... | AX88178                      | 2     | asix       | E19770A9ED |
| 0b95:7720 | ASIX El... | AX88772                      | 2     | asix       | CEA175B6F3 |
| 0bda:8150 | Realtek... | RTL8150 Fast Ethernet Ada... | 2     | rtl8150    | 9A4C5DE0DD |
| 0fe6:9700 | Kontron... | DM9601 Fast Ethernet Adapter | 2     | dm9601     | 3CE6D97F47 |
| 12d1:1050 | Huawei ... | Android Adapter              | 2     | rndis_host | D00D9367B6 |
| 1390:5454 | TOMTOM     | Blue & Me 2                  | 2     | cdc_ether  | 744A3F2E54 |
| 18d1:4ee3 | Google     | Nexus 4/5/7/10 (tether)      | 2     | rndis_host | B80BDB1F75 |
| 19d2:1373 | ZTE WCD... | Android                      | 2     | rndis_host | 1276D33239 |
| 03f0:1c1d | Hewlett... | RNDIS                        | 1     | rndis_w... | F9E96B988C |
| 0489:c022 | Foxconn... | Nokia 8                      | 1     | rndis_host | 288A139263 |
| 04d9:a119 | Holtek ... | OSA Express Network card     | 1     | usbhid     | B8451B8A40 |
| 050d:0121 | Belkin ... | F5D5050 100Mbps Ethernet     | 1     | btusb      | AFEEB9E46C |
| 050d:7051 | Belkin ... | F5D7051 802.11g Adapter v... | 1     | rt2500usb  | 142EC69E09 |
| 056e:4008 | Elecom     | WDC-150SU2M                  | 1     | r8188eu    | D1F1DD8C96 |
| 0583:2060 | Padix (... | USB,2-axis 8-button gamepad  | 1     | usbhid     | 082C541787 |
| 05ac:12ab | Apple      | iPad 4/Mini1                 | 1     | usbfs      | 0D7F7B5382 |
| 05c6:6001 | Qualcomm   | mobile device                | 1     | rndis_host | BEF8DB955F |
| 05c6:676a | Qualcomm   | A0001                        | 1     | rndis_host | B806B8B07F |
| 07b2:5101 | Motorol... | SurfBoard SB5101 Cable Modem | 1     | cdc_ether  | C9DE38152E |
| 07d1:3a07 | D-Link ... | WUA-2340 RangeBooster G A... | 1     | ar5523     | 13D30EC9C7 |
| 0955:cf03 | Nvidia     | Tegra NOTE                   | 1     | rndis_host | 3F12BEB1EC |
| 0a46:9601 | Davicom... | DM9601 Fast Ethernet Adapter | 1     | dm9601     | 1790EBC617 |
| 0b05:1717 | ASUSTek... | WL169gE 802.11g Adapter [... | 1     | rndis_w... | CC92196DD4 |
| 0b05:532f | ASUSTek... | Android                      | 1     | rndis_host | 9F506FB371 |
| 0b05:5483 | ASUSTek... | Android                      | 1     | rndis_host | E5C66B0A72 |
| 0b05:5f04 | ASUSTek... | Android                      | 1     | rndis_host | FC15C02A32 |
| 0b05:7783 | ASUSTek... | Android                      | 1     | rndis_host | 72886E4425 |
| 0b95:1720 | ASIX El... | 10/100 Ethernet              | 1     | usbnet,... | B778A6096A |
| 0b95:772a | ASIX El... | AX88772A Fast Ethernet       | 1     | asix       | 4D498130D3 |
| 0bb4:0eca | HTC (Hi... | Android USB Device           | 1     | rndis_host | 27214865F8 |
| 0bb4:0ee7 | HTC (Hi... | Desire 620G dual sim         | 1     | rndis_host | BA6F321C12 |
| 0bb4:0ffe | HTC (Hi... | Desire HD (modem mode)       | 1     | rndis_w... | C5A4767CED |
| 0bda:8050 | Realtek... | USB 10/100 LAN               | 1     | cdc_ether  | EAE47F7347 |
| 0db0:6874 | Micro S... | RT2573                       | 1     | rt73usb    | 0E8628D300 |
| 0fca:8017 | Researc... | BlackBerry                   | 1     | cdc_ncm... | 85F6EF75DF |
| 0fce:719b | Sony Er... | Android                      | 1     | rndis_host | 2153C318F3 |
| 0fce:719e | Sony Er... | C6903                        | 1     | rndis_host | 013C177927 |
| 0fce:71ad | Sony Er... | Android                      | 1     | rndis_host | E35AD83ABB |
| 0fce:71b5 | Sony Er... | D2005                        | 1     | rndis_host | 5C06CE0C24 |
| 0fce:71bc | Sony Er... | D2203                        | 1     | rndis_host | 7EF8DB4E5A |
| 0fce:818c | Sony Er... | Android                      | 1     | rndis_host | 1E85560547 |
| 0fe6:8101 | ICS Advent | DM9601 Fast Ethernet Adapter | 1     | dm9601     | 7C707CF755 |
| 1004:61d9 | LG Elec... | P500                         | 1     | rndis_host | 73BEEC2582 |
| 1004:61da | LG Elec... | G2 Android Phone [tetheri... | 1     | rndis_host | 5AAA02B75A |
| 12d1:1575 | Huawei ... | K5150 LTE modem              | 1     | cdc_ether  | 27FE9DBBFB |
| 12d1:1c50 | Huawei ... | MOBILE                       | 1     | cdc_ether  | 2168E29CD2 |
| 12d1:260d | Huawei ... | TIT-L01                      | 1     | rndis_host | EE6E754C4C |
| 1376:4e61 | Vimtron... | Mobile Composite Device Bus  | 1     | rndis_host | 8049F0AC94 |
| 1385:4250 | Netgear    | WG111T                       | 1     | ar5523     | A5A6F1E06B |
| 13b1:0018 | Linksys    | USB200M 10/100 Ethernet A... | 1     | asix       | B778A6096A |
| 15a9:0030 | Gemtek     | Wi-Fi Modem Yota 4G LTE      | 1     | rndis_host | 199632408F |

### Phone (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 2a45:2008 | Meizu      | MX Phone (MTP)               | 7     | usbfs      | BCF1D99475 |
| 1bbb:0168 | T & A M... | ALCATEL ONETOUCH PIXI 3 (... | 5     | usbfs      | 8D849C010B |
| 2717:ff48 | MediaTek   | MT65xx Android Phone         | 5     | usbfs      | D6DD84E864 |
| 1004:631c | LG Elec... | G2/Optimus Android Phone ... | 4     | usbfs      | AC6AA4CE59 |
| 05c6:f003 | Qualcomm   | vivo Android Phone           | 3     | usbfs      | 0D7F7B5382 |
| 0bb4:0c02 | HTC (Hi... | Dream / ADP1 / G1 / Magic... | 3     |            | 1C66419356 |
| 1004:6300 | LG Elec... | G2/Optimus Android Phone ... | 3     | usbhid     | 3A3714805B |
| 18d1:4ee1 | Google     | Nexus Device (MTP)           | 3     |            | BF6F02A425 |
| 19d2:0307 | ZTE WCD... | Android Phone                | 3     | usbfs      | 090D61FAD7 |
| 0bb4:2008 | HTC (Hi... | Android Phone via MTP [Wi... | 2     |            | 98FF7EAA07 |
| 0fce:01b5 | Sony Er... | Xperia E1 D2005              | 2     |            | 165BEE2B5C |
| 0fce:01c4 | Sony Er... | Xperia M4                    | 2     | usbfs      | 06472F3D2C |
| 17ef:7497 | Lenovo     | A789 (MTP mode)              | 2     |            | 2D2EB09845 |
| 19d2:0306 | ZTE WCD... | MT65xx Android Phone         | 2     |            | 51E3920DB2 |
| 0421:002f | Nokia M... | 6120 Phone (PC-Suite mode)   | 1     | cdc_acm... | 408A3116F5 |
| 045e:04ec | Microsoft  | Windows Phone (Zune)         | 1     |            | D4F8B5C1D6 |
| 0471:0003 | Philips... | Android Phone                | 1     | rndis_host | 5F5A42CFCD |
| 0471:2008 | Philips... | Android Phone                | 1     |            | D315AFD57C |
| 04e8:685c | Samsung... | GT-I9250 Phone [Galaxy Ne... | 1     |            | 769DC89006 |
| 04e8:685d | Samsung... | GT-I9100 Phone [Galaxy S ... | 1     | cdc_acm    | 1DC2303974 |
| 05c6:9092 | Qualcomm   | DEXP Ixion X LTE 4.5"        | 1     |            | 041AA23640 |
| 0b05:5490 | ASUSTek... | ZenFone                      | 1     |            | 196E542A2B |
| 0b05:551f | ASUSTek... | Android Phone                | 1     | usbfs      | E1C6C1D0DB |
| 0bb4:0c81 | HTC (Hi... | Android Phone                | 1     |            | DD80C13918 |
| 0bb4:0fa2 | HTC (Hi... | Android Phone                | 1     | usbfs      | 5A59F3A3B4 |
| 0bb4:f006 | HTC (Hi... | Android Phone                | 1     | usbhid     | 36497E7C3F |
| 0fce:0189 | Sony Er... | Xperia ZL C6503              | 1     |            | 3436D9709A |
| 0fce:5195 | Sony Er... | Xperia SP C5303              | 1     |            | 481DCFBCF9 |
| 0fce:51ad | Sony Er... | Android Phone                | 1     |            | 41B2D085B5 |
| 1004:61fc | LG Elec... | Optimus 3                    | 1     | cdc_acm    | 24B4EE4228 |
| 1004:61fe | LG Elec... | Optimus Android Phone [US... | 1     | cdc_acm    | 4A83E029A5 |
| 1527:0200 | Silicon... | YAP Phone (no firmware)      | 1     |            | D857FD97FD |
| 17ef:0c02 | Lenovo     | MT65xx Android Phone         | 1     | usbfs      | B776DA24C5 |
| 17ef:74f8 | Lenovo     | MT65xx Android Phone         | 1     |            | 33B48E1C58 |
| 17ef:79a1 | Lenovo     | MT65xx Android Phone         | 1     |            | 9271AE1167 |
| 2717:1240 | Xiaomi     | HM1 Android Phone            | 1     |            | 73772C2940 |
| 2916:f003 | Yota De... | YotaPhone C9660              | 1     | usbfs      | 5595625922 |
| 2922:0003 | MediaTek   | MT65xx Android Phone         | 1     | rndis_host | 897D97A18D |

### Printer (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:2b17 | Hewlett... | LaserJet 1020                | 44    | usblp      | C4C3165448 |
| 03f0:002a | Hewlett... | LaserJet P1102               | 31    | usblp      | 00FC2B5C89 |
| 03f0:4117 | Hewlett... | LaserJet 1018                | 31    | usblp      | 3E3879E6A5 |
| 04a9:2676 | Canon      | CAPT Device                  | 26    | usblp      | A9ED28807E |
| 04e8:341b | Samsung... | SCX-4200 series              | 25    | usblp      | 56D8AE9D24 |
| 04b8:002a | Seiko E... | USB2.0 Printer (Hi-speed)    | 22    | usblp      | 64F39F56BD |
| 04e8:344f | Samsung... | SCX-3400 Series              | 21    | usblp      | B52FC761F9 |
| 03f0:0c17 | Hewlett... | LaserJet 1010                | 19    | usblp      | 645B5E5FF4 |
| 067b:2305 | Prolifi... | PL2305 Parallel Port         | 18    | usblp      | E969E52D33 |
| 04a9:2737 | Canon      | MF4410                       | 17    | usblp      | DC34CF7D36 |
| 04a9:2759 | Canon      | MF3010                       | 16    | usblp      | 2DE6892C13 |
| 03f0:3d17 | Hewlett... | LaserJet P1005               | 15    | usblp      | 0ADA104D17 |
| 04b8:0007 | Seiko E... | Printer                      | 15    | usblp      | DB1A79AC69 |
| 04da:0f0b | Panason... | KX-MB1520RU                  | 15    | usblp      | A606FF5D08 |
| 04e8:3441 | Samsung... | SCX-3200 Series              | 15    | usblp      | 07F6824787 |
| 04f9:0027 | Brother... | HL-2030 Laser Printer        | 15    | usblp      | 98A829DCFD |
| 04b8:0005 | Seiko E... | Printer                      | 14    | usblp      | B174073C45 |
| 04e8:3292 | Samsung... | ML-1640 Series Laser Printer | 14    | usblp      | 3759770E23 |
| 04b8:08a1 | Seiko E... | L210 Series                  | 13    | usblp      | EED3020AA4 |
| 04e8:3469 | Samsung... | M2070 Series                 | 12    | usblp      | 88A8AEBAF0 |
| 03f0:0317 | Hewlett... | LaserJet 1200                | 11    | usblp      | B3E8FFCC97 |
| 04a9:262b | Canon      | LaserShot LBP-1120 Printer   | 11    | usblp      | D425CA175B |
| 03f0:8711 | Hewlett... | Deskjet 2050 J510            | 10    | usblp      | F3C86A9592 |
| 04a9:10dc | Canon      | iP7200 series                | 10    | usblp      | B25EC7E75A |
| 04e8:3321 | Samsung... | M2020 Series                 | 10    | usblp      | 3C4C1E40C5 |
| 03f0:1d17 | Hewlett... | LaserJet 1320                | 9     | usblp      | 308392F74D |
| 04a9:173a | Canon      | PIXMA MP250 series printer   | 9     | usblp      | 915FD7548F |
| 04a9:176c | Canon      | MG2400 series                | 9     | usblp      | 9DD0EDCEF8 |
| 04a9:260a | Canon      | CAPT Printer                 | 9     | usblp      | 98686D396A |
| 04a9:26da | Canon      | LBP3010B printer             | 9     | usblp      | 456F676592 |
| 04e8:3413 | Samsung... | SCX-4100 Scanner             | 9     | usblp      | 9A87AA9B0D |
| 04f9:003f | Brother... | HL-2130 series               | 9     | usblp      | 5C2A8BB899 |
| 03f0:0517 | Hewlett... | LaserJet 1000                | 8     | usblp      | 3AF8833313 |
| 03f0:1017 | Hewlett... | LaserJet 1300                | 8     | usblp      | FD6FD654DB |
| 03f0:2504 | Hewlett... | DeskJet F4200 series         | 8     | usblp      | 28DFB478B7 |
| 03f0:2c17 | Hewlett... | LaserJet 1022                | 8     | usblp      | 9E845B6F0F |
| 03f0:e111 | Hewlett... | DeskJet 2130 series          | 8     | usblp      | 5AEB4ABFE6 |
| 04a9:2771 | Canon      | CAPT USB Device              | 8     | usblp      | 660F205495 |
| 04e8:300c | Samsung... | ML-1210 Printer              | 8     | usblp      | 9E9C06CB6F |
| 04e8:326c | Samsung... | ML-2010P Mono Laser Printer  | 8     | usblp      | B63EF41E02 |
| 0924:3cf4 | Xerox      | Phaser 3140 and 3155         | 8     | usblp      | A15E1952DA |
| 1a86:7584 | QinHeng... | CH340S                       | 8     | usblp      | A4F1D5BD20 |
| 03f0:3e17 | Hewlett... | LaserJet P1006               | 7     | usblp      | BEBC187DBD |
| 04a9:2684 | Canon      | MF3200 series                | 7     | usblp      | 5149320E81 |
| 04a9:26b4 | Canon      | MF4010 series                | 7     | usblp      | A3A56478BC |
| 04a9:271a | Canon      | CAPT USB Device              | 7     | usblp      | 980DE31D88 |
| 04b8:0883 | Seiko E... | ME 340 Series/Stylus NX13... | 7     | usblp      | 7C707CF755 |
| 03f0:7e04 | Hewlett... | DeskJet F4100 Printer series | 6     | usblp      | 413AE4FF4F |
| 03f0:c111 | Hewlett... | Deskjet 1510                 | 6     | usblp      | B3C05679A9 |
| 03f0:d711 | Hewlett... | ENVY 4520 series             | 6     | usblp      | 8FFAA505B6 |
| 04a9:175f | Canon      | PIXMA MP230 series           | 6     | usblp      | EEEE376F38 |
| 04a9:176d | Canon      | PIXMA MG2500 Series          | 6     | usblp      | 1EA43A3881 |
| 04e8:3301 | Samsung... | ML-1660 Series               | 6     | usblp      | 66E92FA977 |
| 04e8:330f | Samsung... | ML-216x Series Laser Printer | 6     | usblp      | 5D1E770980 |
| 04f9:0273 | Brother... | DCP-7057 scanner/printer     | 6     | usblp      | FE44640E41 |
| 03f0:1204 | Hewlett... | DeskJet 930c                 | 5     | usblp      | 435E64F300 |
| 03f0:2d12 | Hewlett... | Officejet 4500 G510g-m       | 5     | usblp      | 9DCEE0A57A |
| 03f0:3817 | Hewlett... | LaserJet P2015 series        | 5     | usblp      | 0C46A0B5DB |
| 03f0:7d04 | Hewlett... | DeskJet F2100 Printer series | 5     | usblp      | 72FFD05528 |
| 04a9:10d3 | Canon      | iP2700 series                | 5     | usblp      | 835B504D5C |
| 04a9:10d8 | Canon      | iP4900 series                | 5     | usblp      | 39B777FE86 |
| 04a9:271c | Canon      | CAPT USB Device              | 5     | usblp      | 3602A90378 |
| 04b8:004c | Seiko E... | L110 Series                  | 5     | usblp      | DE463D111A |
| 04b8:08a8 | Seiko E... | L355 Series                  | 5     | usblp      | 86314575F7 |
| 04b8:08d1 | Seiko E... | L222 Series                  | 5     | usblp      | 964D7E69EB |
| 04e8:325b | Samsung... | Xerox Phaser 3117 Laser P... | 5     | usblp      | 4068C43059 |
| 04e8:3268 | Samsung... | ML-1610 Mono Laser Printer   | 5     | usblp      | 9D3DBD169B |
| 04e8:342e | Samsung... | SCX-4300 Series              | 5     | usblp      | 3719375A02 |
| 0924:3cf7 | Xerox      | WorkCentre 3315              | 5     | usblp      | 629ABB99C2 |
| 0924:4265 | Xerox      | WorkCentre 3119 Series       | 5     | usblp      | 8A288D2FFC |
| 03f0:032a | Hewlett... | LaserJet Professional P1102w | 4     | usblp      | 9BF556CA2F |
| 03f0:092a | Hewlett... | LaserJet Professional P1566  | 4     | usblp      | 396BE2A324 |
| 03f0:0b2a | Hewlett... | LaserJet CP1025nw            | 4     | usblp      | 4DA2F6A4AC |
| 03f0:0d17 | Hewlett... | LaserJet 1012                | 4     | usblp      | 3A1C2004B1 |
| 03f0:3217 | Hewlett... | LaserJet 3050                | 4     | usblp      | 0DCBF3F2DE |
| 03f0:5511 | Hewlett... | DeskJet F300 series          | 4     | usblp      | 0BB045504D |
| 03f0:5c17 | Hewlett... | LaserJet P2055 series        | 4     | usblp      | 79918271CA |
| 03f0:6204 | Hewlett... | DeskJet 5150c                | 4     | usblp      | BA27520038 |
| 03f0:7611 | Hewlett... | DeskJet F2492 All-in-One     | 4     | usblp      | 1D8129C7A2 |
| 03f0:b911 | Hewlett... | Deskjet 2020 series          | 4     | usblp      | E839F7C3F6 |
| 04a9:10c2 | Canon      | PIXMA iP1800 Printer         | 4     | usblp      | AEB1774345 |
| 04a9:176b | Canon      | PIXMA MX920 Series           | 4     | usblp      | 22CB9D3AF2 |
| 04a9:176e | Canon      | PIXMA MG3500 Series          | 4     | usblp      | E75F4538BC |
| 04a9:2660 | Canon      | MF3110                       | 4     | usblp      | 7AA7FB344F |
| 04b8:001b | Seiko E... | L100 Series                  | 4     | usblp      | 0C1E81982D |
| 04e8:323a | Samsung... | ML-1710 Printer              | 4     | usblp      | 0A4959498F |
| 04e8:3256 | Samsung... | ML-1520 Laser Printer        | 4     | usblp      | 4A55C0F238 |
| 04e8:330c | Samsung... | ML-1865                      | 4     | usblp      | 51A7A9C088 |
| 04e8:3426 | Samsung... | SCX-4500 Laser Printer       | 4     | usblp      | BB4BA5DD9F |
| 05ca:042c | Ricoh      | Aficio SP 100SU              | 4     | usblp      | 1D3FD80F42 |
| 03f0:102a | Hewlett... | LaserJet Professional P 1... | 3     | usblp      | E6A8378A5B |
| 03f0:4d11 | Hewlett... | PSC 1400                     | 3     | usblp      | 58E71A74E7 |
| 03f0:6104 | Hewlett... | DeskJet 5650c                | 3     | usblp      | 1905EC200B |
| 03f0:7804 | Hewlett... | DeskJet D1360                | 3     | usblp      | 9BB2C9E5C9 |
| 03f0:a011 | Hewlett... | Deskjet 3050A                | 3     | usblp      | B7499895AB |
| 03f0:b011 | Hewlett... | Deskjet 3520 series          | 3     | usblp      | 112BAE6923 |
| 03f0:c211 | Hewlett... | Deskjet 2540 series          | 3     | usblp      | E412FEBD37 |
| 04a9:10cd | Canon      | iP1900 series                | 3     | usblp      | 441F34B6A5 |
| 04a9:10d5 | Canon      | iP4800 series                | 3     | usblp      | 22731B0DDB |
| 04a9:1714 | Canon      | MP160                        | 3     | usblp      | 82C0242C80 |

### Scanner (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04a9:2220 | Canon      | CanoScan LIDE 25             | 27    |            | B34F330B92 |
| 04a9:1909 | Canon      | CanoScan LiDE 110            | 22    |            | 1871D12AC7 |
| 04a9:220d | Canon      | CanoScan N670U/N676U/LiDE 20 | 18    |            | 8C2BC7A6CD |
| 03f0:0a01 | Hewlett... | ScanJet 2400c                | 17    | usbfs      | 24EA81BF4C |
| 04a9:190a | Canon      | CanoScan LiDE 210            | 14    |            | B41472725B |
| 05d8:4002 | Ultima ... | Artec Ultima 2000 (GT6801... | 14    |            | 9B11B923A2 |
| 04b8:0121 | Seiko E... | GT-F500/GT-F550 [Perfecti... | 13    |            | FF3AABBF0B |
| 04b8:0142 | Seiko E... | GT-F730 [GT-S630/Perfecti... | 13    |            | 1CC81789AC |
| 04a9:190e | Canon      | CanoScan LiDE 120            | 10    | usbfs      | 3A1BF1D002 |
| 04a9:221c | Canon      | CanoScan LiDE 60             | 9     |            | CA59692108 |
| 04b8:0120 | Seiko E... | GT-7400U [Perfection 1270]   | 9     |            | 645B5E5FF4 |
| 04b8:012d | Seiko E... | GT-F650 [GT-S600/Perfecti... | 9     |            | B0B570F44B |
| 04a9:190f | Canon      | CanoScan LiDE 220            | 8     |            | 649FF143AD |
| 04a9:1904 | Canon      | CanoScan LiDE 100            | 6     |            | 44001EFF10 |
| 04b8:011f | Seiko E... | GT-8400UF [Perfection 167... | 6     |            | 3E4401FECF |
| 04b8:0122 | Seiko E... | GT-F520/GT-F570 [Perfecti... | 6     |            | 8712B6DE43 |
| 055f:021b | Mustek ... | BearPaw 1200 CU Plus         | 6     |            | AEEF8EB693 |
| 055f:021d | Mustek ... | BearPaw 2400 CU Plus         | 6     |            | EE0CF0EF40 |
| 04a9:2213 | Canon      | CanoScan LiDE 50/LiDE 35/... | 5     |            | 0D8839C5FC |
| 04b8:010f | Seiko E... | GT-7200U [Perfection 1250... | 5     |            | 936BEEBE68 |
| 04b8:012e | Seiko E... | GT-F670 [Perfection V200 ... | 5     |            | D8D0364599 |
| 04b8:013a | Seiko E... | GT-X820 [Perfection V600 ... | 5     |            | 461345008C |
| 04a5:2311 | Acer Pe... | Benq 5560                    | 4     |            | 25B7198C11 |
| 04a9:220e | Canon      | CanoScan N1240U/LiDE 30      | 4     |            | 91544C4D3A |
| 04b8:0114 | Seiko E... | Perfection 660               | 4     |            | A26B3126F3 |
| 055f:021a | Mustek ... | BearPaw 2448 TA Plus         | 4     |            | 6BB9573F31 |
| 055f:021f | Mustek ... | SNAPSCAN e22                 | 4     |            | 07BA810409 |
| 03f0:1705 | Hewlett... | ScanJet 5590                 | 3     |            | 297E534CF0 |
| 03f0:1d05 | Hewlett... | Scanjet 300                  | 3     |            | DEAB0E9DCE |
| 03f0:2305 | Hewlett... | ScanJet 3970c                | 3     |            | E3ADCE0E5E |
| 03f0:2605 | Hewlett... | ScanJet 3800c                | 3     |            | 8B640C33D5 |
| 04a9:1900 | Canon      | CanoScan LiDE 90             | 3     |            | 82126F997F |
| 04a9:1907 | Canon      | CanoScan LiDE 700F           | 3     | usbfs      | 9BF556CA2F |
| 04a9:2225 | Canon      | CanoScan LiDE 70             | 3     |            | 1EA13B6B1B |
| 04b8:0119 | Seiko E... | GT-X750 [Perfection 4490 ... | 3     |            | 5324E8F5B3 |
| 04b8:012a | Seiko E... | GT-X800 [Perfection 4990 ... | 3     |            | 707B18AA4C |
| 04b8:013b | Seiko E... | Scanner                      | 3     |            | DD8AABC952 |
| 055f:0408 | Mustek ... | BearPaw 2448 CU Pro          | 3     |            | DD43D2EAFE |
| 03f0:0205 | Hewlett... | ScanJet 3300c                | 2     |            | 435E64F300 |
| 03f0:0601 | Hewlett... | ScanJet 6300c                | 2     |            | 2047255023 |
| 03f0:0605 | Hewlett... | ScanJet 2200c                | 2     |            | 0E4B7C3629 |
| 03f0:0805 | Hewlett... | HP4470C                      | 2     |            | A014C216F3 |
| 03f0:1405 | Hewlett... | ScanJet 3670                 | 2     |            | 783D86F34C |
| 03f0:2205 | Hewlett... | ScanJet 3500c                | 2     |            | 126B5ADE99 |
| 03f0:2f11 | Hewlett... | PSC 1200                     | 2     | usblp      | FEE0937E4F |
| 03f0:4205 | Hewlett... | ScanJet G3010                | 2     |            | A9E90B12DC |
| 03f0:4305 | Hewlett... | ScanJet G3110                | 2     |            | 3F1B997D89 |
| 03f0:4605 | Hewlett... | ScanJet G4050                | 2     |            | 665166F420 |
| 04a5:20b0 | Acer Pe... | S2W 3300U/4300U              | 2     |            | FDA7FBDBEF |
| 04a5:20f8 | Acer Pe... | Benq 5000                    | 2     |            | 7952379ACC |
| 04a5:2211 | Acer Pe... | Color FlatbedScanner39       | 2     |            | E507592DE8 |
| 04a5:2331 | Acer Pe... | FlatbedScanner 50            | 2     |            | 0B356FFA8A |
| 04a9:1908 | Canon      | CanoScan                     | 2     | usbfs      | 75193022D0 |
| 04a9:221b | Canon      | CanoScan 4200F               | 2     |            | AFE7557041 |
| 04a9:2224 | Canon      | CanoScan LiDE 600F           | 2     |            | D326506C35 |
| 04a9:2228 | Canon      | CanoScan 4400F               | 2     |            | B8BA2574F5 |
| 04b8:012f | Seiko E... | GT-F700 [Perfection V350]    | 2     |            | F6CDB72510 |
| 04b8:0131 | Seiko E... | GT-F720 [GT-S620/Perfecti... | 2     |            | 99582A958F |
| 055f:0219 | Mustek ... | BearPaw 2400 TA Plus         | 2     |            | 0050E800F4 |
| 055f:021c | Mustek ... | BearPaw 1200 CU Plus         | 2     |            | D40A8E666A |
| 01ef:0004 | Hewlett... | Scanjet                      | 1     |            | 3EB97E6189 |
| 03f0:0305 | Hewlett... | ScanJet 4300c                | 1     | usbfs      | 6BAA62B5E0 |
| 03f0:0405 | Hewlett... | ScanJet 3400cse              | 1     | usbfs      | 6B86B21C18 |
| 03f0:0705 | Hewlett... | ScanJet 4400c                | 1     |            | 1789404C97 |
| 03f0:0b01 | Hewlett... | ScanJet 82x0C                | 1     |            | 2265248DBB |
| 03f0:1205 | Hewlett... | ScanJet 4500C/5550C          | 1     |            | B7C0950BF9 |
| 03f0:1b05 | Hewlett... | ScanJet 4850C/4890C          | 1     | usbfs      | E4F578F490 |
| 03f0:1c05 | Hewlett... | Scanjet 200                  | 1     |            | C87482DA9E |
| 03f0:2005 | Hewlett... | ScanJet 3570c                | 1     |            | FE2E4341C3 |
| 03f0:2505 | Hewlett... | ScanJet 3770                 | 1     |            | 33F8716B48 |
| 03f0:2805 | Hewlett... | ScanJet G2710                | 1     |            | 4B3FF97AD5 |
| 03f0:2811 | Hewlett... | PSC-2100                     | 1     | usbfs      | 390C380926 |
| 03f0:2a05 | Hewlett... | Scanjet N6010                | 1     |            | F855FA062D |
| 03f0:4505 | Hewlett... | ScanJet G4010                | 1     |            | 92D4F82EF9 |
| 0458:2013 | KYE Sys... | ColorPage-HR7 Scanner        | 1     |            | 6A6DA1FE6F |
| 0458:2014 | KYE Sys... | ColorPage-Vivid4             | 1     |            | F89AC2519F |
| 0458:201a | KYE Sys... | ColorPage-Vivid4xe           | 1     |            | 7731F20599 |
| 0458:201f | KYE Sys... | ColorPage-Vivid 1200 XE      | 1     |            | 910131EE7C |
| 04a5:211b | Acer Pe... | FlatbedScanner 22            | 1     |            | 0A23FD59F0 |
| 04a5:2137 | Acer Pe... | Benq 5150/5250               | 1     |            | CA671B64DE |
| 04a9:1905 | Canon      | CanoScan LiDE 200            | 1     |            | 3D5D3F552C |
| 04a9:190d | Canon      | CanoScan 9000F Mark II       | 1     |            | C268560077 |
| 04a9:2204 | Canon      | CanoScan FB630U              | 1     |            | 700FBE7F0A |
| 04a9:221f | Canon      | CanoScan LiDE 500F           | 1     |            | 68FA7AD805 |
| 04b8:0109 | Seiko E... | ES-8500 [Expression 1640 XL] | 1     |            | 52B7E8AF1A |
| 04b8:010b | Seiko E... | GT-7700U [Perfection 1240U]  | 1     |            | F83C6E1394 |
| 04b8:0118 | Seiko E... | GT-F600 [Perfection 4180]    | 1     | usbfs      | 5E6A0B8AE4 |
| 04b8:011b | Seiko E... | GT-9300UF [Perfection 240... | 1     |            | A3F682ED16 |
| 04b8:011c | Seiko E... | GT-9800F [Perfection 3200]   | 1     |            | FA5CA5ADA3 |
| 04b8:011d | Seiko E... | GT-7300U [Perfection 1260... | 1     |            | F7C50F2E53 |
| 04b8:011e | Seiko E... | GT-8300UF [Perfection 166... | 1     |            | F702714A09 |
| 04b8:0807 | Seiko E... | Stylus Photo RX500/510       | 1     | usblp      | E8737C8AC8 |
| 04c5:128e | Fujitsu    | ScanSnap SV600               | 1     | usbfs      | 4D480C550E |
| 055f:0001 | Mustek ... | ScanExpress 1200 CU          | 1     |            | D40E62887C |
| 055f:0006 | Mustek ... | ScanExpress 1200 UB          | 1     |            | 0FA01E4D66 |
| 055f:0007 | Mustek ... | ScanExpress 1200 USB Plus    | 1     |            | F0136C5A6F |
| 055f:0008 | Mustek ... | ScanExpress 1200 CU Plus     | 1     |            | F4710F7B29 |
| 055f:040b | Mustek ... | ScanExpress A3 USB 1200 PRO  | 1     |            | F87D1B2A7A |
| 05d8:4009 | Ultima ... | Umax Astraslim               | 1     |            | A9BE29F98B |
| 05da:30d4 | Microte... | USB1200 Scanner              | 1     |            | 5AD44BAA44 |

### Serial controller (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 067b:2303 | Prolifi... | PL2303 Serial Port           | 30    | pl2303     | 87DAB1466B |
| 1a86:7523 | QinHeng... | HL-340 USB-Serial adapter    | 27    | ch341      | 99B2ABEC05 |
| 0403:6001 | Future ... | FT232 USB-Serial (UART) IC   | 26    | ftdi_sio   | 046006226D |
| 19d2:0016 | ZTE WCD... | ZTE WCDMA Technologies MSM   | 18    | option     | A44CE70FF7 |
| 10c4:ea60 | Cygnal ... | CP2102/CP2109 UART Bridge... | 15    | cp210x     | 3D0DACB49E |
| 0d9f:0002 | Powercom   | Black Knight PRO / WOW Un... | 8     | cypress... | D4B6A04B2C |
| 0403:6010 | Future ... | FT2232C/D/H Dual USB-UART... | 5     | ftdi_sio   | 31EC690BE3 |
| 19d2:2003 | ZTE WCD... | ZTE WCDMA Technologies MSM   | 5     | option     | 36FCA7E911 |
| 04b4:5500 | Cypress... | HID->COM RS232 Adapter       | 4     | cypress... | E0F7798547 |
| 0fcf:1009 | Dynastr... | ANTUSB-m Stick               | 4     | usb_ser... | E45C3CB530 |
| 0403:1234 | Future ... | IronLogic RFID Adapter [Z... | 3     | ftdi_sio   | 876DB8C948 |
| 0451:3410 | Texas I... | TUSB3410 Microcontroller     | 3     | ti_usb_... | 02795B4E4A |
| 1b1c:1c00 | Corsair    | Controller for Corsair Link  | 3     | cp210x     | 24D4B49329 |
| 05c6:9008 | Qualcomm   | Gobi Wireless Modem (QDL ... | 2     | qcserial   | 5C0FC04230 |
| 03f0:1016 | Hewlett... | Jornada 548 / iPAQ HW6515... | 1     | ipaq       | 4FA0B1AA66 |
| 0557:2008 | ATEN In... | UC-232A Serial Port [pl2303] | 1     | pl2303     | 0F9AA13B55 |
| 057c:6201 | AVM        | AVM Fritz!WLAN v1.1 [Texa... | 1     | option     | 96A1EAB120 |
| 0711:0230 | Magic C... | MCT-232 Serial Port          | 1     | mct_u232   | D4B46091B4 |
| 0856:ac19 | B&B Ele... | Model USOPTL4DR              | 1     | ftdi_sio   | 077842DAC4 |
| 0bf8:1001 | Fujitsu... | Fujitsu Pocket Loox 600 PDA  | 1     | ipaq       | 224FD9231B |
| 114f:68a2 | Wavecom    | MC7750                       | 1     | qcserial   | 077842DAC4 |
| 12d1:15c1 | Huawei ... | ME906s LTE M.2 Module        | 1     | option     | 8F87769D12 |
| 1555:0004 | Silicon... | AC4 USB to RS-485 Converter  | 1     | cp210x     | 1ED36B8028 |
| 19d2:0117 | ZTE WCD... | WCDMA Technologies MSM       | 1     | uas, us... | 3BEF273525 |
| 1bbb:022c | T & A M... | HSPA+ USB Modem              | 1     | option     | B5491ACA4A |
| 2341:4660 | Arduino SA | USB-RS485                    | 1     | usbseri... | 58340DB405 |
| 6547:0232 | Arkmicr... | ARK3116 Serial               | 1     | ark3116    | 0FA01E4D66 |
| 9710:7840 | MosChip... | MCS7820/MCS7840 2/4 port ... | 1     | mos7840    | A4BB27D2B7 |

### Sound (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0d8c:013c | C-Media... | CM108 Audio Controller       | 67    | snd_usb... | 87DAB1466B |
| 0d8c:000c | C-Media... | Audio Adapter                | 29    | snd_usb... | 1B78CC518F |
| 0d8c:0201 | C-Media... | CM6501                       | 26    | snd_usb... | C92DC5D0CF |
| 1b3f:2008 | General... | USB Audio Device             | 23    | snd_usb... | 21C6ED28C9 |
| 1130:1620 | Tenx Te... | USB AUDIO                    | 22    | snd_usb... | D6D2A25AA4 |
| 0d8c:0014 | C-Media... | Audio Adapter (Unitek Y-2... | 20    | snd_usb... | EDE121860B |
| 08bb:2902 | Texas I... | PCM2902 Audio Codec          | 18    | snd_usb... | EB0CDD472F |
| 041e:30df | Creativ... | SB X-Fi Surround 5.1 Pro     | 14    | snd_usb... | AE7C45607A |
| 0d8c:0012 | C-Media... | USB Audio Device             | 14    | snd_usb... | 89119460C8 |
| 0d8c:0102 | C-Media... | CM106 Like Sound Device      | 14    | snd_usb... | B86DF74030 |
| 0d8c:0103 | C-Media... | CM102-A+/102S+ Audio Cont... | 13    | snd_usb... | CC9A8D1703 |
| 046d:0a44 | Logitech   | Wired headset H390           | 12    | snd_usb... | 6BDDC45E74 |
| 8086:0808 | Intel      | USB PnP Sound Device         | 12    | snd_usb... | EDC325267B |
| 045e:070f | Microsoft  | LifeChat LX-3000 Headset     | 11    | snd_usb... | B3B453B4DC |
| 046d:0a1f | Logitech   | G930                         | 11    | snd_usb... | 518A8709C7 |
| 093a:2625 | Pixart ... | Multimedia audio controller  | 11    | gspca_p... | 32D6CC8A86 |
| 046d:0a29 | Logitech   | H600 [Wireless Headset]      | 10    | snd_usb... | E1610DFA98 |
| 0556:0001 | Asahi K... | AK5370 I/F A/D Converter     | 9     | snd_usb... | B45F44A0F7 |
| 08bb:2904 | Texas I... | PCM2904 Audio Codec          | 8     | snd_usb... | 046006226D |
| 24ae:6000 | Rapoo      | Wireless Audio               | 8     | snd_usb... | 5AEB4ABFE6 |
| 041e:30d3 | Creativ... | Sound Blaster Play!          | 7     | snd_usb... | 1B53F903AD |
| 046d:0a37 | Logitech   | USB Headset H540             | 7     | snd_usb... | E661404057 |
| 046d:0a4d | Logitech   | G430 Surround Sound Gamin... | 7     | snd_usb... | BF6F02A425 |
| 08bb:2704 | Texas I... | PCM2704 16-bit stereo aud... | 7     | snd_usb... | 50919B2DDB |
| 09da:2701 | A4Tech     | Bloody Gaming Audio Device   | 7     | snd_usb... | C572988845 |
| 0c76:160c | JMTek      | USB Speaker                  | 7     | snd_usb... | FD5E41554C |
| 0d8c:0126 | C-Media... | USB Audio Device             | 7     | snd_usb... | 740FC92339 |
| 041e:3040 | Creativ... | SoundBlaster Live! 24-bit... | 6     | snd_usb... | CBC1CB5648 |
| 046d:0a0c | Logitech   | Clear Chat Comfort USB He... | 6     | snd_usb... | 2FAE8819F7 |
| 1532:0504 | Razer USA  | Razer Kraken 7.1 Chroma      | 6     | snd_usb... | F5EAFE71C9 |
| b58e:9e84 | Blue Mi... | Yeti Stereo Microphone       | 6     | snd_usb... | 8B7204FCBA |
| 046d:0a45 | Logitech   | USB Headset                  | 5     | snd_usb... | 0438613602 |
| 046d:0a5b | Logitech   | G933 Wireless Headset Dongle | 5     | snd_usb... | CC9738C393 |
| 047f:c010 | Plantro... | GameCom 780                  | 5     | snd_usb... | F1C3819116 |
| 047f:c012 | Plantro... | .Audio 628 USB               | 5     | snd_usb... | 48CAEFF57F |
| 0c76:161f | JMTek      | USB PnP Audio Device         | 5     | snd_usb... | 62B0B05A66 |
| 0d8c:0001 | C-Media... | Audio Device                 | 5     | snd_usb... | 97BEC56798 |
| 0d8c:0005 | C-Media... | Blue Snowball                | 5     | snd_usb... | 09CC6BAB56 |
| 0d8c:0008 | C-Media... | USB Audio Device             | 5     | snd_usb... | A4FB239111 |
| 1532:000e | Razer USA  | Razer Megalodon              | 5     | snd_usb... | E55D921BE1 |
| 18c3:6255 | Elite S... | USB Audio Device             | 5     | snd_usb... | BBF84F3432 |
| 413c:a503 | Dell       | AC511 USB SoundBar           | 5     | snd_usb... | 7E26196CB5 |
| 041e:3232 | Creativ... | Sound Blaster Premium HD ... | 4     | snd_usb... | 72FFD05528 |
| 046d:0a01 | Logitech   | USB Headset                  | 4     | snd_usb... | C1C5F4E6DB |
| 046d:0a38 | Logitech   | Headset H340                 | 4     | snd_usb... | 6966FF65AA |
| 0c76:1617 | JMTek      | USB PnP Audio Device         | 4     | snd_usb... | 66C270EC8B |
| 0d8c:0139 | C-Media... | Multimedia Headset [Gigaw... | 4     | snd_usb... | D1E49BE03D |
| 6993:b700 | Yealink... | VOIP USB Phone               | 4     | snd_usb... | 60738639D1 |
| 041e:30d7 | Creativ... | USB Sound Blaster HD         | 3     | snd_usb... | 3108FE53D3 |
| 046d:08c1 | Logitech   | QuickCam Fusion              | 3     | snd_usb... | 004C278780 |
| 046d:0a10 | Logitech   | Speaker                      | 3     | snd_usb... | 195649904F |
| 046d:0a14 | Logitech   | USB Headset                  | 3     | snd_usb... | 0BB045504D |
| 046d:0a15 | Logitech   | G35 Headset                  | 3     | snd_usb... | A85443A8BB |
| 046d:0a66 | Logitech   | [G533 Wireless Headset Do... | 3     | snd_usb... | F733910E90 |
| 047f:d955 | Plantro... | Wireless Audio               | 3     | snd_usb... | A58865F4C4 |
| 054c:09cc | Sony       | DualShock 4 [CUH-ZCT2x]      | 3     | snd_usb... | ADA2BD30FA |
| 05fc:0231 | Harman     | JBL Pebbles                  | 3     | snd_usb... | 838EE46987 |
| 06f8:3009 | Guillemot  | Multimedia audio controller  | 3     | gspca_p... | 9E9C06CB6F |
| 093a:2629 | Pixart ... | Multimedia audio controller  | 3     | gspca_p... | 72FDE83F4C |
| 0951:16a4 | Kingsto... | HyperX 7.1 Audio             | 3     | snd_usb... | 1F7F86ED9E |
| 0c76:1607 | JMTek      | audio controller             | 3     | snd_usb... | 44F4A86DE1 |
| 0d8c:013a | C-Media... | USB PnP Sound Device         | 3     | snd_usb... | 32F9C98FE6 |
| 12ba:0034 | License... | Wireless Stereo Headset      | 3     | snd_usb... | E9220838E0 |
| 1395:0025 | Sennhei... | Headset [PC 8]               | 3     | snd_usb... | 86E09EF939 |
| 1532:0a02 | Razer USA  | Razer ManO'War               | 3     | snd_usb... | 1625938FF3 |
| 1a1d:8301 | Veho       | 2.4G Wireless Headset        | 3     | snd_usb... | 76B92234CA |
| 1a86:752d | QinHeng... | CH345 MIDI adapter           | 3     | snd_usb... | 9968239DA3 |
| 0416:1021 | Winbond... | USB AUDIO Mouse              | 2     | snd_usb... | A860F07046 |
| 041e:3010 | Creativ... | SoundBlaster MP3+            | 2     | snd_usb... | C59EB70BAF |
| 041e:3042 | Creativ... | SB X-Fi Surround 5.1         | 2     | snd_usb... | F32755062C |
| 041e:30dd | Creativ... | Sound Blaster X-Fi Go! Pro   | 2     | snd_usb... | D51FB90D46 |
| 041e:324d | Creativ... | Sound Blaster Play! 3        | 2     | snd_usb... | E43241E488 |
| 046d:0a12 | Logitech   | Wireless Headset             | 2     | snd_usb... | B45F44A0F7 |
| 046d:0a13 | Logitech   | Z-5 Speakers                 | 2     | snd_usb... | A450827948 |
| 046d:0a17 | Logitech   | G330 Headset                 | 2     | snd_usb... | 5FEAFD37C9 |
| 0471:0151 | Philips... | USB Audio System             | 2     | snd_usb... | C2D1F5C35A |
| 0471:2118 | Philips... | SHG7980                      | 2     | snd_usb... | 5441EE6FB7 |
| 0471:2119 | Philips... | SHG8200                      | 2     | snd_usb... | 87086AEB40 |
| 047f:aa09 | Plantro... | DA40                         | 2     | snd_usb... | F5F54893BC |
| 047f:ad01 | Plantro... | GameCom 777 5.1 Headset      | 2     | snd_usb... | 15BF44F112 |
| 047f:c008 | Plantro... | Audio 655 DSP                | 2     | snd_usb... | 8838C40E2F |
| 047f:c01e | Plantro... | C310-M                       | 2     | snd_usb... | 481A2508BF |
| 047f:c021 | Plantro... | RIG                          | 2     | snd_usb... | E96981E395 |
| 0572:14c3 | Conexan... | USB Dongle                   | 2     | snd_usb... | F0CE444FB7 |
| 05a7:1020 | Bose       | USB Audio                    | 2     | snd_usb... | 92F011CFCF |
| 06f8:b101 | Guillemot  | Hercules DJ Console RMX      | 2     | snd_usb... | 1F8CDA3921 |
| 0763:2024 | M-Audio    | M-Audio Fast Track MKII      | 2     | snd_usb... | 65CF4F7F82 |
| 08bb:29b3 | Texas I... | PCM2903B Audio CODEC         | 2     | snd_usb... | DDDDDCBF45 |
| 08bb:29c0 | Texas I... | PCM2900C Audio CODEC         | 2     | snd_usb... | 25810F9DC3 |
| 08bb:29c2 | Texas I... | PCM2902C Audio CODEC         | 2     | snd_usb... | E903B65B9C |
| 0a92:00d1 | EGO SYS... | U24XL                        | 2     | snd_usb... | 298B7A909C |
| 0a92:2041 | EGO SYS... | UGM96                        | 2     | snd_usb... | 3C64BA1E95 |
| 0b0e:0306 | GN Netcom  | Jabra EVOLVE LINK            | 2     | snd_usb... | A0518E949A |
| 0d8c:0006 | C-Media... | Storm HP-USB500 5.1 Headset  | 2     | snd_usb... | 10ABE11AE2 |
| 0d8c:0013 | C-Media... | USB Audio Device             | 2     | snd_usb... | 3DB9496E05 |
| 0d8c:0104 | C-Media... | CM103+ Audio Controller      | 2     | snd_usb... | AF4A74E541 |
| 1210:000a | DigiTech   | Lexicon Alpha                | 2     | snd_usb... | 26F6E631A8 |
| 1397:0301 | BEHRING... | C-1U                         | 2     | snd_usb... | 0B374BC520 |
| 1ae7:2001 | X-TENSIONS | SpeedLink Snappy Mic webc... | 2     | gspca_p... | 9BBC79D61E |
| 1b1c:0a06 | Corsair    | Raptor HS40                  | 2     | snd_usb... | CE684B56C3 |

### Touchpad (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 06cb:0009 | Synaptics  | Composite TouchPad and Tr... | 2     | synapti... | A9E837C9EB |
| 062a:19b5 | MosArt ... | TouchPad                     | 1     | usbhid     | 61BB547684 |

### Touchscreen (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0408:3008 | Quanta ... | OpticalTouchScreen           | 2     | usbhid     | C8D5D01ACA |
| 04e7:0020 | Elo Tou... | Touchscreen Interface (2700) | 2     | usbhid     | 8807184E95 |
| 0eef:0001 | D-WAV S... | eGalax TouchScreen           | 2     | usbtouc... | F27B875C33 |
| 1926:0003 | NextWindow | 1900 HID Touchscreen         | 2     | usbhid     | EF835695B4 |
| 1926:0006 | NextWindow | 1950 HID Touchscreen         | 2     | usbhid     | 62E3C9247C |
| 1926:0344 | NextWindow | Touchscreen                  | 2     | usbhid     | CF7972F23A |
| 1926:0dbe | NextWindow | Touchscreen                  | 2     | usbhid     | BE8250E028 |
| 1926:0dc6 | NextWindow | Touchscreen                  | 2     | usbhid     | 022809A636 |
| 0df9:0001 | MASTouc... | MASTouch USB Touchscreen     | 1     | usbhid     | 0453BA1824 |
| 1926:0001 | NextWindow | Touchscreen                  | 1     | usbhid     | 56053B0099 |
| 1926:0065 | NextWindow | 1950 HID Touchscreen         | 1     | usbhid     | 26B75090A9 |
| 1926:0093 | NextWindow | Touchscreen                  | 1     | usbhid     | 1BA5C50EAD |
| 1926:033c | NextWindow | Touchscreen                  | 1     | usbhid     | 90AD74D06E |
| 1926:0dbd | NextWindow | Touchscreen                  | 1     | nw_fermi   | B5914A50D6 |
| 1926:0e21 | NextWindow | Touchscreen                  | 1     | usbhid     | 14F9CFEBB9 |
| 25aa:8842 | TPV        | OpticalTouchScreen           | 1     | usbhid     | 0DCBF3F2DE |

### Tv card (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 093a:2620 | Pixart ... | TV Card                      | 29    | gspca_p... | F9823E634A |
| 0ac8:303b | Z-Star ... | ZC0303 Webcam                | 24    | gspca_z... | C542826775 |
| 0ac8:305b | Z-Star ... | ZC0305 Webcam                | 20    | gspca_z... | 276FD0BC49 |
| 045e:00f7 | Microsoft  | LifeCam VX-1000              | 17    | gspca_s... | EE184BFE51 |
| 093a:2622 | Pixart ... | Webcam Genius                | 15    | gspca_p... | BF689A3BCB |
| 046d:08d7 | Logitech   | QuickCam Communicate STX     | 11    | gspca_z... | 426AE68B93 |
| 046d:08da | Logitech   | QuickCam Messanger           | 11    | gspca_z... | 03A250A2E9 |
| 093a:2624 | Pixart ... | Webcam                       | 11    | gspca_p... | BDB727808F |
| 046d:089d | Logitech   | QuickCam E2500 series        | 8     | gspca_z... | 6B8676DFEF |
| 045e:00f5 | Microsoft  | LifeCam VX-3000              | 7     | gspca_s... | 0F38E59FD3 |
| 046d:08d9 | Logitech   | QuickCam IM/Connect          | 7     | gspca_z... | 8CDD8FFE23 |
| 046d:092f | Logitech   | QuickCam Express Plus        | 7     | gspca_s... | B9F2AE0CCF |
| 093a:2476 | Pixart ... | CIF Single Chip              | 7     | gspca_p... | D73E7CDAF7 |
| 093a:2626 | Pixart ... | TV Card                      | 7     | gspca_p... | D3A3F71EF7 |
| 093a:262c | Pixart ... | TV Card                      | 7     | gspca_p... | 9FB570E08A |
| 046d:08af | Logitech   | QuickCam Easy/Cool           | 6     | gspca_z... | BB5E8345C0 |
| 093a:2468 | Pixart ... | SoC PC-Camera                | 6     | gspca_p... | B61D174C21 |
| 1415:2000 | Nam Tai... | Sony Playstation Eye         | 6     | gspca_o... | D082929A57 |
| 041e:4052 | Creativ... | Live! Cam Vista IM           | 4     | gspca_o... | EBED6181EA |
| 041e:4064 | Creativ... | VF0420 Live! Cam Vista IM    | 4     | gspca_o... | C9CCBD7C3F |
| 046d:08a2 | Logitech   | Labtec Webcam Pro            | 4     | gspca_z... | 51837DE98F |
| 046d:08ad | Logitech   | QuickCam Communicate STX     | 4     | gspca_z... | 98746816D9 |
| 0ac8:0328 | Z-Star ... | A4Tech PK-130MG              | 4     | gspca_v... | 56944B383F |
| 0ac8:301b | Z-Star ... | ZC0301 Webcam                | 4     | gspca_z... | EF1765E325 |
| 0c45:608f | Microdia   | PC Camera (SN9C103 + OV7630) | 4     | gspca_s... | FEFB26C581 |
| 045e:00f4 | Microsoft  | LifeCam VX-6000 (SN9C20x ... | 3     | gspca_s... | EDFDFB06D3 |
| 093a:2472 | Pixart ... | CIF Single Chip              | 3     | gspca_p... | C1255CDB72 |
| 093a:2621 | Pixart ... | PAC731x Trust Webcam         | 3     | gspca_p... | C07EF20B6E |
| 0ac8:307b | Z-Star ... | USB 1.1 Webcam               | 3     | gspca_z... | 15968F5811 |
| 0c45:6128 | Microdia   | PC Camera (SN9C325 + OM6802) | 3     | gspca_s... | 3AAC9757B6 |
| 2040:7200 | Hauppauge  | WinTV HVR-950                | 3     | au0828     | AE5B68C4AD |
| 0402:5602 | ALi        | M5602 Video Camera Contro... | 2     | gspca_m... | 318644CCEF |
| 041e:405f | Creativ... | WebCam Vista (VF0330)        | 2     | gspca_o... | B3C7478840 |
| 046d:08a9 | Logitech   | Notebook Deluxe              | 2     | gspca_z... | 568DE70E4A |
| 0471:0329 | Philips... | SPC 900NC PC Camera / ORI... | 2     | pwc, sn... | 50792DF026 |
| 093a:2460 | Pixart ... | Q-TEC WEBCAM 100             | 2     | gspca_p... | FC4EBA57CC |
| 093a:2608 | Pixart ... | PAC7311 Trust WB-3300p       | 2     | gspca_p... | 2D16479F4B |
| 093a:260e | Pixart ... | PAC7311 Gigaware VGA PC C... | 2     | gspca_p... | 699063D316 |
| 0ac8:0302 | Z-Star ... | ZC0302 Webcam                | 2     | gspca_z... | ABFAB43B36 |
| 0c45:600d | Microdia   | TwinkleCam USB camera        | 2     | gspca_s... | 6A798F999C |
| 0c45:6028 | Microdia   | Typhoon Easycam USB 330K ... | 2     | gspca_s... | AEAB86C587 |
| 0c45:60b0 | Microdia   | Genius VideoCam Look         | 2     | gspca_s... | 8C929530B7 |
| 0c45:6270 | Microdia   | PC Camera (SN9C201 + MI03... | 2     | gspca_s... | E969E52D33 |
| 041e:401c | Creativ... | Webcam NX [PD1110]           | 1     | gspca_z... | 5305CEA18B |
| 041e:4061 | Creativ... | Live! Cam Notebook Pro [V... | 1     | gspca_o... | 4E187292DA |
| 0458:7029 | KYE Sys... | Genius Look 320s (SN9C201... | 1     | gspca_s... | CE3CF2888A |
| 046d:08ae | Logitech   | QuickCam for Notebooks       | 1     | snd_usb... | E747D8C526 |
| 046d:08f0 | Logitech   | QuickCam Messenger           | 1     | gspca_s... | AB7B26B325 |
| 046d:08f6 | Logitech   | QuickCam Messenger Plus      | 1     | gspca_s... | 7B106B9427 |
| 046d:0928 | Logitech   | QuickCam Express             | 1     | gspca_s... | D78C1D6096 |
| 046d:0929 | Logitech   | Labtec Webcam Pro            | 1     | gspca_s... | 19C101AB87 |
| 046d:092e | Logitech   | QuickCam Chat                | 1     | gspca_s... | 2C10191944 |
| 0471:032d | Philips... | SPC 210NC PC Camera          | 1     | gspca_z... | 2441D6D1D8 |
| 0545:8333 | Xirlink    | Veo Stingray/Connect Web ... | 1     | gspca_t... | B9F90CB8E0 |
| 054c:0154 | Sony       | Eyetoy Audio Device          | 1     | gspca_o... | 4BA24556E4 |
| 06f8:3008 | Guillemot  | USB camera                   | 1     | gspca_s... | 6E4D6B9ECA |
| 0733:0401 | ViewQue... | CS330 Webcam                 | 1     | gspca_s... | C6F1D561A0 |
| 093a:2463 | Pixart ... | CIF Single Chip              | 1     | gspca_p... | 1DE59A68CD |
| 093a:2470 | Pixart ... | SoC PC-Camera                | 1     | gspca_p... | 377F2465DE |
| 093a:2600 | Pixart ... | Typhoon Easycam USB 330K ... | 1     | gspca_p... | 89B7926B1C |
| 0c45:6007 | Microdia   | VideoCAM Eye                 | 1     | gspca_s... | 90EA766D86 |
| 0c45:6029 | Microdia   | Triplex i-mini PC Camera     | 1     | gspca_s... | 4B8FD34544 |
| 0c45:602b | Microdia   | VideoCAM NB 300              | 1     | sn9c102    | DF2321CD21 |
| 0c45:602c | Microdia   | Clas Ohlson TWC-30XOP Webcam | 1     | gspca_s... | C542826775 |
| 0c45:602e | Microdia   | VideoCAM Messenger           | 1     | gspca_s... | 2022755796 |
| 0c45:6100 | Microdia   | USB camera                   | 1     | gspca_s... | 0AB0ACEA30 |
| 0c45:613a | Microdia   | PC Camera (SN9C120)          | 1     | gspca_s... | CFCEDC1F4F |
| 0c45:613c | Microdia   | PC Camera (SN9C120)          | 1     | gspca_s... | 619D8CC7E3 |
| 0c45:613e | Microdia   | PC Camera (SN9C120)          | 1     | gspca_s... | 64E722CEC4 |
| 0c45:6242 | Microdia   | PC Camera (SN9C201 + MI1310) | 1     | gspca_s... | 2CAE195313 |
| 0c45:624e | Microdia   | PC Camera (SN9C201 + SOI968) | 1     | gspca_s... | B60039A681 |
| 0ccd:0096 | TerraTe... | Grabby                       | 1     | em28xx     | F298AE57F6 |
| 1164:0622 | YUAN Hi... | USB GOTVIEW DVD2             | 1     | pvrusb2    | C0CF078B6C |
| 2040:4902 | Hauppauge  | HD PVR                       | 1     | hdpvr      | 01928383AA |
| 2040:7501 | Hauppauge  | WinTV                        | 1     | pvrusb2    | AE5B68C4AD |
| eb1a:2861 | eMPIA T... | TV Card                      | 1     | em28xx     | F6F1F3D526 |
| eb1a:2881 | eMPIA T... | EM2881 Video Controller      | 1     | em28xx     | E22CC7C6C8 |

### Ups (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 051d:0002 | America... | Back-UPS Pro 500/1000/1500   | 126   | usbhid     | 19A4B45331 |
| 0764:0501 | Cyber P... | CP1500 AVR UPS               | 34    | usbhid     | 239A2E1678 |
| 0d9f:0004 | Powercom   | HID UPS Battery              | 10    | usbhid     | EF09CB18CC |
| 06da:0003 | Phoenix... | 1300VA UPS                   | 6     | usbhid     | 9B671B15DD |
| 0d9f:00a2 | Powercom   | Imperial Uninterruptible ... | 5     | usbhid     | AEEDCA54FC |
| 0463:ffff | MGE UPS... | UPS                          | 4     | usbfs      | 8F56CB529C |
| 0925:1234 | Lakevie... | UPS USB MON V1.4             | 4     | usbhid     | A12C16610A |
| 0d9f:00a3 | Powercom   | Smart King PRO Uninterrup... | 3     | usbhid     | F4AF5BD9C9 |
| 0d9f:00a4 | Powercom   | WOW Uninterruptible Power... | 2     | usbhid     | D06D669535 |
| 050d:0751 | Belkin ... | Belkin UPS                   | 1     | usbhid     | 71F7F3AD8A |
| 051d:0003 | America... | UPS                          | 1     | usbhid     | 4213886CA6 |
| 0592:0002 | Powerware  | UPS (X-Slot)                 | 1     |            | 049C18DB44 |
| 05dd:a011 | Delta E... | MINUTEMAN UPS                | 1     | usbhid     | E091D03447 |
| 06da:0002 | Phoenix... | UPS                          | 1     |            | 4F5E89A87E |
| 06da:ffff | Phoenix... | Offline UPS                  | 1     | usbhid     | BF9A749A5B |
| 0764:0601 | Cyber P... | PR1500LCDRT2U UPS            | 1     | usbhid     | 91C657B5A8 |
| 09ae:2010 | Tripp Lite | UPS                          | 1     | usbhid     | 5D1A48EE4E |
| 09ae:2012 | Tripp Lite | UPS                          | 1     | usbhid     | 8C61A8435F |
| 0d9f:00a6 | Powercom   | Black Knight PRO Uninterr... | 1     | usbhid     | B9B505B7F4 |

### Video (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 048d:9006 | Integra... | IT9135 BDA Afatech DVB-T ... | 3     | dvb_usb... | EDB984C4E6 |
| 2304:0224 | Pinnacl... | MovieBox Plus (710-USB)      | 2     |            | E9365BC8D0 |
| 1164:1ee9 | YUAN Hi... | Polaris AV Capture           | 1     |            | 741E3E3C76 |

### Webcam (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 145f:013c | Trust      | Multimedia audio controller  | 1     | gspca_p... | DE1E5CFB7F |

### Wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 22b8:2e25 | Motorol... | Moto E (4)                   | 5     | rndis_host | 11B9D955A8 |
| 2717:ff88 | Android    | Android                      | 5     | rndis_host | 6C00967A8C |
| 0b05:7782 | ASUSTek... | Device CD-ROM                | 4     | rndis_host | 1F62DD8462 |
| 1286:4e31 | Marvell... | Mobile Composite Device Bus  | 2     | rndis_host | F497726038 |
| 17ef:782f | Lenovo     | Lenovo                       | 2     | rndis_host | 7BA3F44B0A |
| 216f:0044 | Altair ... | Modem YOTA 4G LTE            | 2     | rndis_host | 92A7F0EA0B |
| 0421:0704 | Nokia M... | Nokia_X2 (RM-1013)           | 1     | rndis_host | 2762E434EB |
| 0fce:7193 | Sony Er... | C6603                        | 1     | rndis_host | F748A62EBE |
| 0fce:71f6 | Sony Er... | H4311                        | 1     | rndis_host | C9FFEA0EA6 |
| 0fce:81bb | Sony Er... | D5803                        | 1     | rndis_host | 7D93192AAE |
| 12d1:1039 | Huawei ... | Ideos (tethering mode)       | 1     | rndis_host | 98CECC7420 |
| 12d1:107c | Huawei ... | Che2-L11                     | 1     | rndis_host | 6D94D26ECD |
| 2970:2005 | Wileyfox   | Spark                        | 1     | rndis_host | 2E016FB88C |

### Xbox (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:0289 | Microsoft  | Xbox Controller S            | 1     | xpad       | 15784675B6 |

### Others (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0e8d:2008 | MediaTek   | Impress_Luck                 | 15    | usbfs      | FE9E0ACDE5 |
| 2717:ff40 | MediaTek   | SDM636-MTP _SN:02BF3EA3      | 14    | usbfs      | 13D633A029 |
| 04b8:014a | Seiko E... | Perfection V37/V370          | 9     |            | CB151DFE45 |
| 0955:0007 | Nvidia     | stereo controller            | 6     |            | F6D05FF577 |
| 045e:02ea | Microsoft  | Xbox One S Controller        | 5     | xpad       | 85B34D0336 |
| 045e:028f | Microsoft  | Xbox360 Wireless Controller  | 4     |            | 09CCAF8CCF |
| 045e:02e6 | Microsoft  | Wireless XBox Controller ... | 4     |            | 7AF20CDF5A |
| 04eb:e033 | Northst... | eHome Infrared Transceiver   | 4     |            | A8D0FA2257 |
| 0ccd:0080 | TerraTe... | DMX 6Fire USB                | 4     | snd_usb... | 74C3A10E25 |
| 0e8d:201d | MediaTek   | Tele2_Maxi_LTE               | 4     | usbfs      | 3FB0BFFFCA |
| 1b1c:0c09 | Corsair    | H100i v2                     | 4     |            | 63314DCB31 |
| 22b8:2e82 | Motorol... | Moto Z (2)                   | 4     | usbfs      | ADB52FAE26 |
| 2433:b200 | ASETEK     | 690LC                        | 4     |            | 7F1BE20709 |
| 2833:0211 | Oculus VR  | Rift Sensor                  | 4     | uvcvideo   | B3E32D1026 |
| 0483:3748 | STMicro... | ST-LINK/V2                   | 3     |            | A8EF2FFC63 |
| 04b8:013c | Seiko E... | Perfection V19               | 3     |            | 4DDFFD5967 |
| 085c:0400 | ColorVi... | Spyder 4                     | 3     |            | D8D0364599 |
| 131d:0156 | Natural... | TrackIR 4 Pro Head Tracker   | 3     |            | CC9738C393 |
| 16c0:05dc | Van Ooi... | shared ID for use with li... | 3     |            | 78F624C19E |
| 1782:4001 | Spreadt... | IQ4505 Quad                  | 3     |            | 35B986C185 |
| 1b71:3002 | Fushicai   | USBTV007 Video Grabber [E... | 3     | usbtv      | C80D56CB77 |
| 045e:02ad | Microsoft  | Xbox NUI Audio               | 2     |            | 2A34F25D17 |
| 045e:02d1 | Microsoft  | Xbox One Controller          | 2     | xpad       | 4B4782AC1D |
| 045e:02fe | Microsoft  | XBOX ACC                     | 2     |            | 4D739ABCDF |
| 0471:060d | Philips... | Consumer Infrared Transce... | 2     | mceusb     | E7259783D1 |
| 0471:0815 | Philips... | eHome Infrared Receiver      | 2     | mceusb     | 0E8628D300 |
| 04b8:0130 | Seiko E... | GT-X770 [Perfection V500]    | 2     |            | B86DF74030 |
| 04b9:0300 | Rainbow... | SafeNet USB SuperPro/Ultr... | 2     |            | F58375E009 |
| 04f2:a216 | Chicony... | Digital Video Device         | 2     |            | 4F0A8E2C5B |
| 0502:3643 | Acer       | E39                          | 2     |            | B90C237653 |
| 0582:012a | Roland     | UM-ONE                       | 2     | snd_usb... | 1E67C4F731 |
| 05c6:9039 | Qualcomm   | Android                      | 2     | usbfs      | 444FDD1AF8 |
| 0644:8021 | TEAC       | TASCAM US-122mkII            | 2     | snd_usb... | 7F74A650DB |
| 07ca:3835 | AVerMed... | AVerTV Volar Green HD (A8... | 2     | dvb_usb... | 945C737A32 |
| 07ca:8591 | AVerMed... |                              | 2     |            | BE8250E028 |
| 07ca:c039 | AVerMed... | C039 USB Pure Capture        | 2     |            | CF1F13340E |
| 0819:0101 | eLicenser  | License Management and Co... | 2     |            | FDC986FDE7 |
| 0971:2000 | Gretag-... | i1 Pro                       | 2     |            | 6B863A586E |
| 0b05:7772 | ASUSTek... | ASUS Zenfone GO (ZB500KL)... | 2     |            | CBCF30CB1A |
| 0e6f:02b8 | Logic3     | Afterglow Wired Controlle... | 2     | xpad       | 53ABDA4642 |
| 0e8d:201c | MediaTek   | M5                           | 2     |            | 3D80EACDFC |
| 17e9:019e | Display... | USB-DVI                      | 2     | udl        | B972AC5E0E |
| 1934:0702 | Feature... | Integrated Consumer Infra... | 2     | mceusb     | 62E3C9247C |
| 1b1c:0c03 | Corsair    | H100iGTX Cooler              | 2     |            | 1E0208BF20 |
| 1b1c:0c08 | Corsair    | H80i v2                      | 2     |            | CE19512CBB |
| 1b1c:0c12 | Corsair    | H150i Platinum               | 2     |            | 10A2FF392A |
| 1b1c:0c13 | Corsair    | H115i Platinum               | 2     |            | DE305E87C5 |
| 1c88:0007 | Somagic    | SMI Grabber (EasyCAP DC60... | 2     |            | 63C667B4E6 |
| 2013:0245 | PCTV Sy... | PCTV 73ESE                   | 2     | dvb_usb... | B9F2AE0CCF |
| 2040:0265 | Hauppauge  | dualHD                       | 2     | em28xx     | 5DDCA20D30 |
| 22b8:2e76 | Motorol... | moto e5 play                 | 2     | usbfs      | 54C54C6722 |
| 22b8:2e81 | Motorol... | moto z3                      | 2     | usbfs      | 3E998F19FB |
| 24c6:543a | Unknown... | Xbox ONE Pro Ex controller   | 2     | xpad       | C09E006619 |
| 9710:7780 | MosChip... | MCS7780 4Mbps Fast IrDA A... | 2     | mcs7780    | CBBD77219D |
| 03e7:2150 | Intel      | Myriad VPU [Movidius Neur... | 1     |            | 40F6677C70 |
| 03ee:2501 | Mitsumi    | eHome Infrared Receiver      | 1     | mceusb     | A6A38971EE |
| 03fd:0008 | Xilinx     | Platform Cable USB II        | 1     |            | 428E5A184D |
| 0421:069a | Nokia M... | 130 [RM-1035] (Charging o... | 1     |            | B780E08BB9 |
| 0421:0708 | Nokia M... | Nokia_X2 (RM-1013)           | 1     |            | CB20E8407A |
| 0424:2530 | Standar... | Bridge device                | 1     |            | 90F9BD30F6 |
| 0451:d00f | Texas I... | OMAP4430                     | 1     |            | 3D5D3F552C |
| 0458:2019 | KYE Sys... | ColorPage-HR6X Slim          | 1     |            | 0D2C002967 |
| 0458:201d | KYE Sys... | ColorPage-Vivid 1200 X       | 1     |            | 652A294EF2 |
| 045e:02b0 | Microsoft  | Xbox NUI Motor               | 1     |            | 2A34F25D17 |
| 046d:0a46 | Logitech   |                              | 1     |            | 46B504A53C |
| 0471:060c | Philips... | Consumer Infrared Transce... | 1     | mceusb     | 28DFB478B7 |
| 0471:060f | Philips... | Consumer Infrared Transce... | 1     | mceusb     | 56053B0099 |
| 0482:089a | Kyocera    | Android                      | 1     | usbfs      | 3D65B5694E |
| 0483:3746 | STMicro... | ST Micro Connect Lite        | 1     | ftdi_sio   | D6EA87C3B3 |
| 0483:374b | STMicro... | ST-LINK/V2.1                 | 1     | cdc_acm    | F8AA7F430A |
| 048d:9910 | Integra... | IT9910 chipset based grabber | 1     |            | 7EA32D5906 |
| 0499:1507 | Yamaha     | THR10                        | 1     | snd_usb... | 504173F856 |
| 04b4:1002 | Cypress... | CY7C63001 R100 FM Radio      | 1     | dsbr100    | 5AF760C6C0 |
| 04b4:bd14 | Cypress... |                              | 1     |            | 36C4938012 |
| 04b8:013d | Seiko E... | Perfection V39               | 1     |            | 6E57A5E180 |
| 04b8:0151 | Seiko E... | Perfection V800 Photo        | 1     |            | D1AADDC33A |
| 04b9:8004 | Rainbow... | Sentinel Dual Hardware Key   | 1     |            | 39F3C43A6B |
| 04c5:158c | Fujitsu    | arrowsM04                    | 1     |            | 7D4741D740 |
| 04d8:9009 | Microch... | MPLAB ICD3                   | 1     |            | 10A58D1A56 |
| 04d8:e11c | Microch... | TL866CS EEPROM Programmer... | 1     |            | 3A1C2004B1 |
| 04e7:0073 | Elo Tou... | Inc. APR Controller          | 1     |            | 523EA52130 |
| 04e8:2059 | Samsung... |                              | 1     |            | 29ECA9D63C |
| 04e8:507d | Samsung... | YP-U3 (mtp)                  | 1     |            | 2E23494FDE |
| 04f3:0c28 | Elan Mi... | ELAN:Fingerprint             | 1     |            | 13A468E1E4 |
| 04f9:037f | Brother... | ADS-2600We                   | 1     |            | 2909D1E3A6 |
| 0529:0514 | Aladdin... | eToken Pro v4.2.5.4          | 1     |            | E72B408F31 |
| 0572:c68a | Conexan... | EyeTV Stick                  | 1     |            | 0938ABC248 |
| 0582:0000 | Roland     | UA-100(G)                    | 1     | snd_usb... | E1DB1363ED |
| 0582:0132 | Roland     | TRI-CAPTURE                  | 1     | snd_usb... | 1E01EC96F7 |
| 0582:017a | Roland     | VT-3                         | 1     | snd_usb... | 3E9DFA313B |
| 05c6:6769 | Qualcomm   | A0001                        | 1     |            | F0BEF09470 |
| 05c6:900a | Qualcomm   | CDMA Technologies MSM        | 1     |            | FCCBB4A17D |
| 05c6:9091 | Qualcomm   | Android                      | 1     | usbfs      | CBB464C414 |
| 05da:3008 | Microte... | Scanner                      | 1     |            | CC79E0559C |
| 0638:2a1f | Avision    | FB2280E                      | 1     | usbfs      | 76C47C1068 |
| 0644:8035 | TEAC       | US-600                       | 1     |            | B0F616B23E |
| 064f:0bd8 | WIBU-Sy... | Wibu-Box/RU                  | 1     |            | 656140D22A |
| 067b:23a3 | Prolifi... | 123_AaP                      | 1     |            | A3A29982FD |
| 0763:1002 | M-Audio    | MidiSport 2x2                | 1     | snd_usb... | 4B9FC100D8 |
| 0763:2031 | Midiman    | Fast Track C600              | 1     | snd_usb... | 82F7574CAD |

