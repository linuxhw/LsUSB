Most popular USB devices in All In Ones
=======================================

See more info in the [README](https://github.com/linuxhw/LsUSB).

Contents
--------

1. [ USB Devices in All In Ones ](#usb-devices)
   * [ Bluetooth ](#bluetooth-usb)
   * [ Camera ](#camera-usb)
   * [ Card reader ](#card-reader-usb)
   * [ Cdrom ](#cdrom-usb)
   * [ Chipcard ](#chipcard-usb)
   * [ Disk ](#disk-usb)
   * [ Dvb card ](#dvb-card-usb)
   * [ Gamepad ](#gamepad-usb)
   * [ Hasp ](#hasp-usb)
   * [ Hub ](#hub-usb)
   * [ Human interface ](#human-interface-usb)
   * [ Input/keyboard ](#inputkeyboard-usb)
   * [ Input/mouse ](#inputmouse-usb)
   * [ Isdn adapter ](#isdn-adapter-usb)
   * [ Mfp ](#mfp-usb)
   * [ Modem ](#modem-usb)
   * [ Net/wireless ](#netwireless-usb)
   * [ Network ](#network-usb)
   * [ Printer ](#printer-usb)
   * [ Scanner ](#scanner-usb)
   * [ Serial controller ](#serial-controller-usb)
   * [ Sound ](#sound-usb)
   * [ Touchscreen ](#touchscreen-usb)
   * [ Tv card ](#tv-card-usb)
   * [ Ups ](#ups-usb)
   * [ Others ](#others-usb)

USB Devices
-----------

Count  — number of computers with this device installed,
Driver — driver in use for this device,
Probe  — latest probe ID of this device.

### Bluetooth (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 05ac:8215 | Apple      | Built-in Bluetooth 2.0+ED... | 25    | btusb      | 561B991E16 |
| 8087:0a2a | Intel      | Bluetooth Device             | 15    | btusb      | 2BF103DD36 |
| 0a12:0001 | Cambrid... | Bluetooth Dongle (HCI mode)  | 13    | btusb      | 59BB8EC907 |
| 05ac:820f | Apple      | Bluetooth HCI                | 8     | btusb      | B6D6A0F54D |
| 0bda:b00a | Realtek... | Bluetooth Radio              | 8     | btusb      | 94BD16E6CB |
| 0cf3:3004 | Qualcom... | AR3012 Bluetooth 4.0         | 8     | ath3k, ... | 3EBFFF7DAD |
| 0cf3:3005 | Qualcom... | AR3011 Bluetooth             | 7     | btusb      | 59BB8EC907 |
| 0cf3:e500 | Qualcom... | Qualcomm Atheros Bluetoot... | 7     | btusb      | 30FF6DAB9F |
| 13d3:3491 | IMC Net... | Bluetooth Device             | 7     | btusb      | 0FF0BDD7C8 |
| 04ca:3006 | Lite-On... | Lite-On Bluetooth Device     | 6     | ath3k, ... | BEDE9EC674 |
| 05ac:8206 | Apple      | Bluetooth HCI                | 6     | btusb      | B8314E6CBC |
| 05ac:828d | Apple      | Bluetooth USB Host Contro... | 6     | btusb      | 2CB78484FB |
| 8087:0aa7 | Intel      | Bluetooth Device             | 6     | btusb      | 2288828F06 |
| 18e8:6252 | Qcom       | Broadcom Bluetooth USB       | 5     | btusb      | A2FF4426C9 |
| 0489:e00f | Foxconn... | Foxconn T77H114 BCM2070 [... | 3     | btusb      | DE93F78B0C |
| 04ca:300b | Lite-On... | Lite-On Bluetooth Device     | 3     | ath3k, ... | 3370E1020B |
| 04ca:3015 | Lite-On... | Lite-On Bluetooth Device     | 3     | btusb      | 21CA50C386 |
| 0cf3:e004 | Qualcom... | Bluetooth USB Host Contro... | 3     | ath3k, ... | 229BA614EC |
| 0cf3:e005 | Qualcom... | Qualcomm Atheros Bluetoot... | 3     | ath3k, ... | C1A91BB2D3 |
| 0cf3:e007 | Qualcom... | Qualcomm Atheros Bluetoot... | 3     | btusb      | F41BD5BF5B |
| 8087:0a2b | Intel      | Bluetooth Device             | 3     | btusb      | 63FB881055 |
| 0489:e036 | Foxconn... | Bluetooth USB Host Contro... | 2     | ath3k, ... | 7685534B8E |
| 05ac:828b | Apple      | Bluetooth USB Host Contro... | 2     | btusb      | E9C3A73481 |
| 0a5c:21f1 | Broadcom   | HP Portable Bumble Bee       | 2     | btusb      | 3E13F8A157 |
| 0bda:b008 | Realtek... | Bluetooth Radio              | 2     | btusb      | 0F82F36E74 |
| 0bda:b00b | Realtek... | Bluetooth Radio              | 2     | btusb      | 8269929E09 |
| 0bda:b728 | Realtek... | Bluetooth Radio              | 2     | btusb      | F14A7F7311 |
| 0bda:b739 | Realtek... | Bluetooth Radio              | 2     | btusb      | 275E0EEAEF |
| 10ab:0816 | USI        | Bluetooth Module BCM92070    | 2     | btusb      | 114C724C7E |
| 13d3:3394 | IMC Net... | Bluetooth                    | 2     | btusb      | 78D39E18EF |
| 13d3:3410 | IMC Net... | Bluetooth Radio              | 2     | btusb      | 881CDBC376 |
| 13d3:3458 | IMC Net... | Bluetooth Radio              | 2     | btusb      | 2146FDE0CF |
| 8086:0189 | Intel      | Bluetooth Device             | 2     | btusb      | 8672A3F85D |
| 8087:0aaa | Intel      | Bluetooth Device 9460/9560   | 2     | btusb      | 4313964FA7 |
| 044e:3017 | Alps El... | BCM2046 Bluetooth Device     | 1     | btusb      | 2628EBE1BB |
| 0a5c:216d | Broadcom   | BCM43142A0 Bluetooth 4.0     | 1     | btusb      | 1571B0B373 |
| 0b05:17cb | ASUSTek... | Broadcom BCM20702A0 Bluet... | 1     | btusb      | 0DBBF3578B |
| 0bda:b723 | Realtek... | Bluetooth Radio              | 1     | btusb      | E745E03926 |
| 0bda:c024 | Realtek... | Bluetooth Radio              | 1     | btusb      | 7E41940C9C |
| 0cf3:0036 | Qualcom... | Qualcomm Atheros Bluetoot... | 1     | ath3k, ... | 3FEDAA45BD |
| 0cf3:e009 | Qualcom... | Qualcomm Atheros Bluetoot... | 1     | btusb      | FF0EDA1857 |
| 0db0:a872 | Micro S... | Bluetooth Device             | 1     | btusb      | CBD910EA06 |
| 13d3:3414 | IMC Net... | Bluetooth Radio              | 1     | btusb      | 64496AD27B |
| 13d3:3416 | IMC Net... | Bluetooth Radio              | 1     | btusb      | D2834ADB05 |
| 13d3:3472 | IMC Net... | Bluetooth Device             | 1     | btusb      | EE7F588C0D |
| 13d3:3478 | IMC Net... | Bluetooth Device             | 1     | btusb      | 350CC83AC7 |
| 8087:07dc | Intel      | Bluetooth Device             | 1     | btusb      | C95062D4FF |

### Camera (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 05ac:8502 | Apple      | Built-in iSight              | 26    | uvcvideo   | B6D6A0F54D |
| 0ac8:c450 | Z-Star ... | Lenovo USB2.0 UVC Camera     | 18    | uvcvideo   | 0DBBF3578B |
| 05ac:850b | Apple      | FaceTime HD Camera (Built... | 10    | uvcvideo   | 561B991E16 |
| 05ac:8511 | Apple      | FaceTime HD Camera (Built... | 8     | uvcvideo   | 2CB78484FB |
| 04f2:b28b | Chicony... | CNFA257                      | 7     | uvcvideo   | 4CA7976A88 |
| 0ac8:c412 | Z-Star ... | Lenovo USB2.0 UVC Camera     | 7     | uvcvideo   | E98AA97F27 |
| 04f2:b45a | Chicony... | USB2.0 FHD Camera            | 6     | uvcvideo   | 78D39E18EF |
| 05ca:18c0 | Ricoh      | USB2.0 Camera                | 5     | uvcvideo   | 59BB8EC907 |
| 174f:240a | Syntek     | Integrated Camera            | 5     | uvcvideo   | E1331356C7 |
| 2232:1038 | Silicon... | WebCam SC-10IRQ12340N        | 5     | uvcvideo   | 857C8BE5D0 |
| 0408:5185 | Quanta ... | HP High Definition 1MP We... | 4     | uvcvideo   | AAFE223352 |
| 04f2:b185 | Chicony... | HD Video WebCam              | 4     | uvcvideo   | A2FF4426C9 |
| 0ac8:c42d | Z-Star ... | Lenovo IdeaCentre Web Camera | 4     | snd_usb... | EA55AE13AC |
| 0bda:57f6 | Realtek... | USB2.0 HD UVC WebCam         | 4     | uvcvideo   | 0FF0BDD7C8 |
| 13d3:56a3 | IMC Net... | USB2.0 HD UVC WebCam         | 4     | uvcvideo   | 4313964FA7 |
| 04f2:b1f3 | Chicony... | CNFA213                      | 3     | uvcvideo   | 196FDD9B5C |
| 04f2:b34b | Chicony... | USB 2.0 Camera               | 3     | uvcvideo   | BEDE9EC674 |
| 058f:3862 | Alcor M... | USB 2.0 PC Camera            | 3     | snd_usb... | 09FE5E4566 |
| 05c8:0230 | Cheng U... | HP Integrated Webcam         | 3     | uvcvideo   | 8ED375662D |
| 0bda:5683 | Realtek... | Integrated_Webcam_HD         | 3     | uvcvideo   | 7CC5A36861 |
| 174f:14ca | Syntek     | LENOVO LBG 720P CAM          | 3     | uvcvideo   | 5F6FF0EBDA |
| 174f:240e | Syntek     | Integrated Camera            | 3     | uvcvideo   | 30FF6DAB9F |
| 5986:055c | Acer       | BisonCam, NB Pro             | 3     | uvcvideo   | 881CDBC376 |
| 0408:5221 | Quanta ... | HP Integrated Webcam         | 2     | uvcvideo   | 0F82F36E74 |
| 04f2:b1e8 | Chicony... | USB 2.0 Camera               | 2     | uvcvideo   | 1E17CF76B7 |
| 04f2:b2a8 | Chicony... | Laptop_Integrated_Webcam_HD  | 2     | uvcvideo   | 8672A3F85D |
| 04f2:b2c2 | Chicony... | 1.3M WebCam                  | 2     | uvcvideo   | 114C724C7E |
| 04f2:b383 | Chicony... | USB 2.0 camera               | 2     | uvcvideo   | 1EAD53F6C2 |
| 04f2:b40b | Chicony... | HP High Definition 1MP We... | 2     | uvcvideo   | 499EA7DFBB |
| 05c8:036a | Cheng U... | HP High Definition 1MP We... | 2     | uvcvideo   | 8553F899D5 |
| 05ca:18b5 | Ricoh      | Sony Vaio Integrated Webcam  | 2     | uvcvideo   | 2628EBE1BB |
| 064e:9209 | Suyin      | Integrated_Webcam_HD         | 2     | uvcvideo   | D07D321CCE |
| 064e:a213 | Suyin      | USB 2.0 Camera               | 2     | uvcvideo   | DE93F78B0C |
| 090c:37c0 | Silicon... | Silicon Motion Camera        | 2     | uvcvideo   | D1E3B9E1FB |
| 0ac8:c430 | Z-Star ... | Lenovo USB 2.0 UVC Camera    | 2     | snd_usb... | 55C2F8E26B |
| 0bda:57ab | Realtek... | Integrated_Webcam_HD         | 2     | uvcvideo   | 350CC83AC7 |
| 0bda:5801 | Realtek... | USB2.0-Camera                | 2     | uvcvideo   | A7CE72E3F1 |
| 0c45:6494 | Microdia   | Laptop_Integrated_Webcam_HD  | 2     | uvcvideo   | 98B3AC895B |
| 0c45:6712 | Microdia   | Integrated_Webcam_HD         | 2     | uvcvideo   | 275E0EEAEF |
| 174f:14cd | Syntek     | LENOVO LBG 720P CAM          | 2     | uvcvideo   | 91FFB55E1F |
| 1bcf:28bc | Sunplus... | Integrated_Webcam_HD         | 2     | uvcvideo   | FF0EDA1857 |
| 1bcf:2c8b | Sunplus... | HP High Definition 1MP We... | 2     | uvcvideo   | 447E6C6392 |
| 5986:0241 | Acer       | BisonCam, NB Pro             | 2     | uvcvideo   | 17AD880F72 |
| 5986:0313 | Acer       | HP Webcam                    | 2     | uvcvideo   | 5DC25F1C8F |
| 5986:065c | Acer       | LENOVO LBG 720P CAM          | 2     | uvcvideo   | C486FF05F5 |
| 5986:0678 | Acer       | USB 2.0 Camera               | 2     | uvcvideo   | 21CA50C386 |
| 5986:110d | Acer       | Integrated Camera            | 2     | uvcvideo   | 7E41940C9C |
| 0408:5230 | Quanta ... | HP 2.0MP High Definition ... | 1     | uvcvideo   | AC62725ABE |
| 0408:5362 | Quanta ... | HP TrueVision HD Camera      | 1     | uvcvideo   | 94BD16E6CB |
| 0408:5380 | Quanta ... | HP 2.0MP High Definition ... | 1     | uvcvideo   | 8269929E09 |
| 040a:0608 | Kodak      | EASYSHARE C122 DIGITAL CA... | 1     |            | 67B12475CF |
| 0421:0663 | Nokia M... | 301 Dual SIM                 | 1     |            | 89E79AAB3C |
| 0421:06fc | Nokia M... | Lumia 640 XL Dual SIM (RM... | 1     |            | 3321732E8E |
| 046d:0826 | Logitech   | HD Webcam C525               | 1     | uvcvideo   | 114C724C7E |
| 04e8:6860 | Samsung... | GT-I9100 Phone [Galaxy S ... | 1     |            | 70CA845CDF |
| 04f2:b104 | Chicony... | CNF7069 Webcam               | 1     | uvcvideo   | 0FFE695213 |
| 04f2:b27d | Chicony... | USB 2.0 camera               | 1     | uvcvideo   | A629A81791 |
| 04f2:b2c1 | Chicony... | CNFB219                      | 1     | snd_usb... | D44BEF08F5 |
| 04f2:b301 | Chicony... | IR camera                    | 1     | uvcvideo   | 81CEB684C0 |
| 04f2:b32c | Chicony... | USB 2.0 Camera               | 1     | uvcvideo   | D973B3B4E4 |
| 04f2:b37a | Chicony... | USB2.0 FHD UVC WebCam        | 1     | uvcvideo   | C8D8836613 |
| 04f2:b461 | Chicony... | USB2.0 HD Camera             | 1     | uvcvideo   | 3370E1020B |
| 04f2:b508 | Chicony... | HP 2.0MP High Definition ... | 1     | uvcvideo   | 2281502A3C |
| 04f2:b53e | Chicony... | USB2.0 FHD Camera            | 1     | uvcvideo   | E7395D0EE2 |
| 04fc:2080 | Sunplus... | ASUS Webcam                  | 1     | uvcvideo   | AADAAE6450 |
| 05ac:8501 | Apple      | Built-in iSight [Micron]     | 1     |            | 6A774ADE41 |
| 05c8:034d | Cheng U... | USB 2.0 Camera               | 1     | uvcvideo   | D3734872B2 |
| 05c8:03c2 | Cheng U... | HP High Definition 1MP We... | 1     | uvcvideo   | 2B0CDC9F2B |
| 064e:810e | Suyin      | Laptop_Integrated_Webcam_2M  | 1     | uvcvideo   | 2E89DCF36E |
| 064e:8120 | Suyin      | Laptop_Integrated_Webcam_HD  | 1     | uvcvideo   | F92CB57F54 |
| 064e:9323 | Suyin      | HP Integrated Webcam         | 1     | uvcvideo   | F01E3C859D |
| 064e:9324 | Suyin      | HP Integrated Webcam         | 1     | uvcvideo   | 1571B0B373 |
| 064e:9326 | Suyin      | HP Integrated Webcam         | 1     | uvcvideo   | DD94897A21 |
| 064e:e261 | Suyin      | 1.3M WebCam                  | 1     | uvcvideo   | 973EEE4869 |
| 064e:e281 | Suyin      | HP Webcam                    | 1     | uvcvideo   | 68246E58C1 |
| 090c:037c | Silicon... | 300k Pixel Camera            | 1     | uvcvideo   | A2CB18D2E5 |
| 0ac8:3343 | Z-Star ... | Sirius USB 2.0 Camera        | 1     | uvcvideo   | E4BDC45558 |
| 0ac8:c458 | Z-Star ... | Integrated Camera            | 1     | uvcvideo   | 807B68E120 |
| 0bda:5694 | Realtek... | Integrated_Webcam_HD         | 1     | uvcvideo   | 822331563B |
| 0bda:573d | Realtek... | HP 1.0MP High Definition ... | 1     | uvcvideo   | 45679FA2F6 |
| 0bda:5763 | Realtek... | Integrated_Webcam_HD         | 1     | uvcvideo   | 2BF103DD36 |
| 0bda:57af | Realtek... | LENOVO LBG 1080P CAM         | 1     | uvcvideo   | 58DDD6F565 |
| 0bda:5830 | Realtek... | USB Camera                   | 1     | uvcvideo   | 30323EE603 |
| 0bda:58c3 | Realtek... | Integrated_Webcam_HD         | 1     | uvcvideo   | F41BD5BF5B |
| 0c45:6444 | Microdia   | Laptop_Integrated_Webcam_HD  | 1     | uvcvideo   | 0776B86016 |
| 0c45:6493 | Microdia   | Laptop_Integrated_Webcam_HD  | 1     | uvcvideo   | 229BA614EC |
| 0c45:64c6 | Microdia   | Integrated_Webcam_FHD        | 1     | uvcvideo   | 3FEDAA45BD |
| 0c45:64d8 | Microdia   | Integrated_Webcam_HD         | 1     | uvcvideo   | EFDACF8C18 |
| 0c45:670a | Microdia   | Integrated_Webcam_HD         | 1     | uvcvideo   | C1A91BB2D3 |
| 1004:633e | LG Elec... | G2/G3 Android Phone [MTP/... | 1     | usbfs      | DD94897A21 |
| 13d3:5654 | IMC Net... | USB2.0 UVC HD Webcam         | 1     | uvcvideo   | 64496AD27B |
| 13d3:5666 | IMC Net... | USB2.0 HD UVC WebCam         | 1     | uvcvideo   | D50551A34E |
| 13d3:5702 | IMC Net... | UVC VGA Webcam               | 1     | uvcvideo   | 7F0F347502 |
| 174f:149a | Syntek     | Lenovo EasyCamera            | 1     | uvcvideo   | 9964A3852B |
| 174f:149c | Syntek     | Lenovo EasyCamera            | 1     | uvcvideo   | F14A7F7311 |
| 174f:149d | Syntek     | Lenovo USB2.0 UVC Camera     | 1     | uvcvideo   | 93F51B8717 |
| 174f:14af | Syntek     | Lenovo EasyCamera            | 1     | uvcvideo   | 934BF6308F |
| 174f:1729 | Syntek     | ASUS USB2.0 camera           | 1     | uvcvideo   | 0633624824 |
| 174f:174b | Syntek     | USB Video Device             | 1     | uvcvideo   | 90FF435FAD |
| 174f:175d | Syntek     | Integrated Camera            | 1     | uvcvideo   | 63FB881055 |

### Card reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:0129 | Realtek... | RTS5129 Card Reader Contr... | 22    | rtsx_usb   | 8A67730AF4 |
| 0bda:0139 | Realtek... | RTS5139 Card Reader Contr... | 12    | rtsx_usb   | 0FC1178D7D |

### Cdrom (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 8564:1000 | Transcend  | TS32GJF370 JetFlash 32GB     | 11    | uas, us... | 7CD9296ED7 |
| 04c5:2028 | Fujitsu    | External HDD 128GB           | 3     | uas, us... | C3AA8C3D72 |
| 0e8d:1806 | MediaTek   | Samsung SE-208AB Slim Por... | 3     | usb_sto... | 5E0E0CDD48 |
| 0e8d:1887 | MediaTek   | DVDRAM GP60NS50              | 3     | usb_sto... | 94BD16E6CB |
| 174c:55aa | ASMedia... | Name: ASM1051E SATA 6Gb/s... | 3     | usb_sto... | 0FC1178D7D |
| 04fc:0c25 | Sunplus... | SATALink SPIF225A 320GB      | 2     | uas, us... | 5F28079AA4 |
| 152d:2329 | JMicron... | JM20329 SATA Bridge          | 2     | usb_sto... | D6B9184305 |
| 0402:5621 | ALi        | M5621 High-Speed IDE Cont... | 1     | usb_sto... | DDBF7B395A |
| 048d:1176 | Integra... | USB Flash Disk 16GB          | 1     | uas, us... | 3FEDAA45BD |
| 05ac:1500 | Apple      | SuperDrive [A1379]           | 1     | uas, us... | FA22E55FA8 |
| 0b05:7780 | ASUSTek... | Device CD-ROM                | 1     | uas, us... | 4F5252C592 |
| 0bb4:0f25 | HTC (Hi... | One M8                       | 1     | uas, us... | 447E6C6392 |
| 13fd:0840 | Initio     | INIC-1618L SATA 100GB        | 1     | usb_sto... | 275E0EEAEF |
| 13fd:3e40 | Initio     | ZALMAN ZM-VE350 256GB        | 1     | uas, us... | 3C0BD31D74 |
| 152d:0578 | JMicron... | JMS567 SATA 6Gb/s bridge     | 1     | uas        | 350CC83AC7 |
| 1782:5d03 | Spreadt... | umopenphone 34GB             | 1     | uas, us... | B790C5FA32 |

### Chipcard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 072f:90cc | Advance... | ACR38 SmartCard Reader       | 1     |            | 7F0F347502 |
| 072f:90de | Advance... | Token USB 64K                | 1     | usbfs      | 7F6DBA9244 |
| 0a89:0025 | Aktiv      | Rutoken lite                 | 1     | usbfs      | 7F6DBA9244 |

### Disk (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 05ac:8403 | Apple      | Internal Memory Card Reader  | 20    | uas, us... | 561B991E16 |
| 0bda:0153 | Realtek... | 3-in-1 (SD/SDHC/SDXC) Car... | 14    | uas, us... | 94BD16E6CB |
| 090c:1000 | Silicon... | Flash Voyager                | 13    | uas, us... | A68B8E8206 |
| 058f:6387 | Alcor M... | Transcend JetFlash Flash ... | 5     | uas, us... | 9E22E7425D |
| 0930:6545 | Toshiba    | Kingston DataTraveler 102... | 5     | uas, us... | 55C2F8E26B |
| 058f:6366 | Alcor M... | Multi Flash Reader           | 4     | uas, us... | 5F28079AA4 |
| 05e3:0727 | Genesys... | microSD Reader/Writer        | 3     | uas, us... | 7F0F347502 |
| 05e3:0745 | Genesys... | Logilink CR0012 32GB         | 3     | uas, us... | 30323EE603 |
| 0951:1665 | Kingsto... | Digital DataTraveler SE9 ... | 3     | usb_sto... | 777FB79E9C |
| 0951:1666 | Kingsto... | DataTraveler 100 G3/G4/SE... | 3     | uas, us... | C1A91BB2D3 |
| 13fe:3e00 | Kingsto... | Silicon-Power16G 16GB        | 3     | uas, us... | 74411E469D |
| 13fe:4200 | Kingsto... | Silicon-Power32G 32GB        | 3     | uas, us... | E60C7F7B33 |
| 0bc2:5021 | Seagate    | FreeAgent GoFlex USB 2.0 ... | 2     | usb_sto... | 098D8022D0 |
| 0bda:0169 | Realtek... | Mass Storage Device          | 2     | usb_sto... | 2146FDE0CF |
| 0bda:0316 | Realtek... | SD/MMC 128GB                 | 2     | usb_sto... | 2146FDE0CF |
| 13fe:4100 | Kingsto... | Silicon-Power16G 16GB        | 2     | uas, us... | E1331356C7 |
| 13fe:4300 | Kingsto... | USB DISK 2.0 16GB            | 2     | uas, us... | 394B9426A3 |
| 1908:0226 | GEMBIRD    | Mass-Storage 134GB           | 2     | uas, us... | 4F5252C592 |
| abcd:1234 | Chipsbnk   | Alu Line 16GB                | 2     | uas, us... | 7048174273 |
| 03f0:5c11 | Hewlett... | PhotoSmart C4200 Printer ... | 1     | usb_sto... | D973B3B4E4 |
| 0480:a200 | Toshiba... | External USB 3.0             | 1     | uas, us... | 8553F899D5 |
| 0480:b001 | Toshiba... | Stor.E Partner 500GB         | 1     | uas, us... | 797B5C4507 |
| 048d:1167 | Integra... | USB Flash Drive 4GB          | 1     | uas, us... | F4A6CD8AB4 |
| 048d:1337 | Integra... | Storage Device 32GB          | 1     | usb_sto... | EBBCA68281 |
| 04b8:0891 | Seiko E... | Stylus Office BX535WD        | 1     | usb_sto... | 8269929E09 |
| 04e8:6031 | Samsung... | m3 portable 3.0              | 1     | uas, us... | A48C6C8ECF |
| 04e8:61b6 | Samsung... | M3 Portable Hard Drive       | 1     | uas, us... | F1FDC67882 |
| 04e8:61f5 | Samsung... | Portable SSD T5              | 1     | uas        | 2CB78484FB |
| 04e8:681d | Samsung... | Galaxy Portal/Spica Andro... | 1     | uas, us... | 7048174273 |
| 04f1:0008 | Victor ... | GZ-MG30AA/MC500E Digital ... | 1     | uas, us... | B790C5FA32 |
| 054c:05b8 | Sony       | Storage Media 16GB           | 1     | uas, us... | FC55716771 |
| 054c:05ba | Sony       | Storage Media 16GB           | 1     | uas, us... | 16D6FCA3F1 |
| 058f:6364 | Alcor M... | AU6477 Card Reader Contro... | 1     | uas, us... | C8D8836613 |
| 05dc:a81d | Lexar M... | LJDTT16G [JumpDrive 16GB]... | 1     | uas, us... | 0218F8BD43 |
| 0718:0710 | Imation    | TF10                         | 1     | usb_sto... | 7BD55BD71D |
| 0781:5567 | SanDisk    | Cruzer Blade 16GB            | 1     | uas, us... | 39D4FD28B4 |
| 0781:556b | SanDisk    | Cruzer Edge 16GB             | 1     | uas, us... | 6A774ADE41 |
| 0781:5571 | SanDisk    | Cruzer Fit 16GB              | 1     | uas, us... | 2BF103DD36 |
| 0781:5575 | SanDisk    | Cruzer Glide 32GB            | 1     | uas, us... | 561B991E16 |
| 0781:5581 | SanDisk    | Ultra 128GB                  | 1     | uas, us... | D77183679A |
| 0781:5583 | SanDisk    | Ultra Fit 123GB              | 1     | uas, us... | CE0D84E8C5 |
| 0781:5597 | SanDisk    | Cruzer Glide 3.0 124GB       | 1     | uas, us... | 7F1ABC90BD |
| 0951:1603 | Kingsto... | DataTraveler 1GB/2GB Pen ... | 1     | uas, us... | 54AF101FFA |
| 0951:1625 | Kingsto... | DataTraveler 101 II 4GB      | 1     | uas, us... | 6838533F5E |
| 0951:1652 | Kingsto... | DT Ultimate                  | 1     | uas, us... | ADAC976045 |
| 0951:16a2 | Kingsto... | DTR30G2 63GB                 | 1     | uas, us... | FA22E55FA8 |
| 0a16:2007 | Trek Te... | Store 'n' Go                 | 1     | uas, us... | 4EB59BF315 |
| 0bb4:0001 | HTC (Hi... | Android Phone via mass st... | 1     | usb_sto... | 5BB4C2C7A5 |
| 0bc2:2320 | Seagate    | USB 3.0 bridge [Portable ... | 1     | uas        | 857C8BE5D0 |
| 0bc2:3101 | Seagate    | FreeAgent XTreme 640GB       | 1     | uas, us... | EA55AE13AC |
| 0bc2:3312 | Seagate    | SRD00F2 Expansion Desktop... | 1     | uas        | 3FDB026301 |
| 0bc2:50a1 | Seagate    | FreeAgent GoFlex Desk        | 1     | uas, us... | 1BD0896C15 |
| 0bc2:ab20 | Seagate    | Backup Plus Portable Drive   | 1     | uas, us... | 35512E3EE5 |
| 0bda:0158 | Realtek... | USB 2.0 multicard reader     | 1     | ums_rea... | AD16F12938 |
| 0bda:0306 | Realtek... | USB3.0 CRW -SD               | 1     | uas, us... | 0776B86016 |
| 1005:b113 | Apacer ... | Handy Steno/AH123 / Handy... | 1     | uas, us... | 16D6FCA3F1 |
| 1058:074a | Western... | My Passport 074A 500GB       | 1     | uas, us... | FE0F4C11DF |
| 1058:0832 | Western... | My Passport 0832 500GB       | 1     | uas, us... | 6F0288A609 |
| 1058:1010 | Western... | Elements Portable (WDBAAR... | 1     | uas, us... | EA55AE13AC |
| 1058:1078 | Western... | Elements Portable (WDBUZG)   | 1     | uas, us... | 6110DAA2C7 |
| 1058:25ee | Western... | My Book 25EE                 | 1     | uas, us... | 3FDB026301 |
| 125f:385a | A-DATA ... | USB Flash Drive 16GB         | 1     | uas, us... | 60A7B5DDD7 |
| 125f:a35a | A-DATA ... | HD650                        | 1     | uas, us... | 60A7B5DDD7 |
| 12d1:14dc | Huawei ... | E33372 LTE/UMTS/GSM HiLin... | 1     | uas, us... | ADD51B1FFA |
| 1307:0163 | Transcend  | 256MB/512MB/1GB Flash Drive  | 1     | uas, us... | C156F0AF59 |
| 1307:0165 | Transcend  | 2GB/4GB/8GB Flash Drive 16GB | 1     | uas, us... | E2E294AF95 |
| 1307:0310 | Transcend  | SD/MicroSD CardReader [ha... | 1     | uas, us... | EB1E63578D |
| 13fe:1f00 | Kingsto... | Kingston DataTraveler / P... | 1     | uas, us... | 94032B7164 |
| 13fe:3100 | Kingsto... | 2/4 GB stick 16GB            | 1     | uas, us... | 6149AFDA8F |
| 13fe:5500 | Kingsto... | Silicon-Power32G 31GB        | 1     | uas, us... | ADFDA7721C |
| 14cd:1212 | Super Top  | microSD card reader (SY-T18) | 1     | uas, us... | C215749CAB |
| 14cd:6116 | Super Top  | M6116 SATA Bridge            | 1     | uas, us... | 97FBC61E68 |
| 1516:1226 | CompUSA    | Disk 16GB                    | 1     | uas, us... | 82BBE8515C |
| 1516:8628 | CompUSA    | Pen Drive 16GB               | 1     | uas, us... | 2AB277563F |
| 152d:0539 | JMicron... | JMS539/567 SuperSpeed SAT... | 1     | uas, us... | 3EBFFF7DAD |
| 152d:2509 | JMicron... | JMS539 SuperSpeed SATA II... | 1     | uas, us... | 60A7B5DDD7 |
| 154b:00ee | PNY        | USB 2.0 FD                   | 1     | uas, us... | 93BC2AD28A |
| 1687:0165 | Kingmax... | USB2.0 FlashDisk 16GB        | 1     | uas, us... | 9F532A1C9A |
| 174c:5106 | ASMedia... | Transcend StoreJet 25M3 3... | 1     | uas, us... | EDBE64E121 |
| 1782:4d01 | Spreadt... | LOCAL DISK                   | 1     | uas, us... | 714EFD4954 |
| 18a5:0214 | Verbatim   | Portable Hard Drive 320GB    | 1     | uas, us... | 237C6D5B2C |
| 18a5:0237 | Verbatim   | Portable Harddrive (500 GB)  | 1     | uas, us... | FC55716771 |
| 18a5:0243 | Verbatim   | Flash Drive (Store'n'Go) ... | 1     | uas, us... | EB1E63578D |
| 18a5:0302 | Verbatim   | STORE N GO 16GB              | 1     | uas, us... | 2FAFA2B1CE |
| 1908:0222 | GEMBIRD    | Card-Reader                  | 1     | uas, us... | 350CC83AC7 |
| 1e3d:2096 | Chipsba... | Flash Disk 4GB               | 1     | uas, us... | B3426FE3C1 |
| 2022:0139 | AMICON     | PNKEY-TLS MSC                | 1     | usb_sto... | 63C1955CF6 |
| 2109:0715 | VIA Labs   | VLI Product String           | 1     | uas        | BB93D1DD97 |
| 2207:0000 | rockchip   | Disk 6GB                     | 1     | uas, us... | 714EFD4954 |
| 2537:1068 | Norelsys   | External USB-3.0             | 1     | uas, us... | 94BD16E6CB |
| 26bf:201a | Specific   | STORAGE DEVICE 16GB          | 1     | uas, us... | 2CB78484FB |
| 3538:0054 | pqi        | Flash Drive (2GB) 2GB        | 1     | usb_sto... | 63C1955CF6 |
| 8644:8003 | Intenso... | Micro Line 16GB              | 1     | uas, us... | E9C3A73481 |

### Dvb card (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 07ca:1336 | AVerMed... | A336 MiniCard Hybrid DVB-T   | 5     |            | A2FF4426C9 |
| 048d:9135 | Integra... | Zolid Mini DVB-T Stick       | 1     | dvb_usb... | 973EEE4869 |
| 07ca:1334 | AVerMed... | H334 MiniCard Hybrid DVB-T   | 1     |            | 3EBFFF7DAD |
| 07ca:4336 | AVerMed... | A336 MiniCard Hybrid DVB-T   | 1     |            | 0C3E26BCE7 |
| 0bda:2838 | Realtek... | RTL2838 DVB-T                | 1     | dvb_usb... | EA55AE13AC |
| 13d3:3282 | IMC Net... | DVB-T + GPS Minicard [RTL... | 1     |            | 7F0F347502 |

### Gamepad (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:028e | Microsoft  | Xbox360 Controller           | 2     | xpad       | CB28DFB85B |
| 0e8f:0012 | GreenAsia  | Joystick/Gamepad             | 1     | usbhid     | EB1E63578D |

### Hasp (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0529:0001 | Aladdin... | HASP copy protection dongle  | 1     |            | 70DEC15340 |

### Hub (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1d6b:0002 | Linux F... | 2.0 root hub                 | 257   | hub        | B6D6A0F54D |
| 1d6b:0003 | Linux F... | 3.0 root hub                 | 168   | hub        | F41BD5BF5B |
| 1d6b:0001 | Linux F... | 1.1 root hub                 | 82    | hub        | B6D6A0F54D |
| 8087:0024 | Intel      | Integrated Rate Matching Hub | 59    | hub        | 59BB8EC907 |
| 0a5c:4500 | Broadcom   | BCM2046B1 USB 2.0 Hub (pa... | 46    | hub        | B6D6A0F54D |
| 05e3:0608 | Genesys... | USB-2.0 4-Port HUB           | 23    | hub        | 2E89DCF36E |
| 0424:2514 | Standar... | USB 2.0 Hub                  | 18    | hub        | 561B991E16 |
| 8087:8000 | Intel      | Hub                          | 16    | hub        | 3EBFFF7DAD |
| 8087:8008 | Intel      | Hub                          | 15    | hub        | 3EBFFF7DAD |
| 05e3:0610 | Genesys... | 4-port hub                   | 14    | hub        | 63741AC6DC |
| 0438:7900 | AMD        | Hub                          | 13    | hub        | 1571B0B373 |
| 05ac:1006 | Apple      | Hub in Aluminum Keyboard     | 13    | hub        | B6D6A0F54D |
| 1a40:0101 | incompl... | 4-Port HUB                   | 11    | hub        | 94BD16E6CB |
| 8087:0020 | Intel      | Integrated Rate Matching Hub | 9     | hub        | 7F0F347502 |
| 05ac:1003 | Apple      | Hub in Pro Keyboard [Mits... | 5     | hub        | F30DA90CB0 |
| 8087:8001 | Intel      | Hub                          | 4     | hub        | C95062D4FF |
| 0424:2412 | Standar... | Hub                          | 2     | hub        | E9C3A73481 |
| 0451:8140 | Texas I... | TUSB8041 4-Port Hub          | 2     | usbcore    | 2146FDE0CF |
| 0451:8142 | Texas I... | TUSB8041 4-Port Hub          | 2     | usbcore    | 2146FDE0CF |
| 05ac:1005 | Apple      | Keyboard Hub                 | 2     | hub        | B8314E6CBC |
| 05e3:0612 | Genesys... | USB3.0 Hub 123               | 2     | hub        | 4F5252C592 |
| 14cd:8601 | Super Top  | USB 2.0 Hub                  | 2     | hub        | B4AB9671B3 |
| 2109:2812 | VIA Labs   | USB2.0 VL812 Hub             | 2     | hub        | 9EE8211317 |
| 0409:005a | NEC Com... | HighSpeed Hub                | 1     | hub        | CE0D84E8C5 |
| 046d:0b04 | Logitech   | BT Mini-Receiver             | 1     | hub        | E5A38B8F80 |
| 04b3:301a | IBM        | 2-port low-power hub         | 1     | hub        | AC62725ABE |
| 050d:0237 | Belkin ... | F5U237 USB 2.0 7-Port Hub    | 1     | hub        | 3F13E4ABCB |
| 058f:6254 | Alcor M... | USB Hub                      | 1     | hub        | E745E03926 |
| 05e3:0604 | Genesys... | USB 1.1 Hub                  | 1     | hub        | F41BD5BF5B |
| 05e3:0606 | Genesys... | USB 2.0 Hub / D-Link DUB-... | 1     | hub        | 97FBC61E68 |
| 05e3:0617 | Genesys... | USB3.0 Hub                   | 1     | hub        | 97FBC61E68 |
| 0a05:7220 | Unknown... | Hub                          | 1     | hub        | 09FE5E4566 |
| 0bda:0411 | Realtek... | 4-Port USB 3.0 Hub           | 1     | hub        | 94BD16E6CB |
| 0bda:5411 | Realtek... | 4-Port USB 2.0 Hub           | 1     | hub        | 94BD16E6CB |
| 2101:8500 | ActionStar | USB2.0 Hub                   | 1     | hub        | D6B9184305 |
| 2109:0812 | VLI Labs   | USB 3.0 VL812 HUB            | 1     | hub        | 6F0288A609 |
| 214b:7250 |            | USB2.0 HUB                   | 1     | hub        | 74411E469D |
| 21d1:0010 |            | Hub                          | 1     | hub        | 05D6B79D2F |
| 8087:07e6 | Intel      | Hub                          | 1     | hub        | F14A7F7311 |

### Human interface (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 05ac:8242 | Apple      | Built-in IR Receiver         | 36    | usbhid     | B6D6A0F54D |
| 0408:3001 | Quanta ... | Optical Touch Screen         | 9     | usbhid     | A2FF4426C9 |
| 0483:5715 | STMicro... | RemoteSolution               | 5     | usbhid     | 857C8BE5D0 |
| 054c:058d | Sony       | USB Input Device             | 5     | usbhid     | 59BB8EC907 |
| 222a:0011 | ILITEK     | Multi-Touch                  | 4     | usbhid     | 16D6FCA3F1 |
| 05ac:8240 | Apple      | Built-in IR Receiver         | 3     | usbhid     | F30DA90CB0 |
| 1cb6:6651 | IdeaCom... | IDC 6651                     | 2     | usbhid     | 973EEE4869 |
| 2149:2402 | Advance... | CoolTouch System             | 2     | usbhid     | 7685534B8E |
| 222a:010e | ILITEK     | Multi-Touch-V5400            | 2     | usbhid     | 447E6C6392 |
| 054c:0406 | Sony       | IR Receiver                  | 1     | usbhid     | 2628EBE1BB |
| 0596:0537 | MicroTo... | 3M MicroTouch USB controller | 1     | usbhid     | 857C8BE5D0 |
| 05ac:1392 | Apple      | Watch Magnetic Charging C... | 1     | usbhid     | 881CDBC376 |
| 1fd2:6102 | Melfas     | LGD AIT Touch Controller     | 1     | usbhid     | D50551A34E |
| 2149:2316 | Advance... | CoolTouch System             | 1     | usbhid     | 3FEDAA45BD |
| 2149:5cf9 | Advance... | CoolTouch System             | 1     | usbhid     | 58DDD6F565 |
| 21d1:0001 | Adder T... | USB Free-Flow Mouse          | 1     | usbhid     | 05D6B79D2F |
| 222a:0129 | ILITEK     | Multi-Touch-V5100            | 1     | usbhid     | EC25F6A0A6 |

### Input/keyboard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 17ef:602d | Lenovo     | Black Silk USB Keyboard      | 20    | usbhid     | 0DBBF3578B |
| 046d:c52b | Logitech   | Unifying Receiver            | 18    | usbhid     | F41BD5BF5B |
| 046d:c534 | Logitech   | Unifying Receiver            | 10    | usbhid     | 30FF6DAB9F |
| 04ca:0050 | Lite-On... | USB Multimedia Keyboard      | 9     | usbhid     | BEDE9EC674 |
| 17ef:608c | Lenovo     | Lenovo Calliope USB Keyboard | 8     | usbhid     | C586E918F9 |
| 04ca:007d | Lite-On... | USB wired keyboard           | 7     | usbhid     | 21CA50C386 |
| 054c:04b8 | Sony       | RF Receiver                  | 7     | usbhid     | 59BB8EC907 |
| 413c:8505 | Dell       | Universal Receiver           | 7     | usbhid     | FF0EDA1857 |
| 0461:0010 | Primax ... | HP PR1101U / Primax PMX-K... | 6     | usbhid     | 94BD16E6CB |
| 046d:c52e | Logitech   | MK260 Wireless Combo Rece... | 5     | usbhid     | DB3BBECDD0 |
| 04d9:1702 | Holtek ... | Keyboard LKS02               | 5     | usbhid     | 78D39E18EF |
| 05ac:020c | Apple      | Extended Keyboard [Mitsumi]  | 5     | usbhid     | F30DA90CB0 |
| 062a:4101 | MosArt ... | Wireless Keyboard/Mouse      | 5     | usbhid     | B4AB9671B3 |
| 0a5c:4502 | Broadcom   | Keyboard (Boot Interface ... | 5     | usbhid     | B98E1F49EA |
| 1c4f:0002 | SiGma M... | Keyboard TRACER Gamma Ivory  | 5     | usbhid     | BB93D1DD97 |
| 04f2:0618 | Chicony... | RG-0618U Wireless HID Rec... | 4     | usbhid     | A2FF4426C9 |
| 04f3:0103 | Elan Mi... | ActiveJet K-2024 Multimed... | 4     | usbhid     | 0F82F36E74 |
| 05ac:0220 | Apple      | Aluminum Keyboard (ANSI)     | 4     | usbhid     | ADAC976045 |
| 05ac:0221 | Apple      | Aluminum Keyboard (ISO)      | 4     | usbhid     | 2EC70E7CEC |
| 0c45:760b | Microdia   | USB Keyboard                 | 4     | usbhid     | D1E3B9E1FB |
| 413c:2501 | Dell       | KM632 Wireless Keyboard a... | 4     | usbhid     | 229BA614EC |
| 045e:00dd | Microsoft  | Comfort Curve Keyboard 20... | 3     | usbhid     | BAA91679D8 |
| 045e:0745 | Microsoft  | Nano Transceiver v1.0 for... | 3     | usbhid     | E89226495D |
| 0461:4e67 | Primax ... | HP USB Multimedia Keyboard   | 3     | usbhid     | 7CD9296ED7 |
| 046d:c31c | Logitech   | Keyboard K120 for Business   | 3     | usbhid     | 2E89DCF36E |
| 04b3:3025 | IBM        | NetVista Full Width Keyboard | 3     | usbhid     | 1F501F049C |
| 04ca:0058 | Lite-On... | Wireless Device              | 3     | usbhid     | 114C724C7E |
| 04f2:0408 | Chicony... | USB-PS/2 Combo Keyboard      | 3     | usbhid     | 1EAD53F6C2 |
| 04f2:0860 | Chicony... | 2.4G Multimedia Wireless Kit | 3     | usbhid     | AADAAE6450 |
| 04f2:1123 | Chicony... | Wireless Device              | 3     | usbhid     | C8D8836613 |
| 054c:0688 | Sony       | RF Receiver                  | 3     | usbhid     | C486FF05F5 |
| 05ac:0250 | Apple      | Aluminium Keyboard (ISO)     | 3     | usbhid     | B6D6A0F54D |
| 09da:054f | A4Tech     | USB Device                   | 3     | usbhid     | 4279D783B3 |
| 17ef:6018 | Lenovo     | Low Profile USB Keyboard     | 3     | usbhid     | C64BB03524 |
| 248a:8566 | Maxxter    | SVEN RX 360 Art Wireless ... | 3     | usbhid     | 0FFE695213 |
| 03f0:064a | Hewlett... | USB Keyboard                 | 2     | usbhid     | 68246E58C1 |
| 045e:078c | Microsoft  | USB Keyboard                 | 2     | usbhid     | B0C04D42A3 |
| 04ca:004b | Lite-On... | Keyboard                     | 2     | usbhid     | F01E3C859D |
| 04ca:008d | Lite-On... | HP Wireless Keyboard Kit     | 2     | usbhid     | 447E6C6392 |
| 04e8:70e1 | Samsung... | Wireless Combo               | 2     | usbhid     | 2FAFA2B1CE |
| 04f2:0116 | Chicony... | KU-2971/KU-0325 Keyboard     | 2     | usbhid     | 81CF385125 |
| 04f2:0402 | Chicony... | Genius LuxeMate i200 Keyb... | 2     | usbhid     | 1F0CFAF4F6 |
| 04f2:0976 | Chicony... | Wireless Device              | 2     | usbhid     | 0B7EB6DDFC |
| 04f2:1236 | Chicony... | USB Receiver                 | 2     | usbhid     | 4313964FA7 |
| 054c:0374 | Sony       | RF Receiver                  | 2     | usbhid     | 2628EBE1BB |
| 05ac:024f | Apple      | Keyboard                     | 2     | usbhid     | 985A14858E |
| 09da:9066 | A4Tech     | F3 V-Track Gaming Mouse      | 2     | usbhid     | 60A7B5DDD7 |
| 09da:9090 | A4Tech     | XL-730K / XL-750BK / XL-7... | 2     | usbhid     | 0633624824 |
| 17ef:6032 | Lenovo     | Lenovo silver silk (wirel... | 2     | usbhid     | 3EBFFF7DAD |
| 1a2c:0e24 | China R... | USB Keyboard                 | 2     | usbhid     | 265BA34E71 |
| 1a2c:2124 | China R... | USB Keyboard                 | 2     | usbhid     | E9C3A73481 |
| 1a81:1004 | Holtek ... | Wireless Dongle              | 2     | usbhid     | C95062D4FF |
| 248a:00da | Maxxter    | Wireless Receiver            | 2     | usbhid     | 45679FA2F6 |
| 248a:ff0f | Maxxter    | Wireless Receiver            | 2     | usbhid     | D07D321CCE |
| 258a:0001 | SINO WE... | USB KEYBOARD                 | 2     | usbhid     | D6B9184305 |
| 413c:2003 | Dell       | Keyboard                     | 2     | usbhid     | 561B991E16 |
| 413c:2107 | Dell       | USB Entry Keyboard           | 2     | usbhid     | 7CC5A36861 |
| 413c:2113 | Dell       | KB216 Wired Keyboard         | 2     | usbhid     | D07D321CCE |
| 03f0:2b4a | Hewlett... | USB Slim Keyboard - Skyla... | 1     | usbhid     | 2281502A3C |
| 0409:0204 | NEC Com... | Wireless KB/MS               | 1     | usbhid     | 86771347FB |
| 040b:2000 | Weltren... | USB Keyboard                 | 1     | usbhid     | 7BD55BD71D |
| 045e:0730 | Microsoft  | Digital Media Keyboard 3000  | 1     | usbhid     | C156F0AF59 |
| 045e:0734 | Microsoft  | Wireless Optical Desktop 700 | 1     | usbhid     | 237C6D5B2C |
| 045e:0748 | Microsoft  | Microsoft SideWinder 2.4G... | 1     | usbhid     | C156F0AF59 |
| 045e:0768 | Microsoft  | Sidewinder X4                | 1     | usbhid     | 0C3E26BCE7 |
| 045e:0773 | Microsoft  | Microsoft Nano Transceive... | 1     | usbhid     | 39D4FD28B4 |
| 045e:0780 | Microsoft  | Comfort Curve Keyboard 3000  | 1     | usbhid     | 8DDD1670A8 |
| 045e:07b6 | Microsoft  | Comfort Curve Keyboard 3000  | 1     | usbhid     | 8553F899D5 |
| 045e:07b9 | Microsoft  | Wired Keyboard 200           | 1     | usbhid     | A2CB18D2E5 |
| 046a:0011 | Cherry     | G83 (RS 6000) Keyboard       | 1     | usbhid     | D77183679A |
| 046a:b090 | Cherry     | Keyboard                     | 1     | usbhid     | 8A67730AF4 |
| 046d:c22d | Logitech   | G510 Gaming Keyboard         | 1     | usbhid     | 602E2C649E |
| 046d:c24b | Logitech   | Gaming Keyboard G103         | 1     | usbhid     | 0633624824 |
| 046d:c33a | Logitech   | G413 Carbon Mechanical Ga... | 1     | usbhid     | 7F1ABC90BD |
| 046d:c517 | Logitech   | LX710 Cordless Desktop Laser | 1     | usbhid     | 5F28079AA4 |
| 046d:c713 | Logitech   | BT Mini-Receiver             | 1     | usbhid     | E5A38B8F80 |
| 04b3:301b | IBM        | SK-8815 Keyboard             | 1     | usbhid     | AC62725ABE |
| 04ca:0027 | Lite-On... | USB Multimedia Keyboard      | 1     | usbhid     | 2CB78484FB |
| 04ca:005a | Lite-On... | USB Multimedia Keyboard      | 1     | usbhid     | 3D1389215D |
| 04d9:0006 | Holtek ... | RPI Wired Keyboard 1         | 1     | usbhid     | 63741AC6DC |
| 04d9:1503 | Holtek ... | Shortboard Lefty             | 1     | usbhid     | B9D07EAAFE |
| 04d9:1603 | Holtek ... | Keyboard                     | 1     | usbhid     | C1A91BB2D3 |
| 04d9:2323 | Holtek ... | Keyboard                     | 1     | usbhid     | D5C9D589F2 |
| 04d9:a06b | Holtek ... | Wireless USB Device          | 1     | usbhid     | 5C64D33462 |
| 05ac:021d | Apple      | Aluminum Mini Keyboard (A... | 1     | usbhid     | A68B8E8206 |
| 05ac:021e | Apple      | Aluminum Mini Keyboard (ISO) | 1     | usbhid     | B8314E6CBC |
| 05ac:0267 | Apple      | Magic Keyboard A1644         | 1     | usbhid     | 9EE8211317 |
| 05ac:820a | Apple      | Bluetooth HID Keyboard       | 1     | usbhid     | D5C9D589F2 |
| 062a:0201 | MosArt ... | Defender Office Keyboard ... | 1     | usbhid     | FC94E24E2F |
| 09da:024f | A4Tech     | RF Receiver and G6-20D Wi... | 1     | usbhid     | 2AB277563F |
| 09da:0260 | A4Tech     | KV-300H Isolation Keyboard   | 1     | usbhid     | E4BDC45558 |
| 09da:f012 | A4Tech     | USB Device                   | 1     | usbhid     | 1317E04C8F |
| 0a81:0205 | Chesen ... | PS/2 Keyboard+Mouse Adapter  | 1     | usbhid     | 0FC1178D7D |
| 0bf8:1004 | Fujitsu... | FSC KB USB                   | 1     | usbhid     | 66F152CA82 |
| 0bf8:100c | Fujitsu... | Keyboard FSC KBPC PX         | 1     | usbhid     | 2EC70E7CEC |
| 0c45:5011 | Microdia   | USB Wired Keyboard           | 1     | usbhid     | D50551A34E |
| 0c45:5112 | Microdia   | ASUS ZEN AIO WIRED KEYBOARD  | 1     | usbhid     | EE7F588C0D |
| 0c45:7603 | Microdia   | USB Keyboard                 | 1     | usbhid     | E033B6FBEA |
| 0c45:7702 | Microdia   | USB Keyboard                 | 1     | usbhid     | EA55AE13AC |
| 0e8f:0021 | GreenAsia  | Multimedia Keyboard Contr... | 1     | usbhid     | 17AD880F72 |

### Input/mouse (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 093a:2510 | Pixart ... | Optical Mouse                | 14    | usbhid     | 63741AC6DC |
| 0458:003a | KYE Sys... | NetScroll+ Mini Traveler ... | 13    | usbhid     | BAA91679D8 |
| 17ef:602e | Lenovo     | USB Optical Mouse            | 13    | usbhid     | F1FDC67882 |
| 17ef:608d | Lenovo     | lenovo USB Optical Mouse     | 7     | usbhid     | C586E918F9 |
| 0461:4d0f | Primax ... | HP Optical Mouse             | 6     | usbhid     | 94BD16E6CB |
| 04ca:008a | Lite-On... | USB Optical Mouse            | 6     | usbhid     | 21CA50C386 |
| 046d:c05a | Logitech   | M90/M100 Optical Mouse       | 5     | usbhid     | 4CA7976A88 |
| 05ac:0304 | Apple      | Mighty Mouse [Mitsumi, M1... | 5     | usbhid     | A68B8E8206 |
| 093a:2521 | Pixart ... | Optical Mouse                | 5     | usbhid     | 46E2D4B676 |
| 0a5c:4503 | Broadcom   | Mouse (Boot Interface Sub... | 5     | usbhid     | B98E1F49EA |
| 275d:0ba6 |            | USB OPTICAL MOUSE            | 5     | usbhid     | E9C3A73481 |
| 0461:4d64 | Primax ... | USB Optical Mouse            | 4     | usbhid     | 0F82F36E74 |
| 046d:c077 | Logitech   | M105 Optical Mouse           | 4     | usbhid     | 1E17CF76B7 |
| 0000:0538 |            | USB OPTICAL MOUSE            | 3     | usbhid     | CB28DFB85B |
| 0461:4e66 | Primax ... | HP USB Optical Mouse         | 3     | usbhid     | 7CD9296ED7 |
| 046d:c062 | Logitech   | M-UAS144 [LS1 Laser Mouse]   | 3     | usbhid     | 196FDD9B5C |
| 062a:5110 | MosArt ... | ASUS Zen AIO KB/MS/RF/GOLD   | 3     | usbhid     | 0FF0BDD7C8 |
| 09da:000a | A4Tech     | Optical Mouse Opto 510D /... | 3     | usbhid     | DEABFE7945 |
| 1cb6:6672 | IdeaCom... | IDC 6672                     | 3     | usbhid     | 4CA7976A88 |
| 2149:230a | Advance... | CoolTouch System             | 3     | usbhid     | 98B3AC895B |
| 03f0:134a | Hewlett... | Optical Mouse                | 2     | usbhid     | AA4322078C |
| 0408:3000 | Quanta ... | Optical dual-touch panel     | 2     | usbhid     | 57AD94D04A |
| 0458:0185 | KYE Sys... | Wireless Mouse               | 2     | usbhid     | BEDE9EC674 |
| 0461:4d80 | Primax ... | USB Optical Mouse            | 2     | usbhid     | 55C2F8E26B |
| 046d:c05b | Logitech   | M-U0004 810-001317 [B110 ... | 2     | usbhid     | C7340EFD78 |
| 046d:c52f | Logitech   | Unifying Receiver            | 2     | usbhid     | 9EE8211317 |
| 04e7:0022 | Elo Tou... | 2515 IntelliTouch Plus US... | 2     | usbhid     | EA55AE13AC |
| 05af:0808 | Jing-Mo... | USB K/B+Mouse                | 2     | usbhid     | 394B9426A3 |
| 0e8f:00fb | GreenAsia  | USB Mouse                    | 2     | usbhid     | 582AB9032D |
| 17ef:6019 | Lenovo     | Lenovo USB Optical Mouse     | 2     | usbhid     | 63FB881055 |
| 1a2c:0042 | China R... | Usb Mouse                    | 2     | usbhid     | D3734872B2 |
| 2149:2324 | Advance... | CoolTouch System             | 2     | usbhid     | 3EBFFF7DAD |
| 248a:8366 | Maxxter    | Wireless Optical Mouse AC... | 2     | usbhid     | 2B0CDC9F2B |
| 276d:1160 | YSTEK      | G Mouse                      | 2     | usbhid     | A7DAA7D955 |
| 413c:301a | Dell       | MS116 USB Optical Mouse      | 2     | usbhid     | D50551A34E |
| 03f0:2c24 | Hewlett... | Logitech M-UAL-96 Mouse      | 1     | usbhid     | 68246E58C1 |
| 0408:3027 | Quanta ... | HID-TQH-FS                   | 1     | usbhid     | 8553F899D5 |
| 0457:104e | Silicon... | SiS HID Touch Controller     | 1     | usbhid     | 3321732E8E |
| 0458:0031 | KYE Sys... | NetScroll + Optical          | 1     | usbhid     | 47822D532C |
| 045e:0040 | Microsoft  | Wheel Mouse Optical          | 1     | usbhid     | 2CB78484FB |
| 045e:077b | Microsoft  | Comfort Mouse 3000           | 1     | usbhid     | 8DDD1670A8 |
| 045e:0782 | Microsoft  | USB Optical Mouse            | 1     | usbhid     | 24C0C3F829 |
| 045e:0797 | Microsoft  | Optical Mouse 200            | 1     | usbhid     | A2CB18D2E5 |
| 0461:4d15 | Primax ... | Dell Optical Mouse           | 1     | usbhid     | 1F501F049C |
| 0461:4d22 | Primax ... | USB Optical Mouse            | 1     | usbhid     | 66F152CA82 |
| 0461:4d65 | Primax ... | USB Optical Mouse            | 1     | usbhid     | 2E89DCF36E |
| 0461:4e70 | Primax ... | USB Optical Mouse            | 1     | usbhid     | E7395D0EE2 |
| 0461:4e90 | Primax ... | HP Wireless Keyboard Mous... | 1     | usbhid     | 8269929E09 |
| 046d:c00c | Logitech   | Optical Wheel Mouse          | 1     | usbhid     | 499EA7DFBB |
| 046d:c016 | Logitech   | Optical Wheel Mouse          | 1     | usbhid     | 2EC70E7CEC |
| 046d:c03d | Logitech   | M-BT96a Pilot Optical Mouse  | 1     | usbhid     | 95D54FAC68 |
| 046d:c03f | Logitech   | M-BT85 [UltraX Optical Mo... | 1     | usbhid     | 68671C6971 |
| 046d:c040 | Logitech   | Corded Tilt-Wheel Mouse      | 1     | usbhid     | 1CCB72AD4C |
| 046d:c045 | Logitech   | Optical Mouse                | 1     | usbhid     | A629A81791 |
| 046d:c046 | Logitech   | RX1000 Laser Mouse           | 1     | usbhid     | D77183679A |
| 046d:c055 | Logitech   | USB Optical Mouse            | 1     | usbhid     | D5C9D589F2 |
| 046d:c063 | Logitech   | DELL Laser Mouse             | 1     | usbhid     | 0776B86016 |
| 046d:c06c | Logitech   | Optical Mouse                | 1     | usbhid     | 1112BF63F0 |
| 046d:c07d | Logitech   | Gaming Mouse G502            | 1     | usbhid     | 3F13E4ABCB |
| 046d:c084 | Logitech   | G102 Prodigy Gaming Mouse    | 1     | usbhid     | 30FF6DAB9F |
| 046d:c526 | Logitech   | Nano Receiver                | 1     | usbhid     | B4AB9671B3 |
| 046d:c714 | Logitech   | diNovo Edge Keyboard         | 1     | usbhid     | E5A38B8F80 |
| 04d9:fa52 | Holtek ... | USB gaming mouse             | 1     | usbhid     | E033B6FBEA |
| 04f2:0939 | Chicony... | USB Optical Mouse            | 1     | usbhid     | 3DB357276B |
| 04f2:1117 | Chicony... | USB Optical Mouse            | 1     | usbhid     | 1EAD53F6C2 |
| 04f3:0232 | Elan Mi... | Mouse                        | 1     | usbhid     | B6D6A0F54D |
| 056a:0010 | Wacom      | ET-0405 [Graphire]           | 1     | usbhid     | A7CE72E3F1 |
| 056a:00b1 | Wacom      | PTZ-630 [Intuos3 (6x8)]      | 1     | usbhid     | BEF4BEA5F8 |
| 05ac:820b | Apple      | Bluetooth HID Mouse          | 1     | usbhid     | D5C9D589F2 |
| 093a:2516 | Pixart ... | USB OPTICAL MOUSE            | 1     | usbhid     | F744394A1C |
| 093a:5112 | Pixart ... | ASUS AIO USB Optical Mouse   | 1     | usbhid     | EE7F588C0D |
| 09da:3809 | A4Tech     | USB Device                   | 1     | usbhid     | 881CDBC376 |
| 09da:8090 | A4Tech     | X-718BK Oscar Optical Gam... | 1     | usbhid     | 57AD94D04A |
| 09da:c10a | A4Tech     | USB Mouse                    | 1     | usbhid     | 16D6FCA3F1 |
| 10c4:8105 | Cygnal ... | USB OPTICAL MOUSE            | 1     | usbhid     | 17AD880F72 |
| 1267:0210 | Logic3 ... | LG Optical Mouse 3D-310      | 1     | usbhid     | 5DC25F1C8F |
| 13ee:0001 | MosArt     | Optical Mouse                | 1     | usbhid     | ADD51B1FFA |
| 145f:01d9 | Trust      | GXT 101 Gaming Mouse         | 1     | usbhid     | 0DBBF3578B |
| 145f:022a | Trust      | Keyboard & Mouse             | 1     | usbhid     | 2B0CDC9F2B |
| 15d9:0a4c | Trust I... | USB+PS/2 Optical Mouse       | 1     | usbhid     | 8A67730AF4 |
| 15d9:0a4f | Trust I... | Optical Mouse                | 1     | usbhid     | C3AA8C3D72 |
| 1bcf:0005 | Sunplus... | USB Optical Mouse            | 1     | usbhid     | DFFA21B8C5 |
| 1c4f:0034 | SiGma M... | Usb Mouse                    | 1     | usbhid     | 561B991E16 |
| 1cb6:6650 | IdeaCom... | IDC 6650                     | 1     | usbhid     | C7340EFD78 |
| 1cb6:6673 | IdeaCom... | IDC 6673                     | 1     | usbhid     | 114C724C7E |
| 1d57:0006 | Xenta      | Wireless Receiver (RC Las... | 1     | usbhid     | 4368B7F597 |
| 2149:2330 | Advance... | CoolTouch System             | 1     | usbhid     | C1A91BB2D3 |
| 2149:5d00 | Advance... | CoolTouchR System            | 1     | usbhid     | 2EB7BFC64B |
| 222a:0001 | ILITEK     | Multi-Touch                  | 1     | usbhid     | 0C3E26BCE7 |
| 248a:8367 | Maxxter    | SVEN Comfort 3400 Wireless   | 1     | usbhid     | 5F116B16F1 |
| 248a:8514 | Maxxter    | Wireless Receiver            | 1     | usbhid     | BB93D1DD97 |
| 2733:ffa8 |            | Game mouse                   | 1     | usbhid     | 8F9192D6D5 |
| 3938:1031 | MOSART ... | 2.4G Wireless Mouse          | 1     | usbhid     | 7BD55BD71D |
| 413c:2506 | Dell       | KM713 Wireless Keyboard a... | 1     | usbhid     | 3FEDAA45BD |
| 5543:0081 | UC-Logi... | TABLET 1060N                 | 1     | usbhid     | 689F9F13AF |
| ffc0:0040 | Mouse      | USB Laser Mouse              | 1     | usbhid     | 0C3E26BCE7 |

### Isdn adapter (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| ffff:ffff | RAPOO      | AVM FRITZ!Card PCMCIA        | 1     | usbhid     | B8314E6CBC |

### Mfp (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:042a | Hewlett... | LaserJet M1132 MFP           | 1     | uas, us... | BAA91679D8 |
| 03f0:3b17 | Hewlett... | LaserJet M1005 MFP           | 1     | usblp      | EA55AE13AC |

### Modem (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 12d1:1506 | Huawei ... | E398 LTE/UMTS/GSM Modem/N... | 2     | uas, us... | 2B0CDC9F2B |
| 0483:5740 | STMicro... | STM32F407                    | 1     | cdc_acm    | 229BA614EC |
| 12d1:1003 | Huawei ... | E220 HSDPA Modem / E230/E... | 1     | usb_sto... | 807B68E120 |

### Net/wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:8172 | Realtek... | RTL8191SU 802.11n WLAN Ad... | 11    | r8712u     | 4CA7976A88 |
| 0bda:8178 | Realtek... | RTL8192CU 802.11n WLAN Ad... | 2     | rtl8192cu  | 89E79AAB3C |
| 0cf3:9271 | Qualcom... | AR9271 802.11n               | 2     | ath9k_htc  | 0776B86016 |
| 2357:0105 | MediaTek   | MT7610U Archer T1U 802.11... | 2     |            | DB3BBECDD0 |
| 050d:2103 | Belkin ... | F7D2102 802.11n N300 Micr... | 1     | rtl8192... | 8A67730AF4 |
| 050d:825b | Belkin ... | F5D8055 N+ Wireless Adapt... | 1     | rt2800usb  | 237C6D5B2C |
| 0b05:1786 | ASUSTek... | USB-N10 802.11n Network A... | 1     | r8712u     | DD5E0979B0 |
| 0bda:8179 | Realtek... | RTL8188EUS 802.11n Wirele... | 1     | r8188eu    | 90FF435FAD |
| 0bda:a811 | Realtek... | RTL8811AU 802.11a/b/g/n/a... | 1     |            | B4AB9671B3 |
| 0df6:006b | Sitecom... | RTL8188S WLAN Adapter        | 1     | r8712u     | EB1E63578D |
| 148f:3070 | Ralink ... | RT2870/RT3070 Wireless Ad... | 1     | rt2800usb  | 97FBC61E68 |
| 148f:5370 | Ralink ... | RT5370 Wireless Adapter      | 1     | rt2800usb  | 5DC25F1C8F |
| 2001:3314 | D-Link     | RTL8821AU DWA-171 802.11n... | 1     |            | 0413D1A17F |
| 2357:010c | Realtek    | RTL8188EUS TL-WN722N v2      | 1     | r8188eu    | 9704A885AC |

### Network (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0489:c022 | Foxconn... | Nokia 8                      | 2     | rndis_host | 8C18BA014C |
| 04e8:6863 | Samsung... | GT-I9500 [Galaxy S4] / GT... | 1     | rndis_host | B6D6A0F54D |
| 0b95:1790 | ASIX El... | AX88179 Gigabit Ethernet     | 1     | ax88179... | F41BD5BF5B |
| 0bda:b720 | Realtek... | RTL8723BU 802.11n WLAN Ad... | 1     | rtl8xxxu   | 8A67730AF4 |
| 0fe6:9700 | Kontron... | DM9601 Fast Ethernet Adapter | 1     | dm9601,... | 90FF435FAD |
| 1376:4e61 | Vimtron... | Mobile Composite Device Bus  | 1     | rndis_host | 09FE5E4566 |
| 17ef:7436 | Lenovo     | MT65xx Android Phone         | 1     | rndis_host | E7395D0EE2 |

### Printer (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:0853 | Hewlett... | ENVY 5000 series             | 1     | usblp      | 934BF6308F |
| 03f0:0c17 | Hewlett... | LaserJet 1010                | 1     | usblp      | 74411E469D |
| 03f0:2504 | Hewlett... | DeskJet F4200 series         | 1     | usblp      | 714EFD4954 |
| 03f0:7611 | Hewlett... | DeskJet F2492 All-in-One     | 1     | usblp      | 7F0F347502 |
| 03f0:8604 | Hewlett... | DeskJet 5440                 | 1     | usblp      | 86E8BBC133 |
| 03f0:8911 | Hewlett... | Deskjet 1050 J410 series     | 1     | usblp      | 58A24F957D |
| 03f0:d911 | Hewlett... | OfficeJet 4650 series        | 1     | usblp      | 5F28079AA4 |
| 0482:0496 | Kyocera    | FS-1120MFP                   | 1     | usblp      | CBD910EA06 |
| 04b8:08a1 | Seiko E... | L210 Series                  | 1     | usblp      | A11E971A0C |
| 04b8:08a8 | Seiko E... | L355 Series                  | 1     | usblp      | F92CB57F54 |
| 04e8:341b | Samsung... | SCX-4200 series              | 1     | usblp      | 098D8022D0 |
| 04e8:3469 | Samsung... | M2070 Series                 | 1     | usblp      | E1331356C7 |
| 04f9:01ba | Brother... | MFC-3360C                    | 1     | usblp      | 582AB9032D |
| 04f9:0331 | Brother... | MFC-L2700DW                  | 1     | usblp      | C586E918F9 |
| 04f9:0384 | Brother... | MFC-J480DW                   | 1     | usblp      | E033B6FBEA |
| 05ca:0424 | Ricoh      | Printer                      | 1     | usblp      | 176B216E44 |
| 05ca:042b | Ricoh      | Aficio SP 100                | 1     | usblp      | 797B5C4507 |
| 05ca:042c | Ricoh      | Aficio SP 100SU              | 1     | usblp      | A629A81791 |

### Scanner (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:4205 | Hewlett... | ScanJet G3010                | 1     |            | 3D1D05A4CC |
| 04a9:220d | Canon      | CanoScan N670U/N676U/LiDE 20 | 1     |            | 39F18E2183 |
| 04a9:221c | Canon      | CanoScan LiDE 60             | 1     |            | FE0F4C11DF |
| 04a9:2220 | Canon      | CanoScan LIDE 25             | 1     |            | 86E8BBC133 |

### Serial controller (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1a86:7523 | QinHeng... | HL-340 USB-Serial adapter    | 1     | ch341      | C215749CAB |

### Sound (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 05e1:0408 | Syntek     | STK1160 Video Capture Device | 1     | stk1160    | 3EBFFF7DAD |
| 0d8c:0102 | C-Media... | CM106 Like Sound Device      | 1     | snd_usb... | 74411E469D |

### Touchscreen (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1926:0bce | NextWindow | Touchscreen                  | 5     | usbhid     | 59BB8EC907 |
| 0408:3008 | Quanta ... | OpticalTouchScreen           | 4     | usbhid     | 2E89DCF36E |
| 0408:3003 | Quanta ... | OpticalTouchScreen           | 2     | usbhid     | D44BEF08F5 |
| 1926:0083 | NextWindow | 1950 HID Touchscreen         | 2     | usbhid     | A7CE72E3F1 |
| 1926:0330 | NextWindow | Touchscreen                  | 2     | usbhid     | BAA91679D8 |
| 1926:0e17 | NextWindow | Touchscreen                  | 2     | usbhid     | DE93F78B0C |
| 0408:3033 | Quanta ... | OpticalTouchScreen           | 1     | usbhid     | 0B7EB6DDFC |
| 0eef:0001 | D-WAV S... | eGalax TouchScreen           | 1     | usbhid     | 1EAD53F6C2 |
| 1926:0065 | NextWindow | 1950 HID Touchscreen         | 1     | usbhid     | F3D7A20E22 |
| 1926:0332 | NextWindow | Touchscreen                  | 1     | usbhid     | 6F0288A609 |
| 1926:0344 | NextWindow | Touchscreen                  | 1     | usbhid     | 45679FA2F6 |
| 1926:1847 | NextWindow | Touchscreen                  | 1     | usbhid     | DB3BBECDD0 |
| 2149:211b | Advance... | Touchscreen Controller       | 1     | usbhid     | C8D8836613 |
| 6615:0087 | IRTOUCH... | USB TouchScreen              | 1     |            | F14A7F7311 |

### Tv card (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:00f7 | Microsoft  | LifeCam VX-1000              | 1     | gspca_s... | 0218F8BD43 |

### Ups (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0764:0501 | Cyber P... | CP1500 AVR UPS               | 2     | usbhid     | 1BD0896C15 |

### Others (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1164:7f07 | YUAN Hi... | STK7700D                     | 5     |            | 857C8BE5D0 |
| 05ac:8300 | Apple      | Built-in iSight (no firmw... | 2     | isight_... | F30DA90CB0 |
| 0471:2093 | Philips... | Windows Media Center IR T... | 1     | mceusb     | EA55AE13AC |
| 054c:02e1 | Sony       | FeliCa S330 [PaSoRi]         | 1     | pn533_usb  | 2628EBE1BB |
| 07ca:a373 | AVerMed... | A373                         | 1     |            | 114C724C7E |
| 0846:4240 | NetGear    | WG111(v1) rev 2 54 Mbps W... | 1     |            | 394B9426A3 |
| 1164:3edc | YUAN Hi... | STK7700D                     | 1     |            | A7CE72E3F1 |
| 1b24:4001 | Telegen... | Bootloader USB               | 1     |            | E4BDC45558 |
| 1b80:c880 | Afatech    | Rtl2832UDVB                  | 1     |            | C8D8836613 |
| 2040:0265 | Hauppauge  | dualHD                       | 1     | em28xx     | 8269929E09 |
| 22b8:2e82 | Motorol... | Moto Z (2)                   | 1     |            | 3DB357276B |
| 2488:0000 |            | SuperD SPD2900GS USB Driver  | 1     |            | 81CEB684C0 |

