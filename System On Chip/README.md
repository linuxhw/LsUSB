Most popular USB devices in System On Chips
===========================================

See more info in the [README](https://github.com/linuxhw/LsUSB).

Contents
--------

1. [ USB Devices in System On Chips ](#usb-devices)
   * [ Bluetooth ](#bluetooth-usb)
   * [ Camera ](#camera-usb)
   * [ Cdrom ](#cdrom-usb)
   * [ Disk ](#disk-usb)
   * [ Hub ](#hub-usb)
   * [ Input/keyboard ](#inputkeyboard-usb)
   * [ Input/mouse ](#inputmouse-usb)
   * [ Net/wireless ](#netwireless-usb)
   * [ Network ](#network-usb)
   * [ Printer ](#printer-usb)
   * [ Touchscreen ](#touchscreen-usb)

USB Devices
-----------

Count  — number of computers with this device installed,
Driver — driver in use for this device,
Probe  — latest probe ID of this device.

### Bluetooth (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04ca:3015 | Lite-On... | Lite-On Bluetooth Device     | 1     | btusb      | 5E682A0733 |
| 8087:0a2b | Intel      | Bluetooth Device             | 1     | btusb      | 5458E9A8B8 |

### Camera (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:082d | Logitech   | HD Pro Webcam C920           | 1     | uvcvideo   | 0F6BF93DE7 |

### Cdrom (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 152d:0578 | JMicron... | JMS567 SATA 6Gb/s bridge     | 2     | uas        | C4D5FA2F23 |
| 152d:0562 | JMicron... | MK2035GSS                    | 1     | uas        | 97C0D626A3 |

### Disk (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0781:5530 | SanDisk    | Cruzer                       | 1     | usb_sto... | 5E682A0733 |
| 07ab:fc98 | Freecom... | rive II 250GB                | 1     | usb_sto... | A2AB91716A |
| 0951:16a3 | Kingsto... | DT microDuo 3.0 16GB         | 1     | usb_sto... | 72113080A2 |
| 152d:2509 | JMicron... | JMS539 SuperSpeed SATA II... | 1     | usb_sto... | 61DE4E9C89 |
| 174c:5136 | ASMedia... | ASM1053 SATA 6Gb/s bridge    | 1     | usb_sto... | 61DE4E9C89 |

### Hub (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1d6b:0002 | Linux F... | 2.0 root hub                 | 22    | hub        | A2AB91716A |
| 0424:9514 | Standar... | SMC9514 Hub                  | 10    | hub        | A2AB91716A |
| 1d6b:0001 | Linux F... | 1.1 root hub                 | 5     | hub        | C4D5FA2F23 |
| 1d6b:0003 | Linux F... | 3.0 root hub                 | 5     | hub        | 97C0D626A3 |
| 0424:2514 | Standar... | USB 2.0 Hub                  | 4     | hub        | 66A253B82B |
| 0bda:0411 | Realtek... | 4-Port USB 3.0 Hub           | 3     | hub        | 97C0D626A3 |
| 0bda:5411 | Realtek... | 4-Port USB 2.0 Hub           | 3     | hub        | 97C0D626A3 |
| 05e3:0610 | Genesys... | 4-port hub                   | 2     | hub        | C4D5FA2F23 |
| 05e3:0616 | Genesys... | hub                          | 2     | hub        | C4D5FA2F23 |
| 05e3:0608 | Genesys... | USB-2.0 4-Port HUB           | 1     | hub        | 61DE4E9C89 |
| 2001:f103 | D-Link     | DUB-H7 7-port USB 2.0 hub    | 1     | hub        | 7A64B606B6 |
| 2109:2811 | VIA Labs   | USB2.0 Hub                   | 1     | hub        | 97C0D626A3 |
| 2109:8110 | VIA Labs   | USB 3.0 Hub                  | 1     | hub        | 97C0D626A3 |
| 8564:4100 | Transcend  | USB2.0 Hub                   | 1     | hub        | 2BA95DA510 |

### Input/keyboard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:0024 | Hewlett... | KU-0316 Keyboard             | 1     | usbhid     | FABEFC2647 |
| 045e:07f8 | Microsoft  | Wired Keyboard 600 (model... | 1     | usbhid     | 97C0D626A3 |
| 0461:0010 | Primax ... | HP PR1101U / Primax PMX-K... | 1     | usbhid     | 5E682A0733 |
| 046d:c534 | Logitech   | Unifying Receiver            | 1     | usbhid     | 6873B9B896 |
| 0c45:7605 | Microdia   | USB Keyboard                 | 1     | usbhid     | D590264685 |
| 17ef:6018 | Lenovo     | Low Profile USB Keyboard     | 1     | usbhid     | EF679BF628 |
| 1a2c:2124 | China R... | USB Keyboard                 | 1     | usbhid     | 7A64B606B6 |
| 1a2c:2d23 | China R... | USB Keyboard                 | 1     | usbhid     | 0F6BF93DE7 |
| 1c4f:0026 | SiGma M... | Keyboard                     | 1     | usbhid     | 2BA95DA510 |

### Input/mouse (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:076c | Microsoft  | Comfort Mouse 4500           | 1     | usbhid     | 97C0D626A3 |
| 0461:4d65 | Primax ... | USB Optical Mouse            | 1     | usbhid     | 5E682A0733 |
| 046d:c050 | Logitech   | RX 250 Optical Mouse         | 1     | usbhid     | 2BA95DA510 |
| 046d:c077 | Logitech   | M105 Optical Mouse           | 1     | usbhid     | 7A64B606B6 |
| 093a:2521 | Pixart ... | Optical Mouse                | 1     | usbhid     | D590264685 |
| 1915:1028 | Nordic ... | Smart Control                | 1     | usbhid     | 5458E9A8B8 |

### Net/wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0a5c:bd1e | Broadcom   | BCM43143 802.11bgn (1x1) ... | 1     | brcmfmac   | 7A64B606B6 |
| 0bda:8179 | Realtek... | RTL8188EUS 802.11n Wirele... | 1     | r8188eu    | 72113080A2 |

### Network (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0424:ec00 | Standar... | SMSC9512/9514 Fast Ethern... | 10    | smsc95xx   | A2AB91716A |
| 0424:7800 | Standar... | Ethernet controller          | 4     | lan78xx    | 66A253B82B |
| 0bda:8153 | Realtek... | RTL8153 Gigabit Ethernet ... | 2     | r8152      | C4D5FA2F23 |
| 0e8d:2005 | MediaTek   | X5max_PRO                    | 1     | rndis_host | 5E682A0733 |
| 1410:b00b | Novatel... | MiFi 5510                    | 1     | rndis_host | 6873B9B896 |

### Printer (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04f9:0273 | Brother... | DCP-7057 scanner/printer     | 1     | usblp      | 79E8256BFB |

### Touchscreen (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0eef:0001 | D-WAV S... | eGalax TouchScreen           | 1     | usbhid     | 0F6BF93DE7 |

