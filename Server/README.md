Most popular USB devices in Servers
===================================

See more info in the [README](https://github.com/linuxhw/LsUSB).

Contents
--------

1. [ USB Devices in Servers ](#usb-devices)
   * [ Audio ](#audio-usb)
   * [ Bluetooth ](#bluetooth-usb)
   * [ Camera ](#camera-usb)
   * [ Card reader ](#card-reader-usb)
   * [ Cdrom ](#cdrom-usb)
   * [ Disk ](#disk-usb)
   * [ Hasp ](#hasp-usb)
   * [ Hub ](#hub-usb)
   * [ Human interface ](#human-interface-usb)
   * [ Input/keyboard ](#inputkeyboard-usb)
   * [ Input/mouse ](#inputmouse-usb)
   * [ Net/wireless ](#netwireless-usb)
   * [ Network ](#network-usb)
   * [ Printer ](#printer-usb)
   * [ Scanner ](#scanner-usb)
   * [ Serial controller ](#serial-controller-usb)
   * [ Sound ](#sound-usb)
   * [ Ups ](#ups-usb)
   * [ Others ](#others-usb)

USB Devices
-----------

Count  — number of computers with this device installed,
Driver — driver in use for this device,
Probe  — latest probe ID of this device.

### Audio (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0499:170f | Yamaha     | Steinberg UR22mkII           | 1     | snd_usb... | 996F55BB36 |
| 1235:8205 | Focusri... | Scarlett Solo USB            | 1     | snd_usb... | 98ED2C77C7 |

### Bluetooth (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0a12:0001 | Cambrid... | Bluetooth Dongle (HCI mode)  | 4     | btusb      | CCFB9E8382 |

### Camera (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:0766 | Microsoft  | LifeCam VX-800               | 1     | snd_usb... | C7E9D74C3D |
| 046d:0821 | Logitech   | HD Webcam C910               | 1     | uvcvideo   | 22AA6FF795 |
| 046d:0825 | Logitech   | Webcam C270                  | 1     | uvcvideo   | 306C64CE80 |
| 046d:0826 | Logitech   | HD Webcam C525               | 1     | snd_usb... | 50527E087E |
| 046d:082d | Logitech   | HD Pro Webcam C920           | 1     | uvcvideo   | 3F6EE4141E |

### Card reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:0129 | Realtek... | RTS5129 Card Reader Contr... | 1     | rtsx_usb   | ED799888EB |

### Cdrom (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046b:ff20 | America... | Virtual CDROM0               | 19    | uas, us... | 4391DFF8D2 |
| 8564:1000 | Transcend  | TS32GJF370 JetFlash 32GB     | 7     | uas, us... | 5B11F4C63C |
| 04c5:2028 | Fujitsu    | External HDD 128GB           | 2     | uas, us... | 8E73F804F5 |
| 174c:55aa | ASMedia... | Name: ASM1051E SATA 6Gb/s... | 2     | uas        | ABCF5C7050 |
| 03f0:2027 | Hewlett... | Virtual DVD-ROM              | 1     | usb_sto... | 9036136416 |
| 05e3:0719 | Genesys... | SATA adapter                 | 1     | uas, us... | 463CF097DA |
| 0e8d:1806 | MediaTek   | Samsung SE-208AB Slim Por... | 1     | usb_sto... | 08100751B7 |
| 12d1:107e | Huawei ... | File-CD Gadget               | 1     | uas, us... | 179B0500B3 |
| 1bcf:0c31 | Sunplus... | SPIF30x Serial-ATA bridge    | 1     | usb_sto... | D092DEFE79 |

### Disk (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046b:ff31 | America... | Virtual HDISK0               | 19    | uas, us... | 4391DFF8D2 |
| 0624:0249 | Avocent    | Virtual Keyboard/Mouse       | 4     | usb_sto... | B6374DF09A |
| 0951:1666 | Kingsto... | DataTraveler 100 G3/G4/SE... | 4     | uas, us... | 0D6231DB61 |
| 046b:ff40 | America... | Virtual Floppy0              | 3     | usb_sto... | 996F55BB36 |
| 0781:5581 | SanDisk    | Ultra 128GB                  | 3     | uas, us... | C392838A14 |
| 125f:312b | A-DATA ... | Superior S102 Pro 16GB       | 3     | uas, us... | AF9F6ACE3F |
| 0781:5567 | SanDisk    | Cruzer Blade 16GB            | 2     | uas, us... | 97313ACF09 |
| 0781:5583 | SanDisk    | Ultra Fit 123GB              | 2     | uas, us... | 07278BAD4B |
| 0424:4030 | Standar... | LUN 00 Media 0               | 1     | uas, us... | 140DAF886E |
| 0424:4063 | Standar... | Ultra HS-SD/MMC              | 1     | uas, us... | 22AA6FF795 |
| 0480:a202 | Toshiba... | Canvio Basics HDD 500GB      | 1     | uas, us... | 7DC714253B |
| 048d:1234 | Integra... | PenDrive 8GB                 | 1     | usb_sto... | ABCF5C7050 |
| 058f:6362 | Alcor M... | Flash Card Reader/Writer     | 1     | uas, us... | CC8DB94EBA |
| 058f:6387 | Alcor M... | Transcend JetFlash Flash ... | 1     | uas, us... | 73D514E056 |
| 05dc:a838 | Lexar M... | USB JumpDrive Tough 128GB    | 1     | uas, us... | 68E0C84652 |
| 05e3:0723 | Genesys... | GL827L SD/MMC/MS Flash Ca... | 1     | usb_sto... | 342271719A |
| 05e3:0727 | Genesys... | microSD Reader/Writer        | 1     | uas, us... | 8681056F1F |
| 0624:0251 | Avocent    | Virtual Mass Storage         | 1     | uas, us... | A1EFBB1BE1 |
| 0781:5571 | SanDisk    | Cruzer Fit 16GB              | 1     | uas, us... | 320D2811E2 |
| 090c:1000 | Silicon... | Flash Voyager                | 1     | uas, us... | 73D514E056 |
| 090c:6200 | Silicon... | microSD card reader          | 1     | usb_sto... | B7B182E812 |
| 0930:6544 | Toshiba    | TransMemory-Mini / Kingst... | 1     | uas, us... | 661671ECB0 |
| 0951:1660 | Kingsto... | Data Traveller 108 16GB      | 1     | uas, us... | AA728FD340 |
| 0bda:0153 | Realtek... | 3-in-1 (SD/SDHC/SDXC) Car... | 1     | usb_sto... | ABCF5C7050 |
| 0bda:0177 | Realtek... | SD/MMC/MS PRO 31GB           | 1     | uas, us... | 9E91D0ED19 |
| 0bda:0328 | Realtek... | SD/MMC CRW                   | 1     | uas, us... | AD29AF92C2 |
| 0ea0:2222 | Ours Te... | Ours Disk                    | 1     | usb_sto... | C60718AEF4 |
| 1058:0910 | Western... | My Book Essential Edition... | 1     | uas, us... | D804E1DA52 |
| 1058:1130 | Western... | My Book Essential (WDBACW)   | 1     | uas, us... | CCFB9E8382 |
| 1058:25a2 | Western... | Elements 25A2 500GB          | 1     | uas, us... | D804E1DA52 |
| 13fe:4100 | Kingsto... | Silicon-Power16G 16GB        | 1     | usb_sto... | B3CC6B4BAD |
| 13fe:4300 | Kingsto... | USB DISK 2.0 16GB            | 1     | uas, us... | E09920BB82 |
| 14cd:121f | Super Top  | microSD CardReader SY-T18    | 1     | usb_sto... | 1B697EC2B1 |
| 152d:1561 | JMicron... | JMS561U two ports SATA 6G... | 1     | uas        | ABCF5C7050 |
| 18a5:0300 | Verbatim   | STORE N GO 8GB               | 1     | uas, us... | 0F21E859FA |
| 1b1c:1a08 | Corsair    | Voyager Air                  | 1     | uas, us... | 22AA6FF795 |
| 1f75:0621 | Innosto... | JPVX-22JC3T0                 | 1     | usb_sto... | CBBE408AAC |

### Hasp (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0529:0001 | Aladdin... | HASP copy protection dongle  | 2     |            | E53A957D3F |

### Hub (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1d6b:0002 | Linux F... | 2.0 root hub                 | 186   | hub        | 726EA75DCA |
| 1d6b:0003 | Linux F... | 3.0 root hub                 | 90    | hub        | 726EA75DCA |
| 1d6b:0001 | Linux F... | 1.1 root hub                 | 76    | hub        | 726EA75DCA |
| 8087:8002 | Intel      | Hub                          | 41    | hub        | 140DAF886E |
| 8087:800a | Intel      | Hub                          | 41    | hub        | 140DAF886E |
| 0557:7000 | ATEN In... | Hub                          | 36    | hub        | 8E73F804F5 |
| 8087:0024 | Intel      | Integrated Rate Matching Hub | 32    | hub        | B4C8ABC585 |
| 046b:ff01 | America... | Virtual Hub                  | 24    | hub        | 4391DFF8D2 |
| 0409:0058 | NEC Com... | HighSpeed Hub                | 16    | hub        | 4391DFF8D2 |
| 0424:2660 | Standar... | Hub                          | 14    | hub        | 140DAF886E |
| 0424:2514 | Standar... | USB 2.0 Hub                  | 13    | hub        | F33CE0F316 |
| 413c:a001 | Dell       | Hub                          | 10    | hub        | AF9F6ACE3F |
| 04b4:6560 | Cypress... | CY7C65640 USB-2.0 "TetraHub" | 5     | hub        | 25731D395E |
| 05e3:0610 | Genesys... | 4-port hub                   | 4     | hub        | F65EC09250 |
| 8087:0020 | Intel      | Integrated Rate Matching Hub | 4     | hub        | 0F9D97BC71 |
| 8087:8000 | Intel      | Hub                          | 4     | hub        | 7115F8FDB7 |
| 8087:8008 | Intel      | Hub                          | 4     | hub        | 7115F8FDB7 |
| 2109:3431 | VIA Labs   | USB2.0 Hub                   | 3     | hub        | ABCF5C7050 |
| 0438:7900 | AMD        | Hub                          | 2     | hub        | 4B4029781F |
| 05ac:1006 | Apple      | Hub in Aluminum Keyboard     | 2     | hub        | 4B4029781F |
| 05e3:0606 | Genesys... | USB 2.0 Hub / D-Link DUB-... | 2     | usbcore    | 21EEA2083D |
| 05e3:0608 | Genesys... | USB-2.0 4-Port HUB           | 2     | hub        | 7F6C6E8DE0 |
| 0a05:7211 | Unknown... | hub                          | 2     | hub        | 7CE46A749E |
| 0bda:0411 | Realtek... | 4-Port USB 3.0 Hub           | 2     | hub        | 0617E49F12 |
| 0bda:5411 | Realtek... | 4-Port USB 2.0 Hub           | 2     | hub        | 0617E49F12 |
| 1604:10c0 | Tascam     | Hub                          | 2     | hub        | 97313ACF09 |
| 1a40:0101 | incompl... | 4-Port HUB                   | 2     | hub        | 8681056F1F |
| 2109:2811 | VIA Labs   | USB2.0 Hub                   | 2     | hub        | 3F6EE4141E |
| 0000:0001 |            | Hub                          | 1     | hub        | 7115F8FDB7 |
| 03eb:3301 | Atmel      | at43301 4-Port Hub           | 1     | hub        | 81A05F54F2 |
| 0409:005a | NEC Com... | HighSpeed Hub                | 1     | hub        | 98ED2C77C7 |
| 0424:2422 | Standar... | Hub                          | 1     | hub        | 996F55BB36 |
| 0424:2640 | Standar... | USB 2.0 Hub                  | 1     | hub        | 22AA6FF795 |
| 0424:2742 | Standar... | USB2742                      | 1     | hub        | 97313ACF09 |
| 0424:5742 | Standar... | USB5742                      | 1     | hub        | 97313ACF09 |
| 04b3:4006 | IBM        | Hub                          | 1     | hub        | D7CBC31CEE |
| 04b3:4007 | IBM        | Hub                          | 1     | hub        | D7CBC31CEE |
| 04b4:6570 | Cypress... | USB2.0 Hub                   | 1     | hub        | 817865DED6 |
| 04cc:1521 | ST-Eric... | USB 2.0 Hub                  | 1     | hub        | 3F6EE4141E |
| 050d:0234 | Belkin ... | F5U234 USB 2.0 4-Port Hub    | 1     | hub        | 90D8E62525 |
| 0557:8021 | ATEN In... | CS1764A [CubiQ DVI KVMP S... | 1     | hub        | 893E9D69F2 |
| 058f:9254 | Alcor M... | Hub                          | 1     | hub        | 1B697EC2B1 |
| 05ac:1003 | Apple      | Hub in Pro Keyboard [Mits... | 1     | hub        | 6806624961 |
| 05e3:0612 | Genesys... | USB3.0 Hub 123               | 1     | hub        | 018029FB72 |
| 0835:8500 | Action ... | USB2.0 Hub                   | 1     | hub        | AB7179BE16 |
| 14cd:8601 | Super Top  | USB 2.0 Hub                  | 1     | hub        | 140DAF886E |
| 2109:0810 | VIA Labs   | 4-Port USB 3.0 VL81x Hub     | 1     | hub        | 3F6EE4141E |
| 2109:0811 | VIA Labs   | 4-Port USB 3.0 Hub           | 1     | hub        | 7F6C6E8DE0 |
| 2109:0813 | VIA Labs   | USB3.0 Hub                   | 1     | hub        | 3F6EE4141E |
| 2109:2812 | VIA Labs   | USB2.0 VL812 Hub             | 1     | hub        | A1EFBB1BE1 |
| 2109:2813 | VIA Labs   | USB2.0 Hub                   | 1     | hub        | 3F6EE4141E |
| 2109:8110 | VIA Labs   | USB 3.0 Hub                  | 1     | hub        | 3F6EE4141E |
| 413c:1003 | Dell       | Keyboard Hub                 | 1     | hub        | A82CA8F32E |
| 413c:1005 | Dell       | Multimedia Pro Keyboard Hub  | 1     | hub        | A1EFBB1BE1 |

### Human interface (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:c626 | Logitech   | 3Dconnexion Space Navigat... | 1     | usbhid     | 22AA6FF795 |
| 04e8:2084 | Samsung... | CoolTouch System             | 1     | usbhid     | C7E9D74C3D |
| 054c:05c4 | Sony       | DualShock 4 [CUH-ZCT1E]      | 1     | usbhid     | 98ED2C77C7 |
| 056d:0002 | EIZO       | HID Monitor Controls         | 1     | usbhid     | 98ED2C77C7 |
| 05ac:1393 | Apple      | AirPod Case                  | 1     | usbhid     | AD29AF92C2 |
| 0665:5161 | Cypress... | USB to Serial                | 1     | usbhid     | B3CC6B4BAD |
| 077d:0410 | Griffin... | PowerMate                    | 1     | powermate  | 3F6EE4141E |
| 1b1c:0c04 | Corsair    | Integrated USB Bridge        | 1     | usbhid     | 98ED2C77C7 |

### Input/keyboard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046b:ff10 | America... | Virtual Keyboard and Mouse   | 45    | usbhid     | CEC82EF5D0 |
| 0557:2419 | ATEN In... | Generic USB Mouse            | 37    | usbhid     | 8E73F804F5 |
| 046d:c31c | Logitech   | Keyboard K120 for Business   | 25    | usbhid     | D05461EFDA |
| 0624:0248 | Avocent    | Virtual Hub                  | 9     | usbhid     | F33CE0F316 |
| 1c4f:0002 | SiGma M... | Keyboard TRACER Gamma Ivory  | 7     | usbhid     | 21E2FBBB75 |
| 03f0:0024 | Hewlett... | KU-0316 Keyboard             | 6     | usbhid     | AF9F6ACE3F |
| 0461:0010 | Primax ... | HP PR1101U / Primax PMX-K... | 5     | usbhid     | 9A27511AEB |
| 046d:c52b | Logitech   | Unifying Receiver            | 5     | usbhid     | AD29AF92C2 |
| 03f0:7029 | Hewlett... | Virtual Keyboard             | 3     | usbhid     | 179B0500B3 |
| 045e:0745 | Microsoft  | Nano Transceiver v1.0 for... | 3     | usbhid     | C7E9D74C3D |
| 046d:c315 | Logitech   | Classic Keyboard 200         | 3     | usbhid     | 140DAF886E |
| 046d:c534 | Logitech   | Unifying Receiver            | 3     | usbhid     | F170BEEE88 |
| 413c:2107 | Dell       | USB Entry Keyboard           | 3     | usbhid     | CEC82EF5D0 |
| 413c:2113 | Dell       | KB216 Wired Keyboard         | 3     | usbhid     | 1C2A92612A |
| 03f0:1027 | Hewlett... | Virtual keyboard and mouse   | 2     | usbhid     | 6806624961 |
| 0458:0708 | KYE Sys... | Multimedia Keyboard          | 2     | usbhid     | EEE6327556 |
| 045e:0752 | Microsoft  | Wired Keyboard 400           | 2     | usbhid     | 90D8E62525 |
| 046d:c52e | Logitech   | MK260 Wireless Combo Rece... | 2     | usbhid     | 7CE46A749E |
| 04f2:0116 | Chicony... | KU-2971/KU-0325 Keyboard     | 2     | usbhid     | 3BA1B5FC84 |
| 09da:9090 | A4Tech     | XL-730K / XL-750BK / XL-7... | 2     | usbhid     | AB7179BE16 |
| 0c45:7401 | Microdia   | TEMPer Temperature Sensor    | 2     | usbhid     | 52D5275C7F |
| 13ba:0017 | PCPlay     | PS/2 Keyboard+Mouse Adapter  | 2     | usbhid     | 0BD7D9FCEA |
| 258a:0001 | SINO WE... | USB KEYBOARD                 | 2     | usbhid     | 2794B14FEE |
| 03ee:8801 | Mitsumi    | Keyboard                     | 1     | usbhid     | 833ACDFC5E |
| 03f0:034a | Hewlett... | Elite Keyboard               | 1     | usbhid     | CC949D61BE |
| 03f0:304a | Hewlett... | Business Slim Keyboard       | 1     | usbhid     | B6374DF09A |
| 03f0:344a | Hewlett... | USB Slim Keyboard            | 1     | usbhid     | 0617E49F12 |
| 045e:00db | Microsoft  | Natural Ergonomic Keyboar... | 1     | usbhid     | 018029FB72 |
| 045e:00dd | Microsoft  | Comfort Curve Keyboard 20... | 1     | usbhid     | FD500E59D6 |
| 045e:0780 | Microsoft  | Comfort Curve Keyboard 3000  | 1     | usbhid     | D9F1CB21BC |
| 045e:07b2 | Microsoft  | 2.4GHz Transceiver v8.0 u... | 1     | usbhid     | 21EEA2083D |
| 0461:4dbf | Primax ... | USB Wired Keyboard           | 1     | usbhid     | 1F35F79302 |
| 046a:0008 | Cherry     | Wireless Keyboard and Mouse  | 1     | usbhid     | 342271719A |
| 046a:0011 | Cherry     | G83 (RS 6000) Keyboard       | 1     | usbhid     | 28844998E9 |
| 046a:0023 | Cherry     | CyMotion Master Linux Key... | 1     | usbhid     | C392838A14 |
| 046a:0180 | Cherry     | wired keyboard               | 1     | usbhid     | 98ED2C77C7 |
| 046d:c317 | Logitech   | Wave Corded Keyboard         | 1     | usbhid     | 07278BAD4B |
| 046d:c318 | Logitech   | Illuminated Keyboard         | 1     | usbhid     | 7E28DD35AD |
| 046d:c31d | Logitech   | Media Keyboard K200          | 1     | usbhid     | C9D389B2A3 |
| 046d:c517 | Logitech   | LX710 Cordless Desktop Laser | 1     | usbhid     | DEC98C8F86 |
| 046e:5506 | Behavio... | USB Multimedia Keyboard      | 1     | usbhid     | 08100751B7 |
| 04b3:4002 | IBM        | MM2                          | 1     | usbhid     | D7CBC31CEE |
| 04d9:1203 | Holtek ... | Keyboard                     | 1     | usbhid     | E2C162993B |
| 04d9:1503 | Holtek ... | Shortboard Lefty             | 1     | usbhid     | D092DEFE79 |
| 04f2:0111 | Chicony... | KU-9908 Keyboard             | 1     | usbhid     | 3FAC52AF81 |
| 04f2:0112 | Chicony... | KU-8933 Keyboard with PS/... | 1     | usbhid     | 0F21E859FA |
| 04f2:0402 | Chicony... | Genius LuxeMate i200 Keyb... | 1     | usbhid     | ABCF5C7050 |
| 04f3:0103 | Elan Mi... | ActiveJet K-2024 Multimed... | 1     | usbhid     | E53A957D3F |
| 0518:0001 | EzKEY      | USB to PS2 Adaptor v1.09     | 1     | usbhid     | 138B84322E |
| 05ac:020c | Apple      | Extended Keyboard [Mitsumi]  | 1     | usbhid     | 6806624961 |
| 05ac:0220 | Apple      | Aluminum Keyboard (ANSI)     | 1     | usbhid     | 4D09E718D8 |
| 05ac:0221 | Apple      | Aluminum Keyboard (ISO)      | 1     | usbhid     | 4B4029781F |
| 05ac:0267 | Apple      | Magic Keyboard A1644         | 1     | usbhid     | F65EC09250 |
| 05d5:0624 | Super G... | Multimedia keyboard          | 1     | usbhid     | 7115F8FDB7 |
| 0624:0294 | Avocent    | Dell 03R874 KVM dongle       | 1     |            | 97313ACF09 |
| 062a:4101 | MosArt ... | Wireless Keyboard/Mouse      | 1     | usbhid     | A1916FC246 |
| 0835:8501 | Action ... | USB HID                      | 1     | usbhid     | AB7179BE16 |
| 0835:8502 | Action ... | USB HID                      | 1     | usbhid     | AB7179BE16 |
| 0a81:0103 | Chesen ... | Keyboard                     | 1     | usbhid     | BA2F9FC714 |
| 0b51:0020 | Comfort... | Comfort Keyboard             | 1     | usbhid     | 3F6EE4141E |
| 0c45:7603 | Microdia   | USB Keyboard                 | 1     | usbhid     | 1B697EC2B1 |
| 0d3d:0001 | Tangtop... | HID Keyboard                 | 1     | usbhid     | 3F4700F256 |
| 1038:0100 | SteelSe... | Ideazon Zboard               | 1     | usbhid     | 81A05F54F2 |
| 14dd:1005 | Raritan... | D2CIM-VUSB                   | 1     | usbhid     | FF3ACF2BDC |
| 1532:0203 | Razer USA  | BlackWidow Chroma            | 1     | usbhid     | DCFB9EE48A |
| 17ef:6009 | Lenovo     | ThinkPad Keyboard with Tr... | 1     | usbhid     | DAA63A315C |
| 17ef:608c | Lenovo     | Lenovo Calliope USB Keyboard | 1     | usbhid     | ED799888EB |
| 1a2c:0c21 | China R... | USB Keyboard                 | 1     | usbhid     | 893E9D69F2 |
| 1a2c:0e24 | China R... | USB Keyboard                 | 1     | usbhid     | 22AA6FF795 |
| 1a2c:2d23 | China R... | USB Keyboard                 | 1     | usbhid     | 09FB58BF8D |
| 1c4f:0016 | SiGma M... | USB Keyboard                 | 1     | usbhid     | 661671ECB0 |
| 1c4f:0026 | SiGma M... | Keyboard                     | 1     | usbhid     | 6DE12619B5 |
| 1d57:fa60 | Xenta      | 2.4G Receiver                | 1     | usbhid     | A8A3023830 |
| 2222:0013 | MacAlly    | iKeySlim                     | 1     | usbhid     | 1B697EC2B1 |
| 248a:0568 | Maxxter    | Wireless Receiver            | 1     | usbhid     | A8A3023830 |
| 2516:0004 | Cooler ... | Storm QuickFire Rapid Mec... | 1     | usbhid     | 389DD308F7 |
| 2a7a:2eb2 |            | USBKB                        | 1     | usbhid     | EDE05678EF |
| 413c:0000 | Dell       | DRAC 5 Virtual Keyboard a... | 1     | usbhid     | B7596E9C0E |
| 413c:2005 | Dell       | RT7D50 Keyboard              | 1     | usbhid     | 7F6C6E8DE0 |
| 413c:2010 | Dell       | Keyboard                     | 1     | usbhid     | A82CA8F32E |
| 413c:2011 | Dell       | Multimedia Pro Keyboard      | 1     | usbhid     | A1EFBB1BE1 |

### Input/mouse (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:c077 | Logitech   | M105 Optical Mouse           | 21    | usbhid     | D05461EFDA |
| 0557:2221 | ATEN In... | Winbond Hermon               | 14    | usbhid     | B4C8ABC585 |
| 093a:2510 | Pixart ... | Optical Mouse                | 11    | usbhid     | 726EA75DCA |
| 046d:c05a | Logitech   | M90/M100 Optical Mouse       | 5     | usbhid     | D9F1CB21BC |
| 03f0:134a | Hewlett... | Optical Mouse                | 2     | usbhid     | 1C2A92612A |
| 0458:003a | KYE Sys... | NetScroll+ Mini Traveler ... | 2     | usbhid     | AA728FD340 |
| 046d:c05b | Logitech   | M-U0004 810-001317 [B110 ... | 2     | usbhid     | 7115F8FDB7 |
| 15d9:0a4f | Trust I... | Optical Mouse                | 2     | usbhid     | E53A957D3F |
| 1bcf:0005 | Sunplus... | USB Optical Mouse            | 2     | usbhid     | 21E2FBBB75 |
| 413c:301a | Dell       | MS116 USB Optical Mouse      | 2     | usbhid     | C74A66C7E6 |
| 0000:0538 |            | USB OPTICAL MOUSE            | 1     | usbhid     | 8681056F1F |
| 0000:3825 |            | USB OPTICAL MOUSE            | 1     | usbhid     | 25731D395E |
| 03f0:154a | Hewlett... | USB 1000dpi Laser Mouse (... | 1     | usbhid     | 5B9EC879FC |
| 03f0:1f4a | Hewlett... | USB Optical Mouse            | 1     | usbhid     | 0617E49F12 |
| 045e:0040 | Microsoft  | Wheel Mouse Optical          | 1     | usbhid     | 7E28DD35AD |
| 045e:0084 | Microsoft  | Basic Optical Mouse          | 1     | usbhid     | 0F21E859FA |
| 045e:00cb | Microsoft  | Basic Optical Mouse v2.0     | 1     | usbhid     | 463CF097DA |
| 045e:0797 | Microsoft  | Optical Mouse 200            | 1     | usbhid     | B6374DF09A |
| 0461:4d0f | Primax ... | HP Optical Mouse             | 1     | usbhid     | 8E73F804F5 |
| 0461:4d20 | Primax ... | HP Optical Mouse             | 1     | usbhid     | 8E139BB972 |
| 0461:4de2 | Primax ... | USB Optical Mouse            | 1     | usbhid     | 1F35F79302 |
| 046d:c00e | Logitech   | M-BJ58/M-BJ69 Optical Whe... | 1     | usbhid     | 4B4029781F |
| 046d:c016 | Logitech   | Optical Wheel Mouse          | 1     | usbhid     | 81A05F54F2 |
| 046d:c050 | Logitech   | RX 250 Optical Mouse         | 1     | usbhid     | 2C877CF870 |
| 046d:c063 | Logitech   | DELL Laser Mouse             | 1     | usbhid     | A1EFBB1BE1 |
| 046d:c07d | Logitech   | Gaming Mouse G502            | 1     | usbhid     | 98ED2C77C7 |
| 046d:c52f | Logitech   | Unifying Receiver            | 1     | usbhid     | B4AD542A11 |
| 046d:c537 | Logitech   | USB Receiver                 | 1     | usbhid     | 3D4DABE8D3 |
| 04d9:1400 | Holtek ... | PS/2 keyboard + mouse con... | 1     | usbhid     | 52D5275C7F |
| 04f2:0939 | Chicony... | USB Optical Mouse            | 1     | usbhid     | 3FAC52AF81 |
| 04f3:0210 | Elan Mi... | Optical Mouse                | 1     | usbhid     | F8D0599716 |
| 04f3:0234 | Elan Mi... | Optical Mouse                | 1     | usbhid     | 138B84322E |
| 056a:0015 | Wacom      | CTE-440 [Graphire4 (4x5)]    | 1     | usbhid     | 3F6EE4141E |
| 0581:0101 | Racal D... | Racal Generic USB Mouse      | 1     | usbhid     | 7CE46A749E |
| 05ac:0304 | Apple      | Mighty Mouse [Mitsumi, M1... | 1     | usbhid     | 6806624961 |
| 0624:0422 | Avocent    | USBIAC                       | 1     | usbhid     | E09920BB82 |
| 093a:2521 | Pixart ... | Optical Mouse                | 1     | usbhid     | F31CFA06C5 |
| 09da:0006 | A4Tech     | Optical Mouse WOP-35 / Tr... | 1     | usbhid     | D804E1DA52 |
| 09da:33d8 | A4Tech     | USB Device                   | 1     | usbhid     | 996F55BB36 |
| 0c45:800a | Microdia   | Vivitar Vivicam3350B         | 1     | usbhid     | 7F6C6E8DE0 |
| 10c4:8103 | Cygnal ... | USB OPTICAL MOUSE            | 1     | usbhid     | 1E87C9E765 |
| 1241:1166 | Belkin     | MI-2150 Trust Mouse          | 1     | usbhid     | DCFB9EE48A |
| 145f:017e | Trust      | mouse                        | 1     | usbhid     | 07278BAD4B |
| 145f:01ac | Trust      | Trust Gaming Mouse           | 1     | usbhid     | 342271719A |
| 17ef:608d | Lenovo     | lenovo USB Optical Mouse     | 1     | usbhid     | ED799888EB |
| 18f8:0f99 | [Maxxter]  | Optical gaming mouse         | 1     | usbhid     | CBFB7C31D8 |
| 1a81:2204 | Holtek ... | Optical Mouse                | 1     | usbhid     | 08100751B7 |
| 1b1c:1b0f | Corsair    | M45 Gaming Mouse             | 1     | usbhid     | C392838A14 |
| 1bcf:0007 | Sunplus... | Optical Mouse                | 1     | usbhid     | 2794B14FEE |
| 1c4f:0032 | SiGma M... | Usb Mouse                    | 1     | usbhid     | 09FB58BF8D |
| 1e7d:2d5a | ROCCAT     | Savu                         | 1     | usbhid     | 3F6EE4141E |
| 2188:0ae1 | CalDigit   | USB OPTICAL MOUSE            | 1     | usbhid     | 661671ECB0 |
| 248a:5328 | Maxxter    | 2.4G Wireless Mouse          | 1     | usbhid     | F65EC09250 |
| 248a:8367 | Maxxter    | SVEN Comfort 3400 Wireless   | 1     | usbhid     | 140DAF886E |
| 2575:8753 | Weida H... | CoolTouch System             | 1     | usbhid     | A1EFBB1BE1 |
| 275d:0ba6 |            | USB OPTICAL MOUSE            | 1     | usbhid     | CBBE408AAC |
| 413c:3016 | Dell       | Optical 5-Button Wheel Mouse | 1     | usbhid     | 22AA6FF795 |
| 89e5:101b |            | USB OPTICAL MOUSE            | 1     | usbhid     | ABCF5C7050 |

### Net/wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0b05:17ab | ASUSTek... | USB-N13 802.11n Network A... | 2     | rtl8192... | 25731D395E |
| 0bda:8176 | Realtek... | RTL8188CUS 802.11n WLAN A... | 2     | rtl8192cu  | E6F0F25FEA |
| 0846:9041 | NetGear    | WNA1000M 802.11bgn [Realt... | 1     | rtl8192... | 81A05F54F2 |
| 0bda:8178 | Realtek... | RTL8192CU 802.11n WLAN Ad... | 1     | rtl8192... | DEC98C8F86 |
| 0cf3:9271 | Qualcom... | AR9271 802.11n               | 1     | ath9k_htc  | F31CFA06C5 |
| 148f:5370 | Ralink ... | RT5370 Wireless Adapter      | 1     | rt2800usb  | 67F26A64DE |
| 2001:331c | D-Link     | 802.11ac NIC                 | 1     | 88x2bu     | 8681056F1F |
| 7392:7811 | Edimax ... | EW-7811Un 802.11n Wireles... | 1     | rtl8192... | F8F57720F8 |

### Network (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046b:ffb0 | America... | Virtual Ethernet             | 16    | cdc_ether  | 4391DFF8D2 |
| 04b3:4010 | IBM        | RNDIS/Ethernet Gadget        | 6     | cdc_ether  | B55F7AE6F3 |
| 1004:6344 | LG Elec... | G2 Android Phone [tetheri... | 1     | cdc_acm    | F8D0599716 |
| 13b1:0041 | Linksys    | Gigabit Ethernet Adapter     | 1     | r8152      | 1C2A92612A |
| 413c:a102 | Dell       | iDRAC Virtual NIC USB Device | 1     | cdc_ether  | C74A66C7E6 |

### Printer (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:132a | Hewlett... | LaserJet 200 color M251n     | 1     | usblp      | 53D66D8AB3 |
| 03f0:2b17 | Hewlett... | LaserJet 1020                | 1     | usblp      | 6DE12619B5 |
| 03f0:8704 | Hewlett... | DeskJet 5940                 | 1     | usblp      | FD500E59D6 |
| 04f9:002e | Brother... | HL-4040CN                    | 1     | usblp      | A441B16889 |

### Scanner (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04b8:011b | Seiko E... | GT-9300UF [Perfection 240... | 1     |            | 53D66D8AB3 |

### Serial controller (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 067b:2303 | Prolifi... | PL2303 Serial Port           | 1     | pl2303     | 3F6EE4141E |

### Sound (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0d8c:000e | C-Media... | Audio Adapter (Planet UP-... | 3     | snd_usb... | 7CE46A749E |
| 041e:30dd | Creativ... | Sound Blaster X-Fi Go! Pro   | 1     | snd_usb... | 7092025B53 |
| 046d:0a29 | Logitech   | H600 [Wireless Headset]      | 1     | snd_usb... | 3F6EE4141E |
| 047f:0411 | Plantro... | Savi Office Base Station     | 1     | snd_usb... | 3F6EE4141E |
| 0c76:160c | JMTek      | USB Speaker                  | 1     | snd_usb... | 0BD7D9FCEA |
| 0d8c:000c | C-Media... | Audio Adapter                | 1     | snd_usb... | 8681056F1F |
| 8086:0808 | Intel      | USB PnP Sound Device         | 1     | snd_usb... | 9D77BCD222 |

### Ups (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0764:0501 | Cyber P... | CP1500 AVR UPS               | 3     | usbhid     | 21E2FBBB75 |
| 0463:ffff | MGE UPS... | UPS                          | 1     | usbfs      | BF0A7F04B4 |
| 051d:0002 | America... | Back-UPS Pro 500/1000/1500   | 1     | usbhid     | 320D2811E2 |
| 09ae:4004 | Tripp Lite | UPS                          | 1     | usbfs      | 52D5275C7F |

### Others (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0483:dada | STMicro... | SF600-ISP                    | 1     |            | A1EFBB1BE1 |
| 17e9:4305 | Display... | dynadock U3.0                | 1     | cdc_ncm    | 7F6C6E8DE0 |
| 2717:ff40 | MediaTek   | SDM636-MTP _SN:02BF3EA3      | 1     |            | 0617E49F12 |

