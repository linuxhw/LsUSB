Most popular USB devices in Mini Pcs
====================================

See more info in the [README](https://github.com/linuxhw/LsUSB).

Contents
--------

1. [ USB Devices in Mini Pcs ](#usb-devices)
   * [ Audio ](#audio-usb)
   * [ Bluetooth ](#bluetooth-usb)
   * [ Camera ](#camera-usb)
   * [ Card reader ](#card-reader-usb)
   * [ Cdc data ](#cdc-data-usb)
   * [ Cdrom ](#cdrom-usb)
   * [ Chipcard ](#chipcard-usb)
   * [ Disk ](#disk-usb)
   * [ Dvb card ](#dvb-card-usb)
   * [ Gamepad ](#gamepad-usb)
   * [ Hub ](#hub-usb)
   * [ Human interface ](#human-interface-usb)
   * [ Input/keyboard ](#inputkeyboard-usb)
   * [ Input/mouse ](#inputmouse-usb)
   * [ Mfp ](#mfp-usb)
   * [ Modem ](#modem-usb)
   * [ Net/wireless ](#netwireless-usb)
   * [ Network ](#network-usb)
   * [ Phone ](#phone-usb)
   * [ Printer ](#printer-usb)
   * [ Scanner ](#scanner-usb)
   * [ Serial controller ](#serial-controller-usb)
   * [ Sound ](#sound-usb)
   * [ Others ](#others-usb)

USB Devices
-----------

Count  — number of computers with this device installed,
Driver — driver in use for this device,
Probe  — latest probe ID of this device.

### Audio (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 20b1:3008 | XMOS       | iFi (by AMR) HD USB Audio    | 1     | snd_usb... | 9C11BAA82B |

### Bluetooth (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 8087:0a2b | Intel      | Bluetooth Device             | 27    | btusb      | F68CFBE3F5 |
| 8087:0a2a | Intel      | Bluetooth Device             | 26    | btusb      | 4C9A2AA59C |
| 8087:0aaa | Intel      | Bluetooth Device 9460/9560   | 14    | btusb      | 23DC7C0455 |
| 05ac:828a | Apple      | Bluetooth USB Host Contro... | 9     | btusb      | D6FE9DA999 |
| 05ac:8281 | Apple      | Bluetooth Host Controller    | 7     | btusb      | 3419AE2627 |
| 05ac:8289 | Apple      | Bluetooth USB Host Contro... | 5     | btusb      | BC577C8669 |
| 8087:07dc | Intel      | Bluetooth Device             | 5     | btusb      | 6157F936DB |
| 05ac:8216 | Apple      | Bluetooth USB Host Contro... | 4     | btusb      | 191BB34391 |
| 8087:0aa7 | Intel      | Bluetooth Device             | 4     | btusb      | C7F97B95C4 |
| 05ac:8205 | Apple      | Bluetooth HCI                | 3     | btusb      | E78329BE05 |
| 0a12:0001 | Cambrid... | Bluetooth Dongle (HCI mode)  | 2     | btusb      | FC67594808 |
| 05ac:8218 | Apple      | Bluetooth Host Controller    | 1     | btusb      | C2DA8438C2 |
| 0a5c:21e8 | Broadcom   | BCM20702A0 Bluetooth 4.0     | 1     | btusb      | 94F2AA43B7 |
| 13d3:3394 | IMC Net... | Bluetooth                    | 1     | btusb      | B590007CCF |
| 8086:0189 | Intel      | Bluetooth Device             | 1     | btusb      | E907BA80AE |
| 8087:07da | Intel      | Bluetooth Device             | 1     | btusb      | F8F1E1681B |

### Camera (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:0825 | Logitech   | Webcam C270                  | 3     | snd_usb... | C7F97B95C4 |
| 046d:081d | Logitech   | HD Webcam C510               | 2     | uvcvideo   | D6FE9DA999 |
| 0458:707c | KYE Sys... | eFace 1300                   | 1     | snd_usb... | 4E98627DA4 |
| 045e:0772 | Microsoft  | LifeCam Studio               | 1     | usbhid     | 6A65C9BCF0 |
| 045e:0810 | Microsoft  | Microsoft LifeCam HD-3000    | 1     | snd_usb... | 0ED8661BD0 |
| 046d:082d | Logitech   | HD Pro Webcam C920           | 1     | snd_usb... | 654561E17B |
| 046d:085b | Logitech   | Webcam C925e                 | 1     | snd_usb... | 0DD740DC8F |
| 046d:08ca | Logitech   | Mic (Fusion)                 | 1     | uvcvideo   | 7170BD46A3 |
| 05a3:8830 | ARC Int... | USB 2.0 Camera               | 1     | uvcvideo   | 4C9A2AA59C |
| 05ac:1112 | Apple      | FaceTime HD Camera (Display) | 1     | uvcvideo   | 029708EAF4 |
| 05ac:12a8 | Apple      | iPhone5/5C/5S/6              | 1     | usbfs      | 4C9A2AA59C |
| 0ac8:3610 | Z-Star ... | UVC USB2.0 PC-amcap          | 1     | uvcvideo   | 9D7BDB2727 |
| 0ac8:c40a | Z-Star ... | A4 TECH USB2.0 PC Camera J   | 1     | snd_usb... | 3B024E0CB3 |
| 0bda:58bb | Realtek... | HD 720P Webcam               | 1     | uvcvideo   | DD43C776B1 |

### Card reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:0129 | Realtek... | RTS5129 Card Reader Contr... | 8     | rtsx_usb   | BD1F4F70F7 |

### Cdc data (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0547:2001 | Anchor ... | MTouchR2P3                   | 1     |            | 4C9A2AA59C |

### Cdrom (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 152d:0578 | JMicron... | JMS567 SATA 6Gb/s bridge     | 3     | uas, us... | 69BB2EE752 |
| 0e8d:1887 | MediaTek   | DVDRAM GP60NS50              | 2     | usb_sto... | B367B6054B |
| 04c5:2028 | Fujitsu    | External HDD 128GB           | 1     | uas, us... | 26108721CF |
| 054c:03dc | Sony       | DVD RW DRX-S70U              | 1     | usb_sto... | 7A353432A6 |
| 05ac:1500 | Apple      | SuperDrive [A1379]           | 1     | usb_sto... | 3419AE2627 |
| 12d1:1082 | Huawei ... | File-CD Gadget               | 1     | uas, us... | 0C3FEC8909 |
| 13fd:0840 | Initio     | INIC-1618L SATA 100GB        | 1     | usb_sto... | 4E3E10AE15 |
| 13fd:3e40 | Initio     | ZALMAN ZM-VE350 256GB        | 1     | uas, us... | 47C7DFABC5 |
| 152d:0562 | JMicron... | MK2035GSS                    | 1     | uas        | 42A2D2DC72 |
| 413c:9016 | Dell       | DVD+/-RW DW316               | 1     | usb_sto... | 42A2D2DC72 |
| 8564:1000 | Transcend  | TS32GJF370 JetFlash 32GB     | 1     | uas, us... | 12BFB27212 |

### Chipcard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 058f:9540 | Alcor M... | AU9540 Smartcard Reader      | 1     |            | 2CBDC3AD17 |

### Disk (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 090c:1000 | Silicon... | Flash Voyager                | 5     | uas, us... | D188C06456 |
| 0bda:0153 | Realtek... | 3-in-1 (SD/SDHC/SDXC) Car... | 4     | uas, us... | 4C9A2AA59C |
| 13fe:4200 | Kingsto... | Silicon-Power32G 32GB        | 3     | uas, us... | 93C3D30E57 |
| 0781:5583 | SanDisk    | Ultra Fit 123GB              | 2     | uas, us... | 91A5E36C77 |
| 0951:1666 | Kingsto... | DataTraveler 100 G3/G4/SE... | 2     | usb_sto... | 89C50199DD |
| 1058:10a2 | Western... | Elements SE Portable (WDB... | 2     | usb_sto... | 6A65C9BCF0 |
| 0325:ac02 | OCZ Tec... | ATV Turbo / Rally2 Dual C... | 1     | uas, us... | C3E424053A |
| 0480:a007 | Toshiba... | External Disk USB 3.0 750GB  | 1     | uas, us... | A317F5B87E |
| 0480:a200 | Toshiba... | External USB 3.0             | 1     | uas, us... | C7F97B95C4 |
| 0480:a202 | Toshiba... | Canvio Basics HDD 500GB      | 1     | uas, us... | BD1F4F70F7 |
| 04e8:61f3 | Samsung... | Portable SSD T3 (MU-PT250... | 1     | uas        | F598A50056 |
| 04e8:61f5 | Samsung... | Portable SSD T5              | 1     | uas        | 1ECDA8C9DB |
| 058f:6332 | Alcor M... | Multi-Function Card Reader   | 1     | usb_sto... | 4727851853 |
| 058f:6362 | Alcor M... | Flash Card Reader/Writer     | 1     | uas, us... | FD127FD8BF |
| 058f:6366 | Alcor M... | Multi Flash Reader           | 1     | uas, us... | 69BB2EE752 |
| 058f:6387 | Alcor M... | Transcend JetFlash Flash ... | 1     | usb_sto... | 55112C2DEF |
| 058f:9380 | Alcor M... | USB Flash Disk 32GB          | 1     | uas, us... | 7170BD46A3 |
| 05dc:b051 | Lexar M... | microSD RDR UHS-I Card Re... | 1     | uas, us... | 779445C7DD |
| 05e3:0736 | Genesys... | microSD Reader/Writer        | 1     | uas, us... | 0A72BA2CA6 |
| 0781:5567 | SanDisk    | Cruzer Blade 16GB            | 1     | uas, us... | CDCA30B79F |
| 0781:5571 | SanDisk    | Cruzer Fit 16GB              | 1     | uas, us... | 7FCDFDF9E4 |
| 0781:5575 | SanDisk    | Cruzer Glide 32GB            | 1     | uas, us... | A9324F43CF |
| 0781:5580 | SanDisk    | SDCZ80 Flash Drive 16GB      | 1     | uas, us... | EDB53070CD |
| 091e:2660 | Garmin ... | FR620 Flash                  | 1     | uas, us... | 2FB80FF93D |
| 0951:1642 | Kingsto... | DT101 G2 16GB                | 1     | uas, us... | F1A9B27E5C |
| 0951:1643 | Kingsto... | DataTraveler G3 15GB         | 1     | uas, us... | 0DD740DC8F |
| 0bc2:2322 | Seagate    | SRD0NF1 Expansion Portabl... | 1     | uas        | 94F2AA43B7 |
| 0bc2:3330 | Seagate    | External                     | 1     | uas, us... | 7C4C7CAF9A |
| 0bc2:61b7 | Seagate    | Maxtor M3 Portable           | 1     | uas, us... | 94F2AA43B7 |
| 0bc2:ab24 | Seagate    | Backup Plus Portable Drive   | 1     | uas        | BC577C8669 |
| 0bda:0186 | Realtek... | Card Reader                  | 1     | uas, us... | DD552055CD |
| 0bda:0307 | Realtek... | Card Reader                  | 1     | uas, us... | D6FE9DA999 |
| 1058:1042 | Western... | Elements SE Portable (WDB... | 1     | uas, us... | 2FB80FF93D |
| 1058:10a8 | Western... | Elements Portable (WDBUZG)   | 1     | uas, us... | BD1F4F70F7 |
| 1058:1132 | Western... | My Book 1132                 | 1     | uas, us... | 2B957DDA7E |
| 1058:259d | Western... | My Passport Ultra (WDBBKD)   | 1     | uas, us... | 001551B002 |
| 1058:25ee | Western... | My Book 25EE                 | 1     | uas, us... | 24BE20A939 |
| 1058:25fa | Western... | easystore 25FA               | 1     | uas, us... | 42A2D2DC72 |
| 1307:0163 | Transcend  | 256MB/512MB/1GB Flash Drive  | 1     | uas, us... | 070C06EF96 |
| 13fe:6300 | Kingsto... | Disk                         | 1     | uas, us... | 24DD606150 |
| 152d:0551 | JMicron... | JMS551 SuperSpeed two por... | 1     | uas, us... | 037A3E45C7 |
| 152d:0567 | JMicron... | JMS567 SATA 6Gb/s bridge     | 1     | uas, us... | 24DD606150 |
| 154b:00ee | PNY        | USB 2.0 FD                   | 1     | uas, us... | 42A2D2DC72 |
| 1687:3252 | Kingmax... | USB2.0 FlashDisk 2GB         | 1     | uas, us... | 9F1C331D21 |
| 174c:5106 | ASMedia... | Transcend StoreJet 25M3 3... | 1     | usb_sto... | 4E3E10AE15 |
| 18a5:0243 | Verbatim   | Flash Drive (Store'n'Go) ... | 1     | uas, us... | 029708EAF4 |
| 1976:6025 | Chipsbr... | Flash Drive                  | 1     | uas, us... | 2C45E4CE2A |
| 1f75:0621 | Innosto... | JPVX-22JC3T0                 | 1     | uas, us... | 1C3DD0209B |
| 1f75:0917 | Innosto... | Ultra Line 16GB              | 1     | uas, us... | 65759EE30B |
| 2109:0711 | VIA Labs   | External USB 3.0             | 1     | uas, us... | 8A4B351000 |
| 2109:711f | VIA Labs   | External USB 3.0             | 1     | uas        | 77744B6ACF |
| 8564:7000 | Transcend  | Transcend 500GB              | 1     | uas, us... | 01310A1064 |

### Dvb card (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 2040:7070 | Hauppauge  | Nova-T Stick 3               | 1     | dvb_usb... | F68CFBE3F5 |

### Gamepad (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:028e | Microsoft  | Xbox360 Controller           | 2     | xpad       | BC577C8669 |
| 0079:0011 | DragonRise | Gamepad                      | 1     | usbhid     | 6A65C9BCF0 |
| 045e:0291 | Microsoft  | Xbox 360 Wireless Receive... | 1     | xpad       | 2CBDC3AD17 |

### Hub (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1d6b:0002 | Linux F... | 2.0 root hub                 | 125   | hub        | F68CFBE3F5 |
| 1d6b:0003 | Linux F... | 3.0 root hub                 | 107   | hub        | F68CFBE3F5 |
| 0a5c:4500 | Broadcom   | BCM2046B1 USB 2.0 Hub (pa... | 26    | hub        | BC577C8669 |
| 1d6b:0001 | Linux F... | 1.1 root hub                 | 22    | hub        | 3419AE2627 |
| 05e3:0610 | Genesys... | 4-port hub                   | 18    | hub        | 23DC7C0455 |
| 8087:0024 | Intel      | Integrated Rate Matching Hub | 12    | hub        | D6FE9DA999 |
| 05ac:1006 | Apple      | Hub in Aluminum Keyboard     | 10    | hub        | 3419AE2627 |
| 8087:8001 | Intel      | Hub                          | 10    | hub        | C32D1045F9 |
| 0424:2512 | Standar... | USB 2.0 Hub                  | 9     | hub        | D6FE9DA999 |
| 05e3:0608 | Genesys... | USB-2.0 4-Port HUB           | 9     | hub        | D6FE9DA999 |
| 0424:2513 | Standar... | 2.0 Hub                      | 7     | hub        | 3419AE2627 |
| 1a40:0101 | incompl... | 4-Port HUB                   | 7     | hub        | C7F97B95C4 |
| 0bda:5411 | Realtek... | 4-Port USB 2.0 Hub           | 6     | hub        | BC577C8669 |
| 0bda:0411 | Realtek... | 4-Port USB 3.0 Hub           | 5     | hub        | BC577C8669 |
| 05e3:0612 | Genesys... | USB3.0 Hub 123               | 4     | hub        | 23DC7C0455 |
| 0409:005a | NEC Com... | HighSpeed Hub                | 2     | hub        | D6FE9DA999 |
| 8087:8000 | Intel      | Hub                          | 2     | hub        | B590007CCF |
| 03f0:2f24 | Hewlett... | LP2475w Monitor Hub          | 1     | hub        | 44CAA27AAD |
| 0409:0058 | NEC Com... | HighSpeed Hub                | 1     | hub        | 8EEB1D7D04 |
| 0424:2744 | Standar... | USB2744                      | 1     | hub        | 078CF691FD |
| 0424:5744 | Standar... | USB5744                      | 1     | hub        | 078CF691FD |
| 0451:8043 | Texas I... | Hub                          | 1     | hub        | D6FE9DA999 |
| 0557:2410 | ATEN In... | 4-Port USB 3.0 Hub           | 1     | hub        | 1BAD6D6321 |
| 0557:5411 | ATEN In... | 4-Port USB 2.0 Hub           | 1     | hub        | 1BAD6D6321 |
| 0557:8021 | ATEN In... | CS1764A [CubiQ DVI KVMP S... | 1     | hub        | 1BAD6D6321 |
| 058f:6254 | Alcor M... | USB Hub                      | 1     | hub        | 69BB2EE752 |
| 058f:9254 | Alcor M... | Hub                          | 1     | hub        | 97355011D7 |
| 05ac:1003 | Apple      | Hub in Pro Keyboard [Mits... | 1     | hub        | 12BFB27212 |
| 05ac:1005 | Apple      | Keyboard Hub                 | 1     | hub        | 2C74DE0FDE |
| 05ac:9102 | Apple      | Hub                          | 1     | hub        | 12BFB27212 |
| 05ac:9119 | Apple      | Hub                          | 1     | hub        | 12BFB27212 |
| 05ac:9127 | Apple      | Hub in Thunderbolt Display   | 1     | hub        | 029708EAF4 |
| 05e3:0616 | Genesys... | hub                          | 1     | hub        | 2FB80FF93D |
| 05e3:0620 | Genesys... | USB3.0 Hub                   | 1     | hub        | 23DC7C0455 |
| 0835:8500 | Action ... | USB2.0 Hub                   | 1     | hub        | 6A65C9BCF0 |
| 0bda:0401 | Realtek... | USB3.0 Hub                   | 1     | hub        | D6FE9DA999 |
| 0bda:5401 | Realtek... | RTL 8153 USB 3.0 hub with... | 1     | hub        | D6FE9DA999 |
| 1a40:0201 | Terminu... | FE 2.1 7-port Hub            | 1     | hub        | 001551B002 |
| 1d5c:5002 | Fresco ... | USB3.0 Hub                   | 1     | hub        | 23DC7C0455 |
| 1d5c:5012 | Fresco ... | USB2.0 Hub                   | 1     | hub        | 23DC7C0455 |
| 2109:0812 | VLI Labs   | USB 3.0 VL812 HUB            | 1     | hub        | FCEBA80CA7 |
| 2109:0813 | VIA Labs   | USB3.0 Hub                   | 1     | hub        | F1A9B27E5C |
| 2109:2812 | VIA Labs   | USB2.0 VL812 Hub             | 1     | hub        | FCEBA80CA7 |
| 2109:2813 | VIA Labs   | USB2.0 Hub                   | 1     | hub        | F1A9B27E5C |
| 413c:1010 | Dell       | USB 2.0 Hub [MTT]            | 1     | hub        | 654561E17B |
| 8087:8008 | Intel      | Hub                          | 1     | usbcore    | 4E3E10AE15 |

### Human interface (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 05ac:8242 | Apple      | Built-in IR Receiver         | 26    | usbhid     | BC577C8669 |
| 05ac:8240 | Apple      | Built-in IR Receiver         | 3     | usbhid     | E78329BE05 |
| 046d:c216 | Logitech   | F310 Gamepad [DirectInput... | 1     | usbhid     | A317F5B87E |
| 04b4:fd13 | Cypress... | Programmable power socket    | 1     |            | 7C4C7CAF9A |
| 05ac:9219 | Apple      | Cinema Display 20"           | 1     | appledi... | 12BFB27212 |
| 05ac:9227 | Apple      | Thunderbolt Display          | 1     | usbhid     | 029708EAF4 |
| 20a0:0006 | Clay Logic | flirc                        | 1     | usbhid     | 94F2AA43B7 |
| 262a:13d7 | M-Audio... |                              | 1     | snd_usb... | 242161EFFC |

### Input/keyboard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:c52b | Logitech   | Unifying Receiver            | 23    | usbhid     | 64913D4FAA |
| 046d:c534 | Logitech   | Unifying Receiver            | 9     | usbhid     | 5C7849A72A |
| 05ac:0221 | Apple      | Aluminum Keyboard (ISO)      | 6     | usbhid     | 3419AE2627 |
| 062a:4101 | MosArt ... | Wireless Keyboard/Mouse      | 5     | usbhid     | F68CFBE3F5 |
| 1a2c:2d23 | China R... | USB Keyboard                 | 5     | usbhid     | 34358089CA |
| 045e:0745 | Microsoft  | Nano Transceiver v1.0 for... | 4     | usbhid     | 27537B5C01 |
| 046d:c31c | Logitech   | Keyboard K120 for Business   | 4     | usbhid     | 9C11BAA82B |
| 13ba:0018 | PCPlay     | Barcode PCP-BCG4209          | 3     | usbhid     | 74EE338F16 |
| 1c4f:0002 | SiGma M... | Keyboard TRACER Gamma Ivory  | 3     | usbhid     | BC577C8669 |
| 24ae:2000 | RAPOO      | 2.4G Wireless Device         | 3     | usbhid     | 47C7DFABC5 |
| 045e:07b2 | Microsoft  | 2.4GHz Transceiver v8.0 u... | 2     | usbhid     | 2CBDC3AD17 |
| 045e:07b9 | Microsoft  | Wired Keyboard 200           | 2     | usbhid     | EDB53070CD |
| 045e:07f8 | Microsoft  | Wired Keyboard 600 (model... | 2     | usbhid     | 65759EE30B |
| 046d:c318 | Logitech   | Illuminated Keyboard         | 2     | usbhid     | 23DC7C0455 |
| 046d:c52e | Logitech   | MK260 Wireless Combo Rece... | 2     | usbhid     | 77744B6ACF |
| 04d9:a088 | Holtek ... | USB Gaming Mouse             | 2     | usbhid     | D6FE9DA999 |
| 05ac:0220 | Apple      | Aluminum Keyboard (ANSI)     | 2     | usbhid     | 11ECD56F12 |
| 09da:054f | A4Tech     | USB Device                   | 2     | usbhid     | EDB53070CD |
| 1a2c:0e24 | China R... | USB Keyboard                 | 2     | usbhid     | E2F5056B0B |
| 413c:2105 | Dell       | Model L100 Keyboard          | 2     | usbhid     | 24DD606150 |
| 0101:0101 | Logitech   | USB Receiver                 | 1     | usbhid     | 1BAD6D6321 |
| 03f0:0024 | Hewlett... | KU-0316 Keyboard             | 1     | usbhid     | 2FB80FF93D |
| 045e:00b0 | Microsoft  | Digital Media Pro Keyboard   | 1     | usbhid     | F8F1E1681B |
| 045e:00db | Microsoft  | Natural Ergonomic Keyboar... | 1     | usbhid     | 7A40FE9A5C |
| 045e:0750 | Microsoft  | Wired Keyboard 600           | 1     | usbhid     | E78329BE05 |
| 045e:0800 | Microsoft  | Microsoft Nano Transceive... | 1     | usbhid     | C263278BF1 |
| 046a:0010 | Cherry     | SmartBoard XX44              | 1     | usbhid     | 97355011D7 |
| 046a:010e | Cherry     | USB Wireless Device          | 1     | usbhid     | 779445C7DD |
| 046d:c315 | Logitech   | Classic Keyboard 200         | 1     | usbhid     | 2C45E4CE2A |
| 046d:c326 | Logitech   | Washable Keyboard K310       | 1     | usbhid     | C32D1045F9 |
| 046d:c328 | Logitech   | Corded Keyboard K280e        | 1     | usbhid     | 7FCDFDF9E4 |
| 046d:c336 | Logitech   | Gaming Keyboard G213         | 1     | usbhid     | 6A65C9BCF0 |
| 04d9:0169 | Holtek ... | USB Keyboard                 | 1     | usbhid     | A48C087C97 |
| 04d9:0183 | Holtek ... | USB Keyboard                 | 1     | usbhid     | 1BAD6D6321 |
| 04d9:1503 | Holtek ... | Shortboard Lefty             | 1     | usbhid     | 91A5E36C77 |
| 04d9:1603 | Holtek ... | Keyboard                     | 1     | usbhid     | FD127FD8BF |
| 04d9:1605 | Holtek ... | USB Keyboard                 | 1     | usbhid     | 0E8E75B0EB |
| 04d9:1702 | Holtek ... | Keyboard LKS02               | 1     | usbhid     | 4727851853 |
| 04d9:a096 | Holtek ... | USB Keyboard                 | 1     | usbhid     | 69BB2EE752 |
| 04f2:0976 | Chicony... | Wireless Device              | 1     | usbhid     | BD87D7D9AA |
| 04f2:1123 | Chicony... | Wireless Device              | 1     | usbhid     | A11552D6EC |
| 04f2:1516 | Chicony... | USB Keyboard                 | 1     | usbhid     | 24BE20A939 |
| 04f3:0103 | Elan Mi... | ActiveJet K-2024 Multimed... | 1     | usbhid     | 5738D326F6 |
| 04fc:05d8 | Sunplus... | Wireless keyboard/mouse      | 1     | usbhid     | B96B32B8B7 |
| 0518:0001 | EzKEY      | USB to PS2 Adaptor v1.09     | 1     | usbhid     | 6C458ECDE9 |
| 0566:3006 | Montere... | Keyboard                     | 1     | usbhid     | 5A02AA7966 |
| 05ac:020c | Apple      | Extended Keyboard [Mitsumi]  | 1     | usbhid     | 12BFB27212 |
| 05ac:021d | Apple      | Aluminum Mini Keyboard (A... | 1     | usbhid     | 2C74DE0FDE |
| 05ac:024f | Apple      | Keyboard                     | 1     | usbhid     | 5A9B38338D |
| 05ac:0250 | Apple      | Aluminium Keyboard (ISO)     | 1     | usbhid     | CDCA30B79F |
| 05af:8277 | Jing-Mo... | USB Keyboard                 | 1     | usbhid     | F6DDC974E1 |
| 05e3:ffe2 | Genesys... | USB K/B with ACPI            | 1     | usbhid     | 6157F936DB |
| 0603:00f2 | Novatek... | Keyboard (Labtec Ultra Fl... | 1     | usbhid     | 89C50199DD |
| 0835:8501 | Action ... | USB HID                      | 1     | usbhid     | 6A65C9BCF0 |
| 0835:8502 | Action ... | USB HID                      | 1     | usbhid     | 6A65C9BCF0 |
| 09da:f6e3 | A4Tech     | USB Device                   | 1     | usbhid     | 7A353432A6 |
| 0c45:652f | Microdia   | USB DEVICE                   | 1     | usbhid     | ED17922D0B |
| 0c45:7304 | Microdia   | Keyboard                     | 1     | usbhid     | 001551B002 |
| 1241:1503 | Belkin     | Keyboard                     | 1     | usbhid     | 203BE612E2 |
| 12c9:1001 | Newmen ... | 2.4GHz 2way RF Receiver      | 1     | usbhid     | 203BE612E2 |
| 1532:0202 | Razer USA  | Razer DeathStalker           | 1     | usbhid     | DD552055CD |
| 17ef:6009 | Lenovo     | ThinkPad Keyboard with Tr... | 1     | usbhid     | 8F42487A13 |
| 1997:2433 |            | Mini Keyboard                | 1     | usbhid     | 2003A12B3D |
| 1a2c:0002 | China R... | USB Keykoard                 | 1     | usbhid     | FC67594808 |
| 1a2c:0b2a | China R... | USB Keyboard                 | 1     | usbhid     | F1A9B27E5C |
| 1a2c:4b81 | China R... | USB Keyboard                 | 1     | usbhid     | 4C9A2AA59C |
| 1b1c:1b20 | Corsair    | STRAFE RGB Gaming Keyboard   | 1     | usbhid     | 0E16D3F886 |
| 1c4f:0026 | SiGma M... | Keyboard                     | 1     | usbhid     | 070C06EF96 |
| 248a:8566 | Maxxter    | SVEN RX 360 Art Wireless ... | 1     | usbhid     | 5C7849A72A |
| 24ae:2003 | RAPOO      | 5G Wireless Device           | 1     | usbhid     | 7C4C7CAF9A |
| 2c97:0001 | Ledger     | Nano S                       | 1     | usbhid     | ED17922D0B |
| 413c:2110 | Dell       | Wired Multimedia Keyboard    | 1     | usbhid     | 654561E17B |

### Input/mouse (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:c52f | Logitech   | Unifying Receiver            | 6     | usbhid     | BC577C8669 |
| 046d:c077 | Logitech   | M105 Optical Mouse           | 4     | usbhid     | 3419AE2627 |
| 275d:0ba6 |            | USB OPTICAL MOUSE            | 4     | usbhid     | 2DD6AE3565 |
| 046d:c05a | Logitech   | M90/M100 Optical Mouse       | 3     | usbhid     | 5A02AA7966 |
| 0458:003a | KYE Sys... | NetScroll+ Mini Traveler ... | 2     | usbhid     | 30F524BC82 |
| 05ac:0304 | Apple      | Mighty Mouse [Mitsumi, M1... | 2     | usbhid     | 191BB34391 |
| 062a:4102 | MosArt ... | 2.4G Wireless Mouse          | 2     | usbhid     | C263278BF1 |
| 1bcf:0005 | Sunplus... | USB Optical Mouse            | 2     | usbhid     | D6FE9DA999 |
| 248a:8367 | Maxxter    | SVEN Comfort 3400 Wireless   | 2     | usbhid     | 305342DF08 |
| 0000:0538 |            | USB OPTICAL MOUSE            | 1     | usbhid     | E78329BE05 |
| 0000:3825 |            | USB OPTICAL MOUSE            | 1     | usbhid     | F339ADE022 |
| 0458:002e | KYE Sys... | NetScroll + Traveler / Ne... | 1     | usbhid     | B367B6054B |
| 0458:0065 | KYE Sys... | 4D Scroll Mouse              | 1     | usbhid     | 4C9A2AA59C |
| 045e:0083 | Microsoft  | Basic Optical Mouse          | 1     | usbhid     | 5DF12FB380 |
| 045e:0084 | Microsoft  | Basic Optical Mouse          | 1     | usbhid     | 11ECD56F12 |
| 0461:4d08 | Primax ... | USB Mouse                    | 1     | usbhid     | F8F1E1681B |
| 0461:4d51 | Primax ... | 0Y357C PMX-MMOCZUL (B) [D... | 1     | usbhid     | F1A9B27E5C |
| 0461:4d65 | Primax ... | USB Optical Mouse            | 1     | usbhid     | 65759EE30B |
| 046d:c043 | Logitech   | MX320/MX400 Laser Mouse      | 1     | usbhid     | 97355011D7 |
| 046d:c049 | Logitech   | G5 Laser Mouse               | 1     | usbhid     | 7A40FE9A5C |
| 046d:c058 | Logitech   | M115 Mouse                   | 1     | usbhid     | 32E92CBA5F |
| 046d:c05b | Logitech   | M-U0004 810-001317 [B110 ... | 1     | usbhid     | 9C11BAA82B |
| 046d:c062 | Logitech   | M-UAS144 [LS1 Laser Mouse]   | 1     | usbhid     | 2C45E4CE2A |
| 046d:c069 | Logitech   | M-U0007 [Corded Mouse M500]  | 1     | usbhid     | CDCA30B79F |
| 046d:c06c | Logitech   | Optical Mouse                | 1     | usbhid     | 55112C2DEF |
| 046d:c07d | Logitech   | Gaming Mouse G502            | 1     | usbhid     | 24BE20A939 |
| 046d:c080 | Logitech   | Gaming Mouse G303            | 1     | usbhid     | 0B66BDFEE9 |
| 046d:c084 | Logitech   | G102 Prodigy Gaming Mouse    | 1     | usbhid     | 6A65C9BCF0 |
| 046d:c404 | Logitech   | TrackMan Wheel               | 1     | usbhid     | F6DDC974E1 |
| 046d:c408 | Logitech   | Marble Mouse (4-button)      | 1     | usbhid     | 2B957DDA7E |
| 046d:c521 | Logitech   | Cordless Mouse Receiver      | 1     | usbhid     | 12BFB27212 |
| 047d:2048 | Kensington | Orbit Trackball with Scro... | 1     | usbhid     | 2C74DE0FDE |
| 047d:8018 | Kensington | Expert Wireless TB           | 1     | usbhid     | ED17922D0B |
| 04b4:6370 | Cypress... | ViewMate Desktop Mouse CC... | 1     | usbhid     | 2B957DDA7E |
| 04ca:009b | Lite-On... | Wireless Device              | 1     | usbhid     | 2350EA995C |
| 04d9:a09f | Holtek ... | E-Signal LUOM G10 Mechani... | 1     | usbhid     | DD552055CD |
| 0581:0101 | Racal D... | Racal Generic USB Mouse      | 1     | usbhid     | BDA8F46AD6 |
| 093a:2510 | Pixart ... | Optical Mouse                | 1     | usbhid     | 69BB2EE752 |
| 09da:0006 | A4Tech     | Optical Mouse WOP-35 / Tr... | 1     | usbhid     | C2DA8438C2 |
| 09da:000a | A4Tech     | Optical Mouse Opto 510D /... | 1     | usbhid     | D8C76E861B |
| 09da:8090 | A4Tech     | X-718BK Oscar Optical Gam... | 1     | usbhid     | 6C458ECDE9 |
| 0c45:5101 | Microdia   | RF Controller                | 1     | usbhid     | C7F97B95C4 |
| 0e8f:2519 | GreenAsia  | Generic USB Mouse USB Mouse  | 1     | usbhid     | 8D13B8BFE3 |
| 1532:005c | Razer USA  | Razer DeathAdder Elite       | 1     | usbhid     | 0E16D3F886 |
| 1532:006e | Razer USA  | Razer DeathAdder Essential   | 1     | usbhid     | 8F42487A13 |
| 15d9:0a33 | Trust I... | Optical Mouse                | 1     | usbhid     | 345175C61D |
| 15d9:0a4e | Trust I... | USB OPTICAL MOUSE            | 1     | usbhid     | E2F5056B0B |
| 192f:0916 | Avago T... | USB Optical Mouse            | 1     | usbhid     | 242161EFFC |
| 1af3:0001 | Kingsis... | ZOWIE Gaming mouse           | 1     | usbhid     | 23DC7C0455 |
| 1c4f:0048 | SiGma M... | Usb Mouse                    | 1     | usbhid     | 89C50199DD |
| 1e7d:2d51 | ROCCAT     | Kone+ Mouse                  | 1     | usbhid     | B4342A4CAA |
| 248a:8366 | Maxxter    | Wireless Optical Mouse AC... | 1     | usbhid     | A79C3C33B3 |
| 24ae:1800 | MOSART ... | Rapoo 2.4G Wireless Touch... | 1     | usbhid     | 305342DF08 |

### Mfp (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:042a | Hewlett... | LaserJet M1132 MFP           | 1     | usblp      | 930395777E |
| 0924:3cef | Xerox      | Phaser 3100MFP               | 1     |            | 3B024E0CB3 |
| 0924:42db | Xerox      | WorkCentre 3215              | 1     | usblp      | 9D7BDB2727 |

### Modem (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0658:0200 | Sigma D... | Aeotec Z-Stick Gen5 (ZW09... | 2     | cdc_acm    | 4D008C616B |
| c001:c0de | Team Xe... | SX Pro Dongle                | 1     | cdc_acm    | 5DF12FB380 |

### Net/wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:0719 | Microsoft  | Xbox 360 Wireless Adapter    | 1     | xpad       | D188C06456 |
| 0bda:8174 | Realtek... | RTL8192SU 802.11n WLAN Ad... | 1     | r8712u     | 633AB37A3F |
| 0bda:b812 | Realtek... | RTL8812BU USB3.0 802.11ac... | 1     |            | D188C06456 |
| 0cf3:9271 | Qualcom... | AR9271 802.11n               | 1     | ath9k_htc  | 1129BEE39C |
| 148f:3070 | Ralink ... | RT2870/RT3070 Wireless Ad... | 1     | rt2800usb  | FC67594808 |
| 148f:5370 | Ralink ... | RT5370 Wireless Adapter      | 1     | rt2800usb  | F339ADE022 |
| 148f:5572 | Ralink ... | RT5572 Wireless Adapter      | 1     | rt2800usb  | 2CBDC3AD17 |
| 148f:761a | Ralink ... | MT7610U ("Archer T2U" 2.4... | 1     |            | 2350EA995C |

### Network (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:8153 | Realtek... | RTL8153 Gigabit Ethernet ... | 2     | r8152      | F1331B62E6 |
| 04e8:6863 | Samsung... | GT-I9500 [Galaxy S4] / GT... | 1     | rndis_host | 8A4B351000 |
| 0846:9054 | NetGear    | A7000                        | 1     | 8814au     | 2CBDC3AD17 |
| 0bda:8187 | Realtek... | RTL8187 Wireless LAN Adapter | 1     | rtl8187    | 21C033AABC |
| 12d1:108a | Huawei ... | DLI-TL20                     | 1     | rndis_host | B96B32B8B7 |
| 12d1:14db | Huawei ... | E353/E3131 34GB              | 1     | cdc_ether  | F8F1E1681B |

### Phone (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bb4:0c02 | HTC (Hi... | Dream / ADP1 / G1 / Magic... | 1     | usbfs      | FCEBA80CA7 |
| 18d1:4ee1 | Google     | Nexus Device (MTP)           | 1     |            | D6FE9DA999 |

### Printer (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:0c17 | Hewlett... | LaserJet 1010                | 1     | usblp      | 4E98627DA4 |
| 04a9:1787 | Canon      | PIXMA MX490 Series           | 1     | usblp      | FC67594808 |
| 04f9:03d3 | Brother... | MFC-J5330DW                  | 1     | usblp      | 0DD740DC8F |
| 04f9:0424 | Brother... | DCP-L2550DW series           | 1     | usblp      | 24BE20A939 |
| 0922:0020 | Dymo-Co... | LabelWriter 450              | 1     | usblp      | FCEBA80CA7 |

### Scanner (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04a9:1904 | Canon      | CanoScan LiDE 100            | 1     |            | C2DA8438C2 |
| 04b8:0120 | Seiko E... | GT-7400U [Perfection 1270]   | 1     |            | 4E98627DA4 |

### Serial controller (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0403:6001 | Future ... | FT232 USB-Serial (UART) IC   | 1     | ftdi_sio   | 7C4C7CAF9A |
| 0403:6014 | Future ... | FT232H Single HS USB-UART... | 1     | ftdi_sio   | F1331B62E6 |

### Sound (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:0a37 | Logitech   | USB Headset H540             | 1     | snd_usb... | D6FE9DA999 |
| 046d:0a45 | Logitech   | USB Headset                  | 1     | snd_usb... | 9898EDBAF1 |
| 05ac:1107 | Apple      | Thunderbolt Display Audio    | 1     | snd_usb... | 029708EAF4 |
| 05fc:0231 | Harman     | JBL Pebbles                  | 1     | snd_usb... | 21C033AABC |
| 0b0e:245d | GN Netcom  | Jabra Link 370               | 1     | snd_usb... | 2CBDC3AD17 |
| 0ccd:0077 | TerraTe... | Aureon Dual USB              | 1     | snd_usb... | 7C4C7CAF9A |
| 0d8c:000c | C-Media... | Audio Adapter                | 1     | snd_usb... | 2CBDC3AD17 |
| 1852:50d0 | GYROCOM... | FiiO USB DAC-E17             | 1     | snd_usb... | A48C087C97 |
| 1852:7022 | GYROCOM... | Grant Fidelity dac-11        | 1     | snd_usb... | 23DC7C0455 |

### Others (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:5400 | Realtek... | BillBoard Device             | 1     |            | A48C087C97 |
| 17e9:6000 | Display... | USB 3.0 5K Graphic Docking   | 1     | snd_usb... | 23DC7C0455 |
| 17ef:a361 | Lenovo     | BillBoard Device             | 1     |            | 2CBDC3AD17 |
| 18d1:4ee2 | Google     | Nexus Device (debug)         | 1     |            | 2FB80FF93D |
| 1d5c:5100 | Fresco ... | Generic Billboard Device     | 1     |            | 23DC7C0455 |

