Most popular USB devices in Tablets
===================================

See more info in the [README](https://github.com/linuxhw/LsUSB).

Contents
--------

1. [ USB Devices in Tablets ](#usb-devices)
   * [ Bluetooth ](#bluetooth-usb)
   * [ Camera ](#camera-usb)
   * [ Card reader ](#card-reader-usb)
   * [ Cdrom ](#cdrom-usb)
   * [ Chipcard ](#chipcard-usb)
   * [ Disk ](#disk-usb)
   * [ Fingerprint reader ](#fingerprint-reader-usb)
   * [ Hub ](#hub-usb)
   * [ Human interface ](#human-interface-usb)
   * [ Input/keyboard ](#inputkeyboard-usb)
   * [ Input/mouse ](#inputmouse-usb)
   * [ Miscellaneous ](#miscellaneous-usb)
   * [ Net/wireless ](#netwireless-usb)
   * [ Network ](#network-usb)
   * [ Serial controller ](#serial-controller-usb)
   * [ Sound ](#sound-usb)
   * [ Touchpad ](#touchpad-usb)
   * [ Others ](#others-usb)

USB Devices
-----------

Count  — number of computers with this device installed,
Driver — driver in use for this device,
Probe  — latest probe ID of this device.

### Bluetooth (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1286:204c | Marvell... | Bluetooth and Wireless LA... | 19    | btusb      | 329795002B |
| 8087:0a2a | Intel      | Bluetooth Device             | 13    | btusb      | DD16DF4133 |
| 13d3:3501 | IMC Net... | Bluetooth Device             | 9     | btusb      | 838DD8C3AC |
| 8087:0a2b | Intel      | Bluetooth Device             | 6     | btusb      | C97022A8FC |
| 0489:e09f | Foxconn... | Bluetooth Device             | 2     | btusb      | D6EFA0F211 |
| 1286:2044 | Marvell... | Bluetooth and Wireless LA... | 2     | btusb      | 34114C0F3A |
| 0a12:0001 | Cambrid... | Bluetooth Dongle (HCI mode)  | 1     | btusb      | D6EFA0F211 |
| 0bda:b723 | Realtek... | Bluetooth Radio              | 1     | btusb      | DDB662C800 |
| 0cf3:e300 | Qualcom... | Qualcomm Atheros Bluetoot... | 1     | btusb      | D7B5599247 |
| 0cf3:e302 | Qualcom... | Qualcomm Atheros Bluetoot... | 1     | btusb      | 57487D8BF7 |
| 1286:204b | Marvell... | Bluetooth and Wireless LA... | 1     | btusb      | 7C5227AA3D |
| 8087:07da | Intel      | Bluetooth Device             | 1     | btusb      | 68ECFC5409 |

### Camera (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 058f:5608 | Alcor M... | USB 2.0 Web Camera           | 6     | uvcvideo   | EF78567252 |
| 045e:0794 | Microsoft  | Front LifeCam                | 4     | uvcvideo   | 80C0EC4495 |
| 045e:0795 | Microsoft  | Rear LifeCam                 | 4     | uvcvideo   | 80C0EC4495 |
| 04f2:b5a9 | Chicony... | HP TrueVision HD             | 3     | uvcvideo   | DD16DF4133 |
| 05c8:03a3 | Cheng U... | HP TrueVision HD             | 2     | uvcvideo   | 46F1B1D139 |
| 0bda:56c1 | Realtek... | USB Camera                   | 2     | uvcvideo   | D6EFA0F211 |
| 045e:07be | Microsoft  | LifeCam Front                | 1     | uvcvideo   | 7C5227AA3D |
| 045e:07bf | Microsoft  | LifeCam Rear                 | 1     | uvcvideo   | 7C5227AA3D |
| 046d:0825 | Logitech   | Webcam C270                  | 1     | snd_usb... | DBF25F43EB |
| 04ca:7048 | Lite-On... | Integrated Rear Camera       | 1     | uvcvideo   | 5560D6A80E |
| 04f2:b445 | Chicony... | USB 2.0 Camera               | 1     | uvcvideo   | DDB662C800 |
| 04f2:b45c | Chicony... | Integrated Camera            | 1     | uvcvideo   | 5560D6A80E |
| 0c45:6321 | Microdia   | Laptop_Integrated_Webcam_... | 1     | uvcvideo   | 6DD31D6E80 |
| 174f:1a09 | Syntek     | USB2.0 Camera                | 1     | uvcvideo   | 68ECFC5409 |
| 3168:3168 | Camera     | Camera                       | 1     | uvcvideo   | 7728B604CF |
| 5986:0529 | Acer       | USB HD Webcam                | 1     | uvcvideo   | 68ECFC5409 |

### Card reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:0129 | Realtek... | RTS5129 Card Reader Contr... | 4     | rtsx_usb   | DDB662C800 |

### Cdrom (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 8564:1000 | Transcend  | TS32GJF370 JetFlash 32GB     | 4     | uas, us... | 34441EB151 |
| 12d1:107e | Huawei ... | File-CD Gadget               | 1     | uas, us... | 73B87C222F |
| 413c:9016 | Dell       | DVD+/-RW DW316               | 1     | usb_sto... | D7B5599247 |

### Chipcard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0a5c:5834 | Broadcom   | 5880                         | 1     |            | 75F6A84286 |

### Disk (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:090c | Microsoft  | SD Card                      | 4     | uas, us... | A9A066D4B7 |
| 058f:6387 | Alcor M... | Transcend JetFlash Flash ... | 3     | uas, us... | 53FB1E06CD |
| 045e:0306 | Microsoft  | SD/MMC                       | 2     | uas, us... | 9D9D8E830B |
| 090c:1000 | Silicon... | Flash Voyager                | 2     | uas, us... | CDC697F1B7 |
| 0930:6545 | Toshiba    | Kingston DataTraveler 102... | 2     | uas, us... | 7728B604CF |
| 0bda:0307 | Realtek... | Card Reader                  | 2     | uas, us... | 7C5227AA3D |
| 048d:1172 | Integra... | USB Flash Disk 15GB          | 1     | uas, us... | 8F7746D4FE |
| 04e8:61f5 | Samsung... | Portable SSD T5              | 1     | uas        | 00B3317F1F |
| 05e3:0727 | Genesys... | microSD Reader/Writer        | 1     | uas, us... | 0995A2D2E4 |
| 05e3:0752 | Genesys... | STORAGE DEVICE               | 1     | uas, us... | FCEDD9DED7 |
| 0718:0619 | Imation    | TF10 8GB                     | 1     | uas, us... | 73B87C222F |
| 0781:557d | SanDisk    | Cruzer Force (64GB) 16GB     | 1     | uas, us... | DBF25F43EB |
| 0781:558a | SanDisk    | Ultra 62GB                   | 1     | uas, us... | DAA1B9A067 |
| 0951:1624 | Kingsto... | DataTraveler G2 16GB         | 1     | uas, us... | F8914F95A4 |
| 0951:1665 | Kingsto... | Digital DataTraveler SE9 ... | 1     | uas, us... | 57910E3D67 |
| 0951:1666 | Kingsto... | DataTraveler 100 G3/G4/SE... | 1     | uas, us... | 794D43EDD9 |
| 1058:0820 | Western... | My Passport Ultra (WDBMWV... | 1     | uas, us... | FCEDD9DED7 |
| 1516:6221 | CompUSA    | Disk                         | 1     | uas, us... | EF78567252 |

### Fingerprint reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04f3:0903 | Elan Mi... | ELAN:Fingerprint             | 5     |            | 838DD8C3AC |

### Hub (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1d6b:0002 | Linux F... | 2.0 root hub                 | 104   | hub        | DD16DF4133 |
| 1d6b:0003 | Linux F... | 3.0 root hub                 | 104   | hub        | DD16DF4133 |
| 05e3:0608 | Genesys... | USB-2.0 4-Port HUB           | 23    | hub        | DD16DF4133 |
| 05e3:0610 | Genesys... | 4-port hub                   | 7     | hub        | 838DD8C3AC |
| 1a40:0101 | incompl... | 4-Port HUB                   | 7     | hub        | E78474B0F4 |
| 045e:090b | Microsoft  | Hub                          | 5     | hub        | 2FB15D0705 |
| 045e:091a | Microsoft  | Hub                          | 5     | hub        | 2FB15D0705 |
| 045e:0941 | Microsoft  | L1 USB3 Gen1 Hub             | 4     | hub        | A9A066D4B7 |
| 045e:0942 | Microsoft  | L1 USB2 Hub                  | 4     | hub        | A9A066D4B7 |
| 045e:0943 | Microsoft  | L2 USB3 Gen1 Hub             | 4     | hub        | A9A066D4B7 |
| 045e:0944 | Microsoft  | L2 USB2 Hub                  | 4     | hub        | A9A066D4B7 |
| 045e:0901 | Microsoft  | Surface Dock Hub             | 3     | hub        | DBF25F43EB |
| 045e:0903 | Microsoft  | Surface Dock Hub             | 3     | hub        | DBF25F43EB |
| 2109:2811 | VIA Labs   | USB2.0 Hub                   | 3     | usbcore    | 464706154E |
| 2109:2813 | VIA Labs   | USB2.0 Hub                   | 3     | hub        | FCEDD9DED7 |
| 8087:8000 | Intel      | Hub                          | 3     | hub        | 80C0EC4495 |
| 03f0:2512 | Hewlett... | OfficeJet Pro L7300 / Com... | 2     | usbcore    | 3B331BB4AF |
| 0424:2514 | Standar... | USB 2.0 Hub                  | 2     | hub        | 3D33E96551 |
| 045e:0900 | Microsoft  | Surface Dock Hub             | 2     | hub        | DBF25F43EB |
| 045e:0902 | Microsoft  | Surface Dock Hub             | 2     | hub        | DBF25F43EB |
| 1a40:0801 | Terminu... | USB 2.0 Hub                  | 2     | hub        | 4064B5A9F9 |
| 8087:0024 | Intel      | Integrated Rate Matching Hub | 2     | hub        | 68ECFC5409 |
| 0424:2137 | Standar... | USB2137B                     | 1     | hub        | 68ECFC5409 |
| 0424:2807 | Standar... | USB2807 Hub                  | 1     | hub        | D7B5599247 |
| 0424:5537 | Standar... | USB5537B                     | 1     | hub        | 68ECFC5409 |
| 0424:5807 | Standar... | USB5807 Hub                  | 1     | hub        | D7B5599247 |
| 04b4:6504 | Cypress... | USB-C Dock Hub2              | 1     | hub        | D6EFA0F211 |
| 04b4:6506 | Cypress... | CY4603                       | 1     | hub        | D6EFA0F211 |
| 04b4:6508 | Cypress... | USB-C Dock Hub1              | 1     | hub        | D6EFA0F211 |
| 04b4:650a | Cypress... | CY4613                       | 1     | hub        | D6EFA0F211 |
| 058f:6254 | Alcor M... | USB Hub                      | 1     | hub        | 53FB1E06CD |
| 058f:9254 | Alcor M... | Hub                          | 1     | hub        | F49516DF85 |
| 05e3:0612 | Genesys... | USB3.0 Hub 123               | 1     | hub        | 1AD0A80B86 |
| 05e3:0618 | Genesys... | USB2.0 Hub                   | 1     | hub        | FCEDD9DED7 |
| 0bda:0411 | Realtek... | 4-Port USB 3.0 Hub           | 1     | hub        | F8914F95A4 |
| 0bda:5411 | Realtek... | 4-Port USB 2.0 Hub           | 1     | hub        | F8914F95A4 |
| 2109:0813 | VIA Labs   | USB3.0 Hub                   | 1     | hub        | FCEDD9DED7 |
| 2109:0817 | VIA Labs   | USB3.0 Hub                   | 1     | hub        | 1DD80D33E5 |
| 2109:2817 | VIA Labs   | USB2.0 Hub                   | 1     | hub        | 1DD80D33E5 |
| 2109:8110 | VIA Labs   | USB 3.0 Hub                  | 1     | hub        | 5560D6A80E |
| 8087:07e6 | Intel      | Hub                          | 1     | hub        | DDB662C800 |
| 8564:4000 | Transcend  | RDF8 16GB                    | 1     | uas, us... | 57910E3D67 |

### Human interface (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03eb:8209 | Atmel      | maXTouch Digitizer           | 4     | usbhid     | 80C0EC4495 |
| 045e:0904 | Microsoft  | Surface Dock Extender        | 3     | usbhid     | DBF25F43EB |
| 045e:079c | Microsoft  | SAM                          | 1     | usbhid     | 34114C0F3A |
| 04b5:0680 | ROHM LS... | HID Sensor Board DFUP2183ZA  | 1     | usbhid     | 68ECFC5409 |
| 05a7:40fe | Bose       | QC35 II                      | 1     | usbhid     | 59CA47D9E7 |

### Input/keyboard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:c534 | Logitech   | Unifying Receiver            | 8     | usbhid     | 906640CBBB |
| 258a:6a88 | EBITS-E... | USB KEYBOARD                 | 7     | usbhid     | 4064B5A9F9 |
| 046d:c52b | Logitech   | Unifying Receiver            | 6     | usbhid     | 68ECFC5409 |
| 0911:2188 | Philips... | Hantick Wireless Device      | 6     | usbhid     | C1CE0C0EEA |
| 045e:07cd | Microsoft  | Surface Keyboard             | 5     | usbhid     | 2FB15D0705 |
| 04f3:0755 | Elan Mi... | Keyboard                     | 5     | usbhid     | DD16DF4133 |
| 258a:2016 | SINO WE... | USB KEYBOARD                 | 5     | usbhid     | EF78567252 |
| 045e:0922 | Microsoft  | Surface Keyboard             | 4     | usbhid     | A9A066D4B7 |
| 04f2:1558 | Chicony... | ACER Keyboard                | 3     | usbhid     | D6EFA0F211 |
| 0b05:183b | ASUSTek... | ASUS HID Device              | 3     | usbhid     | 23E48DD182 |
| 1018:1006 |            | Keyboard                     | 3     | usbhid     | C3D0D03F6B |
| 046a:0023 | Cherry     | CyMotion Master Linux Key... | 2     | usbhid     | 3B331BB4AF |
| 048d:8911 | Integra... | HTX HID Device               | 2     | usbhid     | BD392FF7CD |
| 0b05:183d | ASUSTek... | ASUS HID Device              | 2     | usbhid     | 457058D29F |
| 0b05:184b | ASUSTek... | T102 HID Device              | 2     | usbhid     | 838DD8C3AC |
| 1c4f:0063 | SiGma M... | USB Keyboard                 | 2     | usbhid     | 204399BB09 |
| 045e:0745 | Microsoft  | Nano Transceiver v1.0 for... | 1     | usbhid     | A8E6DCBD35 |
| 045e:0773 | Microsoft  | Microsoft Nano Transceive... | 1     | usbhid     | F49516DF85 |
| 045e:096f | Microsoft  | Surface Type Cover           | 1     | usbhid     | 57487D8BF7 |
| 046d:c315 | Logitech   | Classic Keyboard 200         | 1     | usbhid     | 998370C932 |
| 04b4:00ff | Cypress... | PS2toUSB KBM Bridge V2.02a   | 1     | usbhid     | 68ECFC5409 |
| 04f3:0103 | Elan Mi... | ActiveJet K-2024 Multimed... | 1     | usbhid     | F8914F95A4 |
| 04f3:074d | Elan Mi... | Generic USB Mouse            | 1     | usbhid     | ED2994B570 |
| 04f3:0757 | Elan Mi... | USBSmartKeyboard             | 1     | usbhid     | FDB567A59E |
| 0603:0002 | Novatek... | USB Composite Device         | 1     | usbhid     | 0995A2D2E4 |
| 062a:4101 | MosArt ... | Wireless Keyboard/Mouse      | 1     | usbhid     | 7D0FCAC082 |
| 06cb:73f5 | Synaptics  | ITE Device(8910)             | 1     | usbhid     | 275015E4FA |
| 0b05:184a | ASUSTek... | ASUS Touchpad and G-senso... | 1     | usbhid     | 0B5AD4104E |
| 1017:1006 | Speedy ... | Generic USB Mouse            | 1     | usbhid     | A752F887E4 |
| 12d1:1090 | Huawei ... | Folio keyboard               | 1     | usbhid     | 3DD5AAE701 |
| 145f:0274 | Trust      | Keyboard                     | 1     | usbhid     | E78474B0F4 |
| 17ef:6018 | Lenovo     | Low Profile USB Keyboard     | 1     | usbhid     | F49516DF85 |
| 17ef:6064 | Lenovo     | NEC 10-inch Tablet Keyboard  | 1     | usbhid     | 5AF55E5191 |
| 17ef:6067 | Lenovo     | ITE Device(8595)             | 1     | usbhid     | 5560D6A80E |
| 17ef:6076 | Lenovo     | ThinkPad 10 Folio Keyboard   | 1     | usbhid     | DF76656467 |
| 1c4f:0002 | SiGma M... | Keyboard TRACER Gamma Ivory  | 1     | usbhid     | C2C81FC796 |
| 248a:ff0f | Maxxter    | Wireless Receiver            | 1     | usbhid     | 8F8CA11240 |
| 258a:0f08 | SINO WE... | USB TOUCHPAD KEYBOARD        | 1     | usbhid     | 736926FF82 |
| 413c:2105 | Dell       | Model L100 Keyboard          | 1     | usbhid     | D7B5599247 |
| 6080:8061 | SINO WE... | USB KEYBOARD                 | 1     | usbhid     | 7728B604CF |

### Input/mouse (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:07e8 | Microsoft  | Surface Type Cover           | 5     | usbhid     | 46111148A0 |
| 258a:1015 | SINO WE... | USB Composite Device         | 5     | usbhid     | 31140595BC |
| 17ef:60bb | Lenovo     | HID Device                   | 3     | usbhid     | 1DD80D33E5 |
| 0458:0185 | KYE Sys... | Wireless Mouse               | 2     | usbhid     | 84C23872E8 |
| 045e:07a9 | Microsoft  | SAM                          | 2     | usbhid     | 80C0EC4495 |
| 045e:07e2 | Microsoft  | Surface Type Cover           | 2     | usbhid     | 7C5227AA3D |
| 046d:c069 | Logitech   | M-U0007 [Corded Mouse M500]  | 2     | usbhid     | 3B331BB4AF |
| 06cb:2872 | Synaptics  | ITE Device(8595)             | 2     | usbhid     | 3D33E96551 |
| 1532:0067 | Razer USA  | Razer Naga Trinity           | 2     | usbhid     | 59CA47D9E7 |
| 17ef:608a | Lenovo     | YOGA Mouse                   | 2     | usbhid     | DAA1B9A067 |
| 1ea7:0064 | SHARKOO... | 2.4G Wireless Mouse          | 2     | usbhid     | C2C81FC796 |
| 413c:301a | Dell       | MS116 USB Optical Mouse      | 2     | usbhid     | 1DD80D33E5 |
| 045e:079a | Microsoft  | SAM                          | 1     | usbhid     | 2EBF213BA2 |
| 045e:07e4 | Microsoft  | Surface Type Cover           | 1     | usbhid     | 9D9D8E830B |
| 045e:09c0 | Microsoft  | Surface Type Cover           | 1     | usbhid     | 428581C02E |
| 046d:c058 | Logitech   | M115 Mouse                   | 1     | usbhid     | CD17A4685B |
| 046d:c05a | Logitech   | M90/M100 Optical Mouse       | 1     | usbhid     | A752F887E4 |
| 046d:c52f | Logitech   | Unifying Receiver            | 1     | usbhid     | 7724DDCBBA |
| 046d:c530 | Logitech   | HID-compliant mouse          | 1     | usbhid     | C97022A8FC |
| 046d:c537 | Logitech   | USB Receiver                 | 1     | usbhid     | 05764955D2 |
| 04f3:0230 | Elan Mi... | 3D Optical Mouse             | 1     | usbhid     | EA1FBBFBF0 |
| 056a:00ec | Wacom      | ISD-V4                       | 1     | usbhid     | 68ECFC5409 |
| 0eef:7917 | D-WAV S... | eGalaxTouch EXC7903-85v10    | 1     | usbhid     | 68ECFC5409 |
| 17ef:6039 | Lenovo     | Laser Wireless Mouse         | 1     | usbhid     | 62F7A7B1E3 |
| 1c4f:0034 | SiGma M... | Usb Mouse                    | 1     | usbhid     | 34441EB151 |
| 1d57:ad03 | Xenta      | Feeling-tech-USB product     | 1     | usbhid     | 998370C932 |
| 248a:8366 | Maxxter    | Wireless Optical Mouse AC... | 1     | usbhid     | 5DE043A395 |
| 248a:8367 | Maxxter    | SVEN Comfort 3400 Wireless   | 1     | usbhid     | F8914F95A4 |
| 276d:1160 | YSTEK      | G Mouse                      | 1     | usbhid     | 037FFBE89A |
| 3938:1110 | Acrox      | AmazonBasics gaming mouse    | 1     | usbhid     | E844699BF8 |
| 6901:2701 |            | USB Mouse                    | 1     | usbhid     | 4D2258703A |

### Miscellaneous (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 2d95:600a | vivo       | NEX S                        | 1     | rndis_host | 1DD80D33E5 |

### Net/wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 413c:81a4 | Dell       | Wireless 5570e HSPA+ (42M... | 2     | qcseria... | 73B87C222F |
| 0411:016f | BUFFALO    | WLI-UC-G301N Wireless LAN... | 1     | rt2800usb  | 5AF55E5191 |
| 148f:5370 | Ralink ... | RT5370 Wireless Adapter      | 1     | rt2800usb  | 80C0EC4495 |
| 148f:5572 | Ralink ... | RT5572 Wireless Adapter      | 1     | rt2800usb  | E844699BF8 |
| 148f:7601 | Ralink ... | MT7601U Wireless Adapter     | 1     | mt7601u    | 8239479528 |
| 2001:3319 | D-Link     | RTL8192EU DWA-131 Wireles... | 1     | rtl8xxxu   | 0995A2D2E4 |
| 2357:0109 | Realtek    | RTL8192EU TL-WN823N 802.1... | 1     | rtl8xxxu   | 736926FF82 |

### Network (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04e8:6864 | Samsung... | GT-I9070 (network tetheri... | 3     | rndis_host | 464706154E |
| 0bda:8153 | Realtek... | RTL8153 Gigabit Ethernet ... | 3     | r8152      | D7B5599247 |
| 045e:07c6 | Microsoft  | RTL8153 GigE [Surface Doc... | 2     | r8152      | DBF25F43EB |
| 0b95:772b | ASIX El... | AX88772B                     | 2     | asix       | B8B52EB742 |
| 0bda:8152 | Realtek... | RTL8152 Fast Ethernet Ada... | 2     | r8152      | 68ECFC5409 |
| 1199:a001 | Sierra ... | EM7345 4G LTE                | 2     | cdc_acm    | DF76656467 |
| 2357:000a | TP-Link    | M7200                        | 2     | rndis_host | 31140595BC |
| 8087:0911 | Intel      | PRODUCT_MODEM                | 2     | cdc_mbim   | E78474B0F4 |
| 04b4:3610 | Cypress... | USB-C PD Docking             | 1     | ax88179... | D6EFA0F211 |
| 04e8:6863 | Samsung... | GT-I9500 [Galaxy S4] / GT... | 1     | rndis_host | 4077F8762C |
| 1199:9011 | Sierra ... | MC8305                       | 1     | qcserial   | 68ECFC5409 |
| 17ef:7436 | Lenovo     | MT65xx Android Phone         | 1     | rndis_host | 485C815772 |
| 2001:1a02 | D-Link     | DUB-E100 Fast Ethernet Ad... | 1     | asix       | 24743A2A16 |
| 216f:0043 | Altair ... | Modem YOTA 4G LTE            | 1     | cdc_ether  | 8F7746D4FE |
| 2b0e:171d | Android    | Android                      | 1     | rndis_host | 7D0FCAC082 |
| 413c:81b6 | Dell       | DW5811e Snapdragon X7 LTE    | 1     | qcseria... | C97022A8FC |
| 9710:7830 | MosChip... | MCS7830 10/100 Mbps Ether... | 1     | mcs7830    | 7728B604CF |

### Serial controller (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0403:6001 | Future ... | FT232 USB-Serial (UART) IC   | 1     | ftdi_sio   | 68ECFC5409 |

### Sound (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 08bb:2912 | Texas I... | PCM2912A Audio Codec         | 1     | snd_usb... | D6EFA0F211 |
| 0bda:4000 | Realtek... | USB Audio                    | 1     | snd_usb... | 5560D6A80E |
| 0bda:4014 | Realtek... | USB Audio                    | 1     | snd_usb... | D7B5599247 |
| 0c76:1607 | JMTek      | audio controller             | 1     | snd_usb... | 1DD80D33E5 |
| 0d8c:0012 | C-Media... | USB Audio Device             | 1     | snd_usb... | D6EFA0F211 |

### Touchpad (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 044e:1218 | Alps El... | Touchpad                     | 3     | usbhid     | 73B87C222F |
| 044e:1210 | Alps El... | Touchpad                     | 1     | usbhid     | C97022A8FC |

### Others (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:091e | Microsoft  | XBOX ACC                     | 2     |            | A9A066D4B7 |
| 04b4:5217 | Cypress... | Billboard Device             | 1     | usbhid     | D6EFA0F211 |
| 0a5c:6420 | Broadcom   | BCM2045A0                    | 1     | btusb      | 3DD5AAE701 |
| 0e8d:201d | MediaTek   | Tele2_Maxi_LTE               | 1     |            | C5AB20FD17 |
| 17e9:4300 | Display... | USB Display Adapter          | 1     | snd_usb... | 68ECFC5409 |
| 187f:0600 | Siano M... | MDTV Receiver                | 1     | smsusb     | DDB662C800 |
| 2109:0100 | VIA Labs   | USB 2.0 BILLBOARD            | 1     |            | FCEDD9DED7 |
| 2109:0102 | VIA Labs   | USB 2.0 BILLBOARD            | 1     |            | 57910E3D67 |

